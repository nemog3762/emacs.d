
=EmacCASI=

- [[elisp:org-agenda][(a) org-agenda]]
- [[elisp:bookmark-jump][(b) bookmark]]
- [[elisp:calendar][(c) calendar]]
- [[elisp:dired-jump ][(d) dired]]
- [[elisp:my-toggle-notes][(n) my-toggle-notes]]
- [[elisp:my-choose-open][(o) my-choose-open]]
- [[elisp:org-timer-set-timer][(p) pomodoro]]
- [[elisp:recentf-open][(r) open recent file]]
- [[elisp:my-lookup-search-web][(s) my-lookup-search-web]]

;; Local Variables:
;; eval: (start-mode)
;; End:
