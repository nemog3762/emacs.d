;; -*- coding: utf-8; lexical-binding: t; -*-

;;; Aliases

(defalias 'choose-emoji            'emoji-search)
(defalias 'delete-line             'kill-line)
(defalias 'dl                      'duplicate-line)
(defalias 'my-input-esperanto      '(lambda () (interactive ) (set-input-method "esperanto-postfix")))
(defalias 'my-input-restart        'toggle-input-method )
(defalias 'my-multiple-cursors     'set-rectangular-region-anchor)
(defalias 'rectangle-select        'rectangle-mark-mode)
(defalias 'my-org-enumerate        'org-toggle-item)
(defalias 'qcal                    'quick-calc)
(defalias 'toggle-lines-wrap       'toggle-truncate-lines)
(defalias 'toggle-view-whitespace  'whitespace-mode)
(defalias 'unicode-insert          'insert-char)
(defalias 'org-toggle-table-type   'org-table-create-with-table.el)

;;; Useful things from Xah

(load "xah-commands"   t)
;; (load "xah-elisp-mode" t) ;; This autoload in .el files
(load "xah-find"       t)
(load "xah-html-mode"  t)
(load "xah-math-input" t)
(load "xah-css-mode"   t)

;;; Load my .el files

(load "my-gui"        t)
(load "my-defaults"   t)
(load "my-commands"   t)
(load "my-isearch"    t)
(load "my-completion" t)
(load "my-bin"        t)
(load "my-calc"       t)
(load "my-project"    t)
(load "my-keyboard"   t)
(load "my-doc-view"   t)
(load "my-eww"        t)
(load "my-image"      t)
(load "my-nxml"       t)
(load "my-prog"       t)
(load "my-lookup"     t)
(load "my-newsticker" t)
(load "my-bible"      t)
(load "my-insert-char"           t)
(load "my-keyboard-leader-key"   t)
(load "my-packages-internet-no"  t)
(when (file-exists-p (concat user-emacs-directory "flag-my-packages-internet-yes"))
  (load "my-packages-internet-yes" t))

;;; Other loads

(use-package my-org :after (org))
(use-package my-choose)

(use-package my-calendar
  :defer 1
  :commands (calendar)
  :after (calendar))

;;; Dired

(use-package dired+
  :defer t
  :config
  (set-face-attribute 'diredp-dir-name nil :foreground nil :background nil :weight 'bold)
  (set-face-attribute 'diredp-omit-file-name nil :strike-through nil :inherit 'shadow)
  (set-face-attribute 'diredp-dir-heading nil :background nil)
  ;; Non-nil means `dired-toggle-marks' acts also on `.' and `..'.
  (setq diredp-toggle-dot+dot-dot-flag nil))

(use-package my-dired
  :defer 0.2
  :commands (dired dired-jump)
  :after dired)

(use-package my-dired-ranger
  :ensure nil
  :after dired
  :bind
  (:map dired-mode-map
        ("y" . my-dired-ranger-copy)
        ("p" . my-dired-ranger-paste)
        ("m" . my-dired-ranger-move)))

(use-package my-dired-open
  :init
  (require 'my-dired-open)
  :bind
  (:map dired-mode-map
        ("o" . my-open-in-default-app)))

;;; my-dashboard

(setq initial-buffer-choice (concat user-emacs-directory "my-dashboard.org"))

(define-minor-mode start-mode
  "Provide functions for custom start page."
  :lighter " start"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "a") 'org-agenda)
            (define-key map (kbd "b") 'bookmark-jump)
            (define-key map (kbd "c") 'calendar)
            (define-key map (kbd "d") 'dired-jump)
            (define-key map (kbd "n") 'my-toggle-notes)
            (define-key map (kbd "o") 'my-choose-open)
            (define-key map (kbd "p") 'org-timer-set-timer)
            (define-key map (kbd "r") 'recentf-open)
            (define-key map (kbd "s") 'my-lookup-search-web)
            (define-key map (kbd "DEL") 'my-switch-to-last-buffer)
          map))

;; make start.org read-only; use 'SPC t r' to toggle off read-only.
(add-hook 'start-mode-hook 'read-only-mode)

(provide 'start-mode)

(global-set-key (kbd "<f3><f3>") (lambda() (interactive) (find-file (concat user-emacs-directory "my-dashboard.org"))))
