;;;; Functions

;;; Function to toggle GUI elements
(defun my-toggle-ui-gui-elements ()
  "Toggle UI elements in Emacs with prompts for each option."
  (interactive)
  (if (y-or-n-p "Activate menu bar? ")
      (menu-bar-mode 1)
    (menu-bar-mode -1))
  (if (y-or-n-p "Activar scroll bar? ")
      (scroll-bar-mode 1)
    (scroll-bar-mode -1))
  (if (y-or-n-p "Activate tool bar? ")
      (tool-bar-mode 1)
    (tool-bar-mode -1))
  ;; Update frame
  (redraw-frame (selected-frame)))

;;; Function for toggle theme
(defun my-toggle-theme-modus ()
  "Toggle between 'modus-operandi' (light) and 'modus-vivendi' (dark) themes."
  (interactive)
  (if (get real-this-command 'xstatus)
      (progn
        (load-theme 'modus-operandi t)
        (put real-this-command 'xstatus nil))
    (progn
      (load-theme 'modus-vivendi t)
      (put real-this-command 'xstatus t))))

;;;; Theme

;;; Set background
;; (set-background-color "honeydew2")
(set-background-color "white")

;;; Set theme
;; (load-theme 'modus-operandi)

;;; Hightline color
;;(set-face-background 'hl-line "light gray")

;;; Font

;; No colocar al texto cursiva como subrayado.
(set-face-attribute 'italic nil :slant 'italic :underline nil :foreground "red")

;; Line spacing
(setq-default line-spacing nil)

;; Option to only change size and use default font
;; The value is in 1/10pt, so 100 will give you 10pt, etc.
;; Default size of font is 12pt or 120.
(set-face-attribute 'default nil :height 100)

;; Set font (set the firts match, else, set 'DejaVu Sans Mono')
;; Test for the font: 0 O o / 1 l I
(cond
 ;; Not built-in
 ((member "JetBrains Mono" (font-family-list))
  (progn
    (set-frame-font "JetBrains Mono Light 13" nil t)
    (add-to-list 'default-frame-alist '(font . "JetBrains Mono Light 13"))
    ))
 ;; Default in macOS
 ((member "Menlo" (font-family-list))
  (progn
    (set-frame-font "Menlo 13" nil t)
    (add-to-list 'default-frame-alist '(font . "Menlo 13"))
    ))
 ;; Default in Ubuntu
 ((member "Ubuntu Mono" (font-family-list))
  (progn
    (set-frame-font "Ubuntu Mono 13" nil t)
    (add-to-list 'default-frame-alist '(font . "Ubuntu Mono 13"))
    ))
 ;; Default in Windows and GNU
 (t
  (progn
    (set-frame-font "DejaVu Sans Mono 13" nil t)
    (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono 13"))
    )))

;; 2024-01-17 La 'variable-pitch' cambia mucha otras fuentes, estilos y colores,
;; por lo que un .org con tablas y formulas se vuelve complicado leer.
;; Alternativas: No usarlo o enconrar una mejor forma de configurarlas.
;; Setting up variable-pitch
;; https://zzamboni.org/post/beautifying-org-mode-in-emacs/
;; (cond
;;  ((member "Libertinus Mono"   (font-family-list)) (set-face-font 'variable-pitch "Libertinus Mono"))
;;  ((member "Liberation Serif"   (font-family-list)) (set-face-font 'variable-pitch "Liberation Serif"))
;;  ((member "Latin Modern Roman" (font-family-list)) (set-face-font 'variable-pitch "Latin Modern Roman"))
;;  )

;;; Cursor

;; Draw cursor as wide as the gliph below
(setq x-stretch-cursor t)

;; Color
(set-cursor-color "Magenta")

;; Blink
(blink-cursor-mode 1)

;; Change type, from block to i-beam '|'.
;; May don't work on terminal.
(modify-all-frames-parameters (list (cons 'cursor-type 'bar)))

;;; mode-line

;; Don't show major or minor modes in the mode-line
;; https://stackoverflow.com/a/71264320
;; (setq-default mode-line-format (delq 'mode-line-modes mode-line-format))

;; Only show the major mode
(progn
  (defun my/mode-line-major-mode ()
    "Devuelve solo el nombre del major mode."
    (propertize (format "%s" major-mode)
                'face 'mode-line-active))
  (setq-default mode-line-format
              '(
                "%e"                               ;; Muestra errores de espacio en disco si ocurren
                mode-line-front-space              ;; Espacio inicial en la línea de modo
                mode-line-mule-info                ;; Información de codificación de caracteres
                mode-line-client                   ;; Indica si Emacs está en modo cliente (servidor)
                mode-line-modified                 ;; Indica si el buffer ha sido modificado (*)
                mode-line-remote                   ;; Indica si el buffer proviene de un sistema remoto
                mode-line-frame-identification     ;; Identificación del marco (frame) en modo gráfico
                mode-line-buffer-identification    ;; Nombre del buffer actual
                " ["
                (:eval (my/mode-line-major-mode))  ;; Evalúa y muestra solo el modo mayor (major mode)
                "] "
                mode-line-position                 ;; Muestra la posición del cursor (línea y columna)
                (vc-mode vc-mode)                  ;; Indica el estado de control de versiones si está activo
                " "                                ;; Espacio adicional
                mode-line-misc-info                ;; Información adicional (modo de entrada, batería, etc.)
                mode-line-end-spaces               ;; Espacio final en la línea de modo
                )))

;; Restart the mode-line to default value
;; (setq-default mode-line-format (default-value 'mode-line-format))

;; 'display-time-mode'    Display the current time.
;; (display-time-mode 1)

;; 'line-number-mode'     Display the current line number.mfs

;; 'column-number-mode'   Display the current column number
(column-number-mode t)
(setq mode-line-position (list "(%l,%c)"))

;; 'size-indication-mode' Show the current buffer size. See size-indication-mode
(size-indication-mode 1)

;; 'display-battery-mode' Show laptop battery information - DisplayBatteryMode.
(display-battery-mode 1)
