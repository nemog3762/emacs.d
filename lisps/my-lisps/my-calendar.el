;;; my-calendar.el --- my calendar config for Emacs -*- lexical-binding: t; -*-

;;; Configs

;; Calendar starts on Monday (1) or Sunday (0)
(setq calendar-week-start-day 0)

;; Format of date
;; 'american' (month/day/year)
;; 'european' (day/month/year)
;; 'iso'      (year/month/day)
(setq calendar-set-date-style 'iso)

;; En el calendario, mostrar el día actual.
(add-hook 'calendar-today-visible-hook 'calendar-mark-today)
(set-face-attribute 'calendar-today nil :background "green")

;; Movement
(define-key calendar-mode-map (kbd "<tab>")     'calendar-scroll-left)
(define-key calendar-mode-map (kbd "<backtab>") 'calendar-scroll-right)

;;; Festivos

;; Fijos y de pascua
(setq calendar-holidays
      '(
        ;; Festivos de fecha fija
        (holiday-fixed 01 01 "Año Nuevo")
        (holiday-fixed 05 01 "Dia del trabajo")
        (holiday-fixed 07 20 "Día de la independencia")
        (holiday-fixed 08 07 "Batalla de Boyacá")
        (holiday-fixed 12 08 "Inmaculada Concepción")
        (holiday-fixed 12 25 "Navidad")
        ;; Festivos basados en la Pascua
        (holiday-easter-etc -50 "Sábado de carnaval")
        (holiday-easter-etc -49 "Domingo de carnaval")
        (holiday-easter-etc -48 "Lunes de carnaval")
        (holiday-easter-etc -47 "Martes de carnaval")
        (holiday-easter-etc -03 "Jueves Santo")
        (holiday-easter-etc -02 "Viernes Santo")
        (holiday-easter-etc  43 "Ascención de Jesús")
        (holiday-easter-etc  64 "Corpus Christi")
        (holiday-easter-etc  71 "Sagrado Corazón de Jesús")
        ))

;; Festivos trasladables (Emiliani)
;; Si la fecha no es lunes, su celebración es trasladada al siguiente lunes en
;; el calendario. Ley 51 de 1983, o "Ley Emiliani", en la cual se creó esta
;; regla para incentivar el turismo.w
(setq holiday-other-holidays
      '(
        (holiday-sexp '(calendar-nth-named-day 1 1 01 year 06)
                      "Epifanía (Reyes Magos)")      ;; Enero 8
        (holiday-sexp '(calendar-nth-named-day 1 1 03 year 19)
                      "Día de San José")             ;; Marzo 19
        (holiday-sexp '(calendar-nth-named-day 1 1 06 year 29)
                      "San Pedro y San Pablo")       ;; Junio 29
        (holiday-sexp '(calendar-nth-named-day 1 1 08 year 15)
                      "Asunción de la Virgen")       ;; Agosto 15
        (holiday-sexp '(calendar-nth-named-day 1 1 10 year 12)
                      "Día de la raza")              ;; Octubre 12
        (holiday-sexp '(calendar-nth-named-day 1 1 11 year 01)
                      "Todos los santos")            ;; Noviembre 1
        (holiday-sexp '(calendar-nth-named-day 1 1 11 year 11)
                      "Independencia de Cartagena")  ;; Noviembre 11
        ))

;; Visualización de festivos en el calendario
(setq calendar-mark-holidays-flag t)
;; Carga los festivos definidos
(setq calendar-holidays (append holiday-other-holidays calendar-holidays))

(provide 'my-calendar)

;;; my-calendar.el ends here
