;; Configuración de opciones para image-mode
(setq image-dired-thumb-margin 1)    ;; Separación entre imágenes
(setq image-dired-thumb-relief 1)    ;; Relieve de miniaturas

;; Función personalizada para mover imágenes a la papelera
(defun my-image-delete-to-trash ()
  "Move the current image to trash."
  (interactive)
  (when (eq major-mode 'image-mode)
    (let ((file (buffer-file-name)))
      (when (and file (file-exists-p file))
        (if (yes-or-no-p (format "Delete %s?" file))
            (progn
              (move-file-to-trash file)
              (kill-buffer)))))))

;; Asignaciones de teclas en image-mode
(defun my-image-mode-keybindings ()
  "Custom keybindings for `image-mode`."
  (define-key image-mode-map (kbd "q")            #'kill-this-buffer)
  (define-key image-mode-map (kbd "D")            #'my-image-delete-to-trash)
  (define-key image-mode-map (kbd "<deletechar>") #'my-image-delete-to-trash)
  (define-key image-mode-map (kbd "+")            #'image-increase-size)
  (define-key image-mode-map (kbd "-")            #'image-decrease-size)
  (define-key image-mode-map (kbd "r")            #'image-rotate))

;; Agregar configuraciones personalizadas al hook de image-mode
(add-hook 'image-mode-hook #'my-image-mode-keybindings)
