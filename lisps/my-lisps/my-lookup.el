;;; my-lookup.el --- my lookup -*- lexical-binding: t; -*-

;; Idea from: http://xahlee.info/emacs/emacs/xah-lookup.html

(setq my-search-services
      ;; Note than ALL querys MUST have '%s' for the search term
      '(
        ("1337x.to"                              . "https://1337x.to/search/%s/1/")
        ("BibleGateway (NTV)"                    . "https://www.biblegateway.com/passage/?search=%s&version=NTV")
        ("BibleGateway (NVI)"                    . "https://www.biblegateway.com/passage/?search=%s&version=NVI")
        ("BibleGateway (RV1960)"                 . "https://www.biblegateway.com/passage/?search=%s&version=RV1960")
        ("BibleGateway (RV1960, NVI, NTV)"       . "https://www.biblegateway.com/passage/?search=%s&version=RVR1960,NVI,NTV")
        ("Debian Package Tracker"                . "https://tracker.debian.org/pkg/%s")
        ("Duck Duck Go"                          . "https://duckduckgo.com/html/?q=%s")
        ("Flathub Flatpak"                       . "https://flathub.org/apps/search?q=%s")
        ("Google Maps"                           . "https://www.google.com/maps/place/%s")
        ("Google Scholar"                        . "https://scholar.google.com/scholar?q=%s")
        ("Google Translate (English to Spanish)" . "https://translate.google.com/?sl=en&tl=es&text=%s")
        ("Google Translate (Spanish to English)" . "https://translate.google.com/?sl=es&tl=en&text=%s")
        ("Google"                                . "https://www.google.com/search?q=-reddit+-youtube+%s&udm=14")
        ("Internet Archive"                      . "https://archive.org/search?query=%s")
        ("Lectulandia"                           . "https://ww3.lectulandia.com/search/%s")
        ("LibGen Library Genesis"                . "http://libgen.is/search.php?req=%s")
        ("mail.ru"                               . "https://my.mail.ru/video/search?q=%s")
        ("Open Street Map"                       . "https://www.openstreetmap.org/search?query=%s")
        ("Oxford definition"                     . "https://www.oxfordlearnersdictionaries.com/us/definition/english/%s_1")
        ("Pip package"                           . "https://pypi.org/search/?q=%s")
        ("Python3 official documentation"        . "https://docs.python.org/3/search.html?q=%s")
        ("RAE"                                   . "https://dle.rae.es/%s")
        ("ScienceDirect Topics"                  . "https://www.sciencedirect.com/topics?searchPhrase=%s")
        ("Weather wttr.in"                       . "https://wttr.in/%s")
        ("Wikipedia"                             . "https://es.wikipedia.org/wiki/%s")
        ("Xah Emacs Search"                      . "http://xahlee.info/emacs/emacs_search.html?q=%s")
        ("Yandex"                                . "https://yandex.com/search/?text=%s")
        ("YouTube (Yewtu.be)"                    . "https://yewtu.be/search?q=%s")
        ("YouTube Music"                         . "https://music.youtube.com/search?q=%s")
        ("YouTube"                               . "https://youtube.com/results?search_query=%s")
        ("Emacs Wiki"                            . "https://duckduckgo.com/?q=site:emacswiki.org %s")
        ))

(defun my-lookup (service-name)
  "Generic lookup function that searches SERVICE-NAME from 'my-search-services'.
Prompts for a search term and opens the browser."
  (let* (
         ;; Get the url depending on 'service-name' and 'my-search-services'.
         (url (cdr (assoc service-name my-search-services)))
         )
    (if (not url)
        (message "Selected service (%s) is not listed in 'my-search-services' variable." service-name)
      (let* ((search-term (my-get-search-term
                           (format "Lookup on %s: " service-name)))
             (full-url (format url search-term)))
        (if current-prefix-arg
            (eww full-url)
          (browse-url full-url))))))

(defun my-lookup-search-web ()
  "Prompt for a service, then search using the selected service."
  (interactive)
  ;; Get `service' to search for (use the var 'my-search-services')
  (my-lookup (completing-read "Select service: " (mapcar 'car my-search-services) nil t nil nil "Duck Duck Go")))

(provide 'my-lookup)
;;; my-lookup.el ends here
