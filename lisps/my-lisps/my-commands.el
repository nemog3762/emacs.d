;;; my-common.el --- my common functions for Emacs -*- lexical-binding: t; -*-

;;;; Functions general

;;; reload-emacs
(defun reload-emacs ()
  "Reload Emacs config. Not the same than 'restart-emacs'"
  (interactive)
  (load-file (expand-file-name "init.el" user-emacs-directory)))

;;; my-get-search-term
(defun my-get-search-term (&optional string)
  "Get a string, the selected text or the word under the cursor."
  (interactive)
  (let ((search-term
         (if (use-region-p) ;; Si hay texto seleccionado, escoger la región
             (buffer-substring-no-properties (region-beginning) (region-end))
           ;; Sino, ingresar texto y recomendar la palabra bajo el cursor
           (read-string (or string "String?: ") (thing-at-point 'word)))))
    ;; Si search-term está vacío, salir, sino, retornar
    (if (string-empty-p search-term) (keyboard-quit) search-term)))

;;; my-copy-yank-to-clipboard
(defun my-copy-yank-to-clipboard ()
  "Copy the current directory into the kill ring."
  (interactive)
  (let* (
         ;; Get the file name or bufer name
         (filename (cond
                    ((derived-mode-p 'dired-mode) (dired-get-file-for-visit))
                    ((buffer-file-name) (buffer-file-name))
                    (t nil)))
         ;; Ask the name, path or content
         (choice (when filename
                   (completing-read
                    "Copy name, pathor content? "
                    '("name (nombre)" "path (ruta)" "content (contenido)"))))
         ;; Selecciona el texto a copiar
         (text-to-copy (cond
                        ((string-equal choice "name (nombre)")
                         (file-name-nondirectory filename))
                        ((string-equal choice "path (ruta)")    filename)
                        ((string-equal choice "content (contenido)")
                         ;; Lee el contenido del archivo
                         (with-temp-buffer
                           (insert-file-contents filename)
                           (buffer-string)))
                        (t nil))))
    ;; Copiar texto
    (when text-to-copy
      (kill-new text-to-copy))))

;;; my-color-picker
(defun my-color-picker ()
  "Use 'read-color' to select a color and insert it in the chosen format.
The user can select between hexadecimal (#RRGGBB), RGB (rgb(num1, num2, num3)), or the name of the color."
  (interactive)
  (let* ((color-name (read-color "Choose a color: "))
         (rgb-value (color-name-to-rgb color-name)))
    (if rgb-value
        (let* ((hex-string (apply 'format "\"#%02x%02x%02x\""
                                  (mapcar (lambda (x) (round (* 255 x))) rgb-value)))
               (rgb-string (apply 'format "rgb(%d, %d, %d)"
                                  (mapcar (lambda (x) (round (* 255 x))) rgb-value)))
               (choice (completing-read "Choose format: "
                                        '("Hexadecimal (#RRGGBB)"
                                          "RGB (rgb(num1, num2, num3))"
                                          "Color Name"))))
          (cond
           ((string-equal choice "Hexadecimal (#RRGGBB)")
            (insert hex-string))
           ((string-equal choice "RGB (rgb(num1, num2, num3))")
            (insert rgb-string))
           ((string-equal choice "Color Name")
            (insert color-name))))
      (message "Could not convert color to a usable format!"))))

;;; my-toggle-notes
(defun my-toggle-notes ()
  "Toggle to '~/my-notes.txt' (or equivalent depending on the OS) file.
If the file is open in the current buffer, return to the last buffer."
  (interactive)
  (let ((notes-file (cond
                     ((eq system-type 'windows-nt) "D:\\Google Drive\\symlinks\\my-notes.txt")
                     ((eq system-type 'gnu/linux)  "~/my-notes.txt")
                     ((eq system-type 'darwin)     "~/Desktop/my-notes.txt")))
        (notes-buffer (find-buffer-visiting (cond
                                              ((eq system-type 'windows-nt) "D:\\Google Drive\\symlinks\\my-notes.txt")
                                              ((eq system-type 'gnu/linux)  "~/my-notes.txt")
                                              ((eq system-type 'darwin)     "~/Desktop/my-notes.txt")))))
    (if (and notes-buffer (eq (current-buffer) notes-buffer))
        (switch-to-buffer (other-buffer))
      (find-file notes-file))))

;;; my-download-file-sync
(defun my-download-file-sync (&optional url download-dir download-name) ()
       "Get a file from a url using url-copy-file
https://stackoverflow.com/questions/4448055/download-a-file-with-emacs-lisp"
       (interactive)
       (let* ((url (or url (read-string "Enter download URL: ")))
              (download-dir (or download-dir "~/Descargas/"))
              (download-name (or download-name (car (last (split-string url "/" t)))))
              (full-path (concat download-dir download-name)))
         ;; Check
         (if (not (file-exists-p full-path))
             (url-copy-file url full-path t t)
           (message "%s existe" full-path))))

;;; my-jump-to-file-or-directory
(defun my-jump-to-file-or-directory ()
  "Search file or directory. Open in dired if directory, or jump to file in dired."
  (interactive)
  (let* (
         ;; Lista de archivos y directorios recursivamente
         ;; Seleccionar uno de la lista
         (selection (completing-read "Select file/dir: "
                                     (directory-files-recursively default-directory "." t)))
         ;; Expandir ruta seleccionada con respecto al directorio actual
         (full-path (expand-file-name selection default-directory)))
    ;; Verificar si selección es un directorio
    (if (file-directory-p full-path)
        ;; Abrir en dired si es directorio
        (dired full-path)
      ;; Si es un archivo, abrir en dired el directorio que lo contiene
      (dired (file-name-directory full-path))
      ;; Mover el cursor al inicio del buffer dired
      (goto-char (point-min))
      ;; Buscar el archivo dentro del buffer y posiciona el cursor en el
      (search-forward (file-name-nondirectory full-path) nil t))))

;;; my-new-file
(defun my-new-file ()
  "Create an empty file and open it.
The filename is the current date and hour"
  (interactive)
  (find-file (make-empty-file (format "%s/%s.txt"
                                      (cond
                                       ((eq system-type 'windows-nt) "D:")
                                       ((eq system-type 'gnu/linux)  "~")
                                       ((eq system-type 'darwin)     "~/Desktop"))
                                      (format-time-string "%Y-%m-%d-%H-%M-%S")))))

;;; my-count-words-in-paragraph
(defun my-count-words-in-paragraph ()
  "Count the number of words in the current paragraph."
  (interactive)
  (save-excursion
    (let (start end)
      ;; Move to the beginning and end of the paragraph
      (backward-paragraph)
      (setq start (point))
      (forward-paragraph)
      (setq end (point))
      ;; Use count-words-region to count the words between start and end
      (message "Words in paragraph: %d" (count-words-region start end)))))

;;; my-find-characters-non-ascii
(defun my-find-characters-non-ascii ()
  "Find any non-ascii characters in the current buffer."
  (interactive)
  (occur "[^[:ascii:]]"))

;;; my-find-characters-non-utf8
(defun my-find-characters-non-utf8 ()
  "Find any non-UTF-8 characters in the current buffer."
  (interactive)
  (occur "[^[:print:]\n\t\r]"))

;;; my-random-thoughts
(defvar my-random-thoughts '(
                             "80/20 Rule: Identify the 20% of activities that lead 80% of your results."
                             "Ask 'why' 5 times to get to the root cause of any problem."
                             "Eat the elephant: Break complex problems into smaller, manageable parts."
                             "Occam's Razor: Prefer the simplest explanation that fits the facts."
                             "Consider future impacts by asking, 'What comes next?'"
                             "Study cognitive blases so you can identify and outsmart your blases regularly."
                             "Seek intellectual sparring partners with diverse views to challenge your assumptions."
                             "What gets measured gets managed.' Peter Drucker."
                             "Factorin muances. 'Not everything that counts can be counted.' Albert Einstein"
                             "Invert problems: Growth also comes from avoiding mistakes and bad habits."
                             "Practice morning journaling; use writing as tool to clarify your thinking."
                             "Adopt probabilisic thinking: Weigh decisions based on likelihood and risk."
                             "Understand opportunity cost. Consider the next best alternative you're giving up."
                             "Practice steel manning: Argue agalnst the strongest version of opposing views."
                             "Master compounding: 1% daily Improvement leads to 37x growth in one year."
                             "'The greatest enemy of knowledge is the ilusion of knowledge.' Stephien Hawking"
                             )
  "List of phrases of random thoughts.")
(defun my-random-thoughts ()
  "Set a random phrase from 'my-random-thoughts'."
  (interactive)
  (message (format "%s" (nth (random (length my-random-thoughts)) my-random-thoughts))))

(defun my-gitlab-open-material-de-asignaturas-web ()
  "Open web from git 'materual-de-asignaturas'"
  (interactive)
  (browse-url "https://nemog3762.gitlab.io/material-de-asignaturas"))

;;;; Functions edit

;;; my-match-paren-goto
(defun my-match-paren-goto (arg)
  "Go to the matching delimiter or the previous one if not on a delimiter.
Similar to vi's % behavior."
  (interactive "p")
  (cond
   ;; Si el cursor está en un delimitador de apertura
   ((looking-at "[\[\(\{]") (forward-sexp))
   ;; Si el cursor está en un delimitador de cierre
   ((looking-back "[\]\)\}]" 1) (backward-sexp))
   ;; Si el cursor está dentro y justo antes de un delimitador de cierre
   ((looking-at "[\]\)\}]") (forward-char) (backward-sexp))
   ;; Si el cursor está dentro y justo después de un delimitador de apertura
   ((looking-back "[\[\(\{]" 1) (backward-char) (forward-sexp))
   ;; Buscar el delimitador anterior si no está en un delimitador
   (t (backward-up-list 1 'ESCAPE-STRINGS 'NO-SYNTAX-CROSSING))))
(global-set-key (kbd "C-{") 'my-match-paren-goto)

;;; my-match-paren-select
(defun my-match-paren-select ()
 "Select the text between matching parentheses, brackets, or braces."
  (interactive)
  (cond
   ((looking-at "[\[\(\{]")       ; Si estamos en el inicio de un paréntesis
    (forward-sexp)                  ; Avanzamos al final de la expresión
    (set-mark (point))              ; Establecemos la marca al punto actual
    (backward-sexp))                ; Retrocedemos al inicio de la expresión, seleccionando el texto
   ((looking-back "[\]\)\}]" 1)     ; Si estamos al final de un paréntesis
    (backward-sexp)                 ; Retrocedemos al inicio de la expresión
    (set-mark (point))              ; Establecemos la marca
    (forward-sexp))                 ; Avanzamos al final de la expresión, seleccionando el texto
   (t nil)))                        ; Si no estamos sobre paréntesis, no hacer nada
(global-set-key (kbd "C-}") 'my-match-paren-select)

;;; my-remove-carriage-return-cr
(defun my-remove-carriage-return-cr ()
  "Delete carriage return from clipboard."
  (interactive)
  (with-temp-buffer
    (yank)
    (replace-regexp "\n" " " nil (point-min) (point-max))
    (kill-ring-save (point-min) (point-max))))

;;; my-edit-script-dotfiles-es
(defun my-edit-script-dotfiles-es ()
  "Select a dotfile from ~/gits/dots and edit it."
  (interactive)
  (let ((path "~/gits/dots")) ;; Define el directorio base
    (if (file-directory-p path)
        (let* (
               ;; Lista de archivos en el directorio
               (file-list (directory-files-recursively path ".*" nil))
               ;; Seleccionar archivo de la lista
               (selection (completing-read "Select file: " file-list))
               ;; Expande la ruta completa
               (full-path (expand-file-name selection path)))
          (if (file-regular-p full-path) ;; Verifica si es un archivo regular
              (find-file full-path) ;; Abre el archivo
            (dired full-path))) ;; Si no es un archivo, abre en dired
      (message "Path %s doesn't exist!" path))))

;;; my-sudo-edit
(defun my-sudo-edit (&optional arg)
  "Edit currently visited file as root.
With a prefix ARG prompt for a file to visit.
Will also prompt for a file to visit if current
buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

;;; my-comment-or-uncomment
(defun my-comment-or-uncomment ()
  "Comments or uncomments the current line or region.
https://emacs.dyerdwelling.family/emacs/20230215204855-emacs--commenting-uncommenting/"
  (interactive)
  (if (region-active-p)
    (comment-or-uncomment-region
      (region-beginning)(region-end))
    (comment-or-uncomment-region
      (line-beginning-position)(line-end-position))))

;;; my-remove-duplicate-blank-lines
(defun my-remove-duplicate-blank-lines (start end)
  "Replace multiple blank lines with a single blank line.
By default, applies to the entire buffer. If called with C-u, applies to the selected region."
  (interactive
   (if (use-region-p)
       (list (region-beginning) (region-end))
     (list (point-min) (point-max))))
  (save-excursion
    (goto-char start)
    (while (re-search-forward "\n\\(\n\\)+" end t)
      (replace-match "\n\n")))
  (message "Duplicate blank lines removed in %s."
           (if (use-region-p) "region" "buffer")))

;;; my-mark-paragraph
(defun my-mark-paragraph ()
  "Redefinition of `mark-paragraph'"
  (interactive)
  (mark-paragraph -1))
(global-set-key (kbd "M-h") 'my-mark-paragraph)

;;; my-mark-word
(defun my-mark-word ()
  "Redefinition of `mark-word'"
  (interactive)
  (backward-to-word 1)
  (forward-to-word 1)
  (push-mark)
  (forward-word)
  (setq mark-active t))
(global-set-key (kbd "M-@") 'my-mark-word)

;; ;;; my-sort-lines
;; (defun my-sort-lines ()
;;   "Redifinition of 'sort-lines'"
;;   (interactive)
;;   (sort-lines t)

;;;; Functions for terminals

;;; my-terminal-here
(defun my-terminal-here ()
  "Open a terminal in the current directory.
Works on Windows, macOS, and Linux.
TODO in GNU/Linux detect if the user uses bash, zsh or fish.
"
  (interactive)
  (let ((current-directory (expand-file-name default-directory)))
    (cond
     ((eq system-type 'windows-nt)  ;; Windows
      (start-process "cmd" nil "cmd.exe" "/c" "start" "cmd.exe" "/K"
                     (concat "cd /d " current-directory)))
     ((eq system-type 'darwin)      ;; macOS
      (start-process "terminal" nil "open" "-a" "Terminal" current-directory))
     ((eq system-type 'gnu/linux)   ;; GNU/Linux
      (call-process shell-file-name nil 0 nil
                    shell-command-switch "x-terminal-emulator")))))

;;; my-terminal-in
(defun my-terminal-in ()
  "Open a terminal in a directory
TODO Works on GNU/Linux, maybe in macOS, fix for Windows
TODO in GNU/Linux detect if the user uses bash, zsh or fish."
  (interactive)
  (let* (
         ;; Define command
         (find-command
          (concat "find $HOME -type d"))
         ;; Execute command
         (results (split-string (shell-command-to-string find-command) "\n" t))
         ;; Solicitar al usuario que seleccione un archivo o directorio
         (current-directory (completing-read "Select file/dir: " results nil t))
         )
    (when (and current-directory (not (string-empty-p current-directory)))
      (cond
       ((eq system-type 'windows-nt)
        (start-process "cmd" nil "cmd.exe" "/c" "start" "cmd.exe" "/K"
                       (concat "cd /d " current-directory)))
       ((eq system-type 'darwin)
        (start-process "terminal" nil "open" "-a" "Terminal" current-directory))
       ((eq system-type 'gnu/linux)
        (call-process shell-file-name nil 0 nil
                      shell-command-switch (format "cd '%s'; x-terminal-emulator" current-directory)))))))

;;; my-terminal-here-tmux
(defun my-terminal-here-tmux ()
  "Open x-terminal-emulator in tmux session
    Based on https://azzamsa.com/n/scripts-el/"
  (interactive)
  (start-process
   "tmux" nil "setsid" "--fork" "x-terminal-emulator" "-e" "bash"
   "-c" "tmux -q has-session && exec tmux new-window || exec tmux new-session -n$USER -s$USER@$HOSTNAME"))

;;;; Functions for frame

;;; my-transparency-frame
(defun my-transparency-frame ()
  "Set frame transparency using true transparency
This don't work well with 'corfu-mode'"
  (interactive)
  (let* ((value (read-number "Value for transparency: " 100)))
    (set-frame-parameter nil 'alpha-background value)
    (add-to-list 'default-frame-alist '(alpha-background . value))))

;;;; Functions for windows

;;; my-window-split-toggle
(defun my-window-split-toggle ()
  "Toggle between horizontal and vertical split with TWO windows."
  (interactive)
  (if (> (length (window-list)) 2)
      (error "Can't toggle with more than 2 windows!")
    (let ((func (if (window-full-height-p)
                    #'split-window-vertically
                  #'split-window-horizontally)))
      (delete-other-windows)
      (funcall func)
      (save-selected-window
        (other-window 1)
        (switch-to-buffer (other-buffer))))))

;;; my-window-toggle-cycle-layout
(progn
  (defvar my-window-layout-state 0
    "State variable to track the current window layout.")
  (defun my-window-toggle-cycle-layout ()
    "Toggle between different window layouts:
1. One window.
2. Split horizontally.
3. Split vertically.
4. Tree windows.
5. Four windows."
    (interactive)
    ;; Cycle between states (number == states -1)
    (setq my-window-layout-state (mod (1+ my-window-layout-state) 6))
    ;; Delete all windows first
    (delete-other-windows)
    (cond
     ((= my-window-layout-state 0)
      (delete-other-windows)
      (message "Config %s: One window layout" my-window-layout-state))
     ((= my-window-layout-state 1)
      (split-window-below) (windmove-down) (next-buffer)
      (windmove-up)
      (message "Config %s: Two windows split horizontally" my-window-layout-state))
     ((= my-window-layout-state 2)
      (split-window-right) (windmove-right) (next-buffer)
      (windmove-left)
      (message "Config %s: Two windows split vertically" my-window-layout-state))
     ((= my-window-layout-state 3)
      (split-window-right) (windmove-right) (next-buffer)
      (split-window-below) (windmove-down)  (next-buffer) (next-buffer)
      (windmove-left)
      (message "Config %s: Three windows" my-window-layout-state))
     ((= my-window-layout-state 4)
      (split-window-below) (windmove-down) (next-buffer)
      (split-window-right) (windmove-right) (next-buffer) (next-buffer)
      (windmove-up)
      (message "Config %s: Three windows" my-window-layout-state))
     ((= my-window-layout-state 5)
      (split-window-right) (windmove-right) (next-buffer)
      (split-window-below) (windmove-down) (next-buffer) (next-buffer)
      (windmove-left)
      (split-window-below) (windmove-down)  (next-buffer) (next-buffer)  (next-buffer)
      (windmove-up)
      (message "Config %s: Four windows" my-window-layout-state))
     )))

;;; my-windows-swap-two
(defun my-window-swap-two ()
  "Swap the contents of the two windows."
  (interactive)
  (if (= (count-windows) 2)
      (let* ((win1 (nth 0 (window-list)))
             (win2 (nth 1 (window-list)))
             (buf1 (window-buffer win1))
             (buf2 (window-buffer win2))
             (start1 (window-start win1))
             (start2 (window-start win2)))
        (set-window-buffer win1 buf2)
        (set-window-buffer win2 buf1)
        (set-window-start win1 start2)
        (set-window-start win2 start1))
    (message "You need exactly two windows to do this.")))

;;; my-window-quick-jump
;; Credits:
;; V1: https://emacs.dyerdwelling.family/emacs/20241206143221-emacs--emacs-core-emacs-init-without-external-packages/
;; V2: https://emacs.dyerdwelling.family/emacs/20241209085935-emacs--emacs-core-window-jumping-visual-feedback/
;; V3: https://emacs.dyerdwelling.family/emacs/20241213115239-emacs--emacs-core-window-jumping-between-two-windows/
;; V4: Minor changes
(progn
  (defun my-window-quick-jump ()
    "Jump to a window by typing its assigned character label.
  If there are only two windows, jump directly to the other window."
    (interactive)
    (let* ((window-list (window-list nil 'no-mini)))
      ;; If only 1 window, exit
      (if (= (length window-list) 1)
          (keyboard-quit))
      (if (= (length window-list) 2)
          ;; If there are only two windows, switch to the other one directly.
          (select-window (other-window-for-scrolling))
        ;; Otherwise, show the key selection interface.
        (let* ((my/quick-window-overlays nil)
               (sorted-windows (sort window-list
                                     (lambda (w1 w2)
                                       (let ((edges1 (window-edges w1))
                                             (edges2 (window-edges w2)))
                                         (or (< (car edges1) (car edges2))
                                             (and (= (car edges1) (car edges2))
                                                  (< (cadr edges1) (cadr edges2))))))))
               (window-keys (seq-take '("h" "j" "k" "l" "a" "s" "d" "f")
                                      (length sorted-windows)))
               (window-map (cl-pairlis window-keys sorted-windows)))
          (setq my/quick-window-overlays
                (mapcar (lambda (entry)
                          (let* ((key (car entry))
                                 (window (cdr entry))
                                 (start (window-start window))
                                 (overlay (make-overlay start start (window-buffer window))))
                            (overlay-put overlay 'after-string
                                         (propertize (format " [%s] " key)
                                                     'face '(:foreground "white" :background "blue" :weight bold)))
                            (overlay-put overlay 'window window)
                            overlay))
                        window-map))
          (let ((key (read-key (format "Select window [%s]: " (string-join window-keys ", ")))))
            (mapc #'delete-overlay my/quick-window-overlays)
            (setq my/quick-window-overlays nil)
            (when-let ((selected-window (cdr (assoc (char-to-string key) window-map))))
              (select-window selected-window)))))))
  (global-set-key (kbd "M-s") #'my-window-quick-jump))

;;;; Functions for buffers

;;; my-buffer-next-user
(defun my-buffer-next-user ()
  "Change to the next user buffer"
  (interactive)
  (let ((current-buffer (current-buffer)))
    (while (progn
             (next-buffer)
             (and (string-prefix-p "*" (buffer-name))
                  (not (eq (current-buffer) current-buffer)))))))

;;; my-buffer-previous-user
(defun my-buffer-previous-user ()
  "Chante to the previous user buffer"
  (interactive)
  (let ((current-buffer (current-buffer)))
    (while (progn
             (previous-buffer)
             (and (string-prefix-p "*" (buffer-name))
                  (not (eq (current-buffer) current-buffer)))))))

;;; my-buffer-show-list
(defun my-buffer-show-list ()
  "Display the list of buffer names in the minibuffer.
The current buffer is highlighted with a black background and white foreground,
while the rest are displayed with normal text. Each buffer name is enclosed in brackets.
Buffers that start with a space and an asterisk or are named '*Buffer List*' are excluded."
  (interactive)
  (let* ((current-buffer-name (buffer-name))
         (buffer-names
          (mapcar (lambda (buf)
                    (let ((name (buffer-name buf)))
                      ;; Exclude buffers starting with " *" or named "*Buffer List*"
                      (unless (or (string-prefix-p " *" name)
                                  (string= name "*Buffer List*"))
                        (if (string= name current-buffer-name)
                            (propertize (format "[%s]" name)
                                        'face '(:background "black" :foreground "white"))
                          (format "[%s]" name)))))
                  (buffer-list)))
         ;; Remove nil values caused by excluded buffers
         (filtered-buffer-names (delq nil buffer-names))
         (message-text (string-join filtered-buffer-names " ")))
    (message "%s" message-text)))

;;; my-switch-to-last-buffer (alias to spacemacs/alternate-buffer)
;; https://emacs.stackexchange.com/questions/64796/switch-to-the-most-recently-selected-buffer-in-the-current-window
(progn
  (defun spacemacs/alternate-buffer (&optional window)
    "Switch back and forth between current and last buffer in the current window."
    (interactive)
    (let ((current-buffer (window-buffer window))
          (buffer-predicate
           (frame-parameter (window-frame window) 'buffer-predicate)))
      ;; switch to first buffer previously shown in this window that matches
      ;; frame-parameter `buffer-predicate'
      (switch-to-buffer
       (or (cl-find-if (lambda (buffer)
                         (and (not (eq buffer current-buffer))
                              (or (null buffer-predicate)
                                  (funcall buffer-predicate buffer))))
                       (mapcar #'car (window-prev-buffers window)))
           ;; `other-buffer' honors `buffer-predicate' so no need to filter
           (other-buffer current-buffer t)))))
  (defalias 'my-switch-to-last-buffer 'spacemacs/alternate-buffer)
  (global-set-key (kbd "C-|") #'my-switch-to-last-buffer))

;;; my-buffer-kill-others
(defun my-buffer-kill-others ()
  "Kill all other buffers besides the current one."
  (interactive)
  (mapc 'kill-buffer (delq (current-buffer) (buffer-list))))

(defun my-buffer-kill-buffers-emacs ()
  "Kill all buffers whose names start with '*' or 'magit', except '*Messages*'."
  (interactive)
  (dolist (buffer (buffer-list))
    (let ((buffer-name (buffer-name buffer)))
      (when (and (or (string-prefix-p "*" buffer-name)
                     (string-prefix-p "magit" buffer-name))
                 (not (string= buffer-name "*Messages*")))
        (kill-buffer buffer))))
  (message "Closed all star and magit buffers except '*Messages*'."))

;;; my-revert-this-buffer
(defun my-revert-this-buffer ()
  "Revert the current buffer."
  (interactive)
  (unless (minibuffer-window-active-p (selected-window))
    (revert-buffer t t)
    (message "Reverted this buffer")))

;;; my-buffer-save-to-txt
(defun my-buffer-save-to-txt (filename)
  "Save buffer to .txt."
  (interactive "FFile name: ")
  ;; Use write-region with (point-min) and (point-max) to get all content.
  (write-region (point-min) (point-max) filename)
  (message "Buffer saved: %s" filename))

;;; my-buffer-new-scratch
(defun my-buffer-new-scratch ()
  "Crea un nuevo scratch buffer con un nombre único."
  (interactive)
  (let ((buffer-name (generate-new-buffer-name "*scratch*")))
    (switch-to-buffer (generate-new-buffer buffer-name))
    (fundamental-mode)))

;;;; Functions for files

;;; my-file-show-file-name
(defun my-file-show-file-name ()
  "Show the full path to the current file in the minibuffer.
https://camdez.com/blog/2013/11/14/emacs-show-buffer-file-name/"
  (interactive)
  (let ((file-name (buffer-file-name)))
    (if file-name
        (progn
          (message file-name)
          (kill-new file-name))
      (message "Buffer not visiting a file"))))

;;; my-file-delete-this-file
(defun my-file-delete-this-file ()
  "Delete the current file, and kill the buffer."
  (interactive)
  (unless (buffer-file-name)
    (error "No file is currently being edited"))
  (when (yes-or-no-p (format "Really delete '%s'?"
                             (file-name-nondirectory buffer-file-name)))
    (delete-file (buffer-file-name))
    (kill-this-buffer)))

;;; my-file-rename-this-file
(defun my-file-rename-this-file (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (unless filename
      (error "Buffer '%s' is not visiting a file!" name))
    (progn
      (when (file-exists-p filename)
        (rename-file filename new-name 1))
      (set-visited-file-name new-name)
      (rename-buffer new-name))))

;;; my-file-duplicate-backup
(defun my-file-duplicate-backup ()
  "Create backup of current file with format 'name_AAAA-MM-DD-HH-MM-SS'."
  (interactive)
  (let* ((current-file (or (and (derived-mode-p 'dired-mode) (dired-get-file-for-visit))
                          (buffer-file-name)))
         (backup-name (concat (file-name-sans-extension current-file)
                             "-" (format-time-string "%Y-%m-%d-%H-%M-%S") "-backup"
                             (file-name-extension current-file t))))
    (if (file-directory-p current-file)
        (copy-directory current-file backup-name)
      (copy-file current-file backup-name t))
    (revert-buffer)
    (message "New file: %s" backup-name)))

(provide 'my-common)
;;; my-common.el ends here
