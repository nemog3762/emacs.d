;;; my-org.el --- my org configs -*- lexical-binding: t; -*-

;;;; Alias
(defalias 'my-org-table-toggle-coordinates 'org-table-toggle-coordinate-overlays)
(defalias 'my-org-show-headigs-by-tag      'org-match-sparse-tree)
(defalias 'my-org-insert-tag               'org-set-tags-command)
(defalias 'my-org-delete-subtree           'org-cut-special)
(defalias 'org-subtree-promote-level       'org-shiftmetaleft)
(defalias 'org-subtree-demote-level        'org-shiftmetaright)

;;;; Faces

;; 'org-mode-restart' for changes to take effect in already visited Org files
;; https://www.gnu.org/software/emacs/manual/html_node/modus-themes/Custom-Org-emphasis-faces.html
(setq org-todo-keyword-faces
      '(
        ("TODO" . (:background "IndianRed1"   :foreground "white" :weight bold))
        ("DONE" . (:background "light green" :foreground "blue" :weight bold))
        ))

(custom-set-faces
;;; Others
 '(org-verbatim  ((t (:background "gray91" :extend t))))
 '(org-table     ((t (:foreground "black" :background "AliceBlue"))))
;;; Headers
 '(org-level-1 ((t (:weight normal :height 1.2 :foreground "orange"))))
 '(org-level-2 ((t (:inherit 'org-level-1 :foreground "blue"))))
 '(org-level-3 ((t (:inherit 'org-level-2 :foreground "red"))))
 '(org-level-4 ((t (:inherit 'org-level-3 :foreground "dark green"))))
 '(org-level-5 ((t (:inherit 'org-level-4 :foreground "orange"))))
 '(org-level-6 ((t (:inherit 'org-level-5 :foreground "blue"))))
 '(org-level-7 ((t (:inherit 'org-level-6 :foreground "red"))))
 '(org-level-8 ((t (:inherit 'org-level-7 :foreground "dark green"))))
;;; Org block
 '(org-block-begin-line ((t (:foreground "dark gray"     :weight normal :height 1.0 :background "gray91" :extend t :box nil))))
 '(org-block            ((t (:inherit fixed-pitch-serif  :weight normal :height 1.0 :background "gray91" :extend t))))
 )

;;;; Configs

;;; General configs

;; (setq org-pretty-entities t)                ;; Show specials characters like x_{2}
;; (setq org-use-sub-superscripts "{}")        ;; Show specials characters like x_{2}
(setq org-fontify-quote-and-verse-blocks t) ;; Don't use 'org-block' face for quote block and verse block
(setq org-fontify-done-headline nil)        ;; Don't change the face of header when is DONE
(setq org-fontify-emphasized-text t)        ;; Mostrar cursiva, *negrita*, etc.
(setq org-hide-emphasis-markers t)          ;; Ocultar marcadores como /, *, _. Mostrar con el paquete 'org-appear'
(setq org-support-shift-select t)           ;; Habilitar tecla =Shift= si se inicia selección fuera del =heading=
(setq org-startup-indented t)               ;; Indentar niveles.
(setq org-edit-src-content-indentation 0)   ;; No hacer indentaciones de código en bloques de código
(setq org-startup-with-inline-images nil)   ;; Mostrar por defecto las imágenes
(setq org-image-actual-width nil)           ;; Ajustar ancho de imágenes https://www.miskatonic.org/2016/08/25/image-display-size-in-org/
(setq org-list-allow-alphabetical t)        ;; Permitir en las listas A, B, C, etc...
(use-package htmlize :defer t)              ;; Al exportar, resaltar la sintaxis del código.
(setq org-format-latex-options              ;; Tamaño ecuación de LaTeX
      (plist-put org-format-latex-options :scale 1.5))
;; Better 'org-goto' (use completing-read)
;; https://emacs.stackexchange.com/questions/20759/all-org-subheadings-in-imenu
(setq  org-goto-interface 'outline-path-completionp)
(setq org-outline-path-complete-in-steps nil)

;;;; Exports

(defun my-org-export-to-all ()
  "Export a org file to .html, .pdf (latex), .tex and .odt"
  (interactive)
  ;; .html
  (let ((org-html-head my-org-export-custom-style-var))
    ;; .html
    (org-html-export-to-html)
    ;; .pdf
    (org-latex-export-to-pdf)
    ;; .tex
    (org-latex-export-to-latex)
    ;; .odp
    (org-odt-export-to-odt)
    ))

;;; Export for LaTeX

;; Run compile twice to show TOC in .pdf
;; https://emacs.stackexchange.com/a/76256
(setq org-latex-pdf-process
      '(
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        ))

;;; Execute code block with 'C-c_C-c'
;; http://orgmode.org/worg/org-contrib/babel/languages/ob-doc-python.html
;; Agregar Matlab como lenguaje permitido (se puede usar Scilab como alias)
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (emacs-lisp . t)
   (python     . t)
   (shell      . t)
   (C          . t) ;; Activa C (usado como Scilab)
   ))

;;; Pomodoro in org-mode
(setq org-timer-default-timer 10)
(setq org-clock-sound (expand-file-name   "bell.wav" user-emacs-directory))
(defalias 'org-pomodoro-set               'org-timer-set-timer)
(defalias 'org-pomodoro-pause-or-continue 'org-timer-pause-or-continue)
(defalias 'org-pomodoro-stop              'org-timer-stop)

;;;; Hooks

(add-hook 'org-mode-hook
          (lambda ()
            ;; Mejor visualización de líneas largas
            ;; No colocar flechas al final de la ventana
            (visual-line-mode)
            ;; Expandir y contraer títulos con click
            (require 'org-mouse)
            ;; <s TAB para insertar #+begin_src y #+end_src ... C-c C-,
            (require 'org-tempo)
            ;; Keys
            (local-set-key "\M-n" 'outline-next-heading)
            (local-set-key "\M-p" 'outline-previous-heading)
            ))

;;;; Functions

;;; Automatically mark DONE when all sub checkboxes are checked
(progn
  (defun ndk/checkbox-list-complete ()
    "Automatically mark DONE when all sub checkboxes are checked
https://lists.gnu.org/archive/html/emacs-orgmode/2012-02/msg00515.html
https://orgmode.org/worg/org-hacks.html#mark-done-when-all-checkboxes-checked "
    (save-excursion
      (org-back-to-heading t)
      (let ((beg (point)) end)
        (end-of-line)
        (setq end (point))
        (goto-char beg)
        (if (re-search-forward
             "\\[\\([0-9]*%\\)\\]\\|\\[\\([0-9]*\\)/\\([0-9]*\\)\\]" end t)
            (if (match-end 1)
                (if (equal (match-string 1) "100%")
                    ;; all done - do the state change
                    (org-todo 'done)
                  (org-todo 'todo))
              (if (and (> (match-end 2) (match-beginning 2))
                       (equal (match-string 2) (match-string 3)))
                  (org-todo 'done)
                (org-todo 'todo)))))))
  (eval-after-load 'org-list
    '(add-hook 'org-checkbox-statistics-hook
               (function ndk/checkbox-list-complete))))

;;; my-org-where-am-i
(defun my-org-where-am-i ()
  "Returns a string of headers indicating where point is in the current tree."
  (interactive)
  (if (derived-mode-p 'org-mode)
      (let* ((my-list (org-get-outline-path t)))
        (message "%s" (mapconcat 'identity my-list " * ")))
    (message "This function only works in Org mode.")))

;;; my-org-copy-block-to-clipboard
(defun my-org-copy-block-to-clipboard ()
  "Copy the content of the current code block."
  (interactive)
  (save-excursion
    (org-babel-mark-block)
    (kill-ring-save (region-beginning) (region-end))
    (message "Code block copied to clipboard.")))

;;; my-org-wrap-with-char
(defun my-org-wrap-with-char ()
  "Wrap the selected text with a character in Org mode.
Prompt the user for a single character and wrap the selected text with it."
  (interactive)
  (if (use-region-p)
      (let* ((char (read-char "Enter a character to wrap: "))
             (start (region-beginning))
             (end (region-end)))
        (goto-char end)
        (insert char)
        (goto-char start)
        (insert char))
    (message "No text selected!")))

;;; my-org-insert-file-link-recursively
(defun my-org-insert-file-link-recursively ()
  "Recursively select a file and insert it as an Org-mode link with a relative path."
  (interactive)
  (let* ((file (completing-read "Select file/dir: " (directory-files-recursively default-directory "." t))))
    (when (and file (file-exists-p file))
      ;; Calcula la ruta relativa
      (let ((relative-file (file-relative-name file default-directory)))
        ;; Inserta la ruta relativa como enlace
        (insert (format "[[file:%s]]" relative-file))))))

;;; my-org-export-with-custom-style

(setq my-org-export-custom-style-var "
<style>
  body {
      font-family: 'Latin Modern Roman', 'Liberation Serif', Garamond, Palatino, Georgia, serif;
      line-height: 1.3;
      font-size: 25px;
      /* main colors */
      background-color: white;
      color: black;
      /* margin of text */
      /* max-width: 100 px; */
      margin: 5%;
  }
  pre {
      line-height: 1.1;
  }
  /* headers */
  h1, h2, h3, h4, h5, h6 {
      /* text-align: center; */
      color: #2A6099;
  }
  /* emphasis */
  em, i {
      color: red;
  }
  /* list marker */
  li::marker, dt {
      color: red;
      font-weight : normal;
  }
  /* list unsorted */
  ul {
      line-height: 1.0;
      list-style-type: square;
  }
  /* code inline */
  code {
      font-family: 'Latin Modern Mono', 'Liberation Mono', mono;
      background-color: #EFEFEF;
      border-radius: 5px;
  }
  /* code in block */
  pre {
      font-family: 'Latin Modern Mono', 'Liberation Mono', mono;
      background-color: #efefef;
      padding: 10px;
      border-radius: 5px;
  }
  /* imagen */
  img {
      max-width: 50%;                            /* La imagen no excederá el ancho del contenedor */
      height: 50%;                               /* La altura se ajustará automáticamente según el ancho */
      transition: transform 0.3s ease-in-out;    /* Ejemplo de transición de transformación */
      border-radius: 1px;                        /* Ejemplo de bordes redondeados */
      display: block;                            /* Asegura que la imagen sea tratada como un bloque */
      margin: 0 auto;                            /* Centra horizontalmente la imagen */
  }
  /* mouseimage mouse over */
  img:hover {
      transform: scale(1.4);           /* Aumento de tamaño al pasar el cursor */
  }
  .figure-number {
      color: red;
      font-weight : bold;
      /* text-decoration: underline #FF0000; */
  }
</style>
")

(defun my-org-export-with-custom-style ()
  "Export Org file to HTML with a custom style block.
If C-u is pressed before execution, prompt to select an Org file to export."
  (interactive)
  (require 'ox-html)
  (let* ((custom-style my-org-export-custom-style-var)
         (org-html-head-extra custom-style)
         (file (if current-prefix-arg
                   (read-file-name "Select Org file to export: " nil nil t nil
                                   (lambda (f) (string-match-p "\\.org$" f)))
                 (buffer-file-name))))
    (if file
        (with-current-buffer (find-file-noselect file)
          (let ((html-file (org-html-export-to-html)))  ; Capturar la ruta del archivo HTML generado
            (browse-url html-file)))  ; Abrir el archivo HTML en el navegador
      (user-error "No file selected or current buffer is not associated with a file."))))

;;; my-org-image-from-...
(progn
  ;; Var - Path of sources
  (defvar my-org-image-from--dir "src/img/")
  ;; Helper function
  (defun my-org-image-from--generate-filename (dir)
  "Generate a filename based on the current timestamp in DIR, allowing user to override it with completion."
  (let* ((default-name (format-time-string "%Y-%m-%d-%H-%M-%S.png"))
         (existing-files (directory-files dir nil "\\.png$")) ; Obtener archivos PNG en el directorio
         (name (completing-read (format "Filename (default: %s): " default-name)
                                existing-files
                                nil
                                nil
                                nil
                                nil
                                default-name))
         (filename (concat dir name)))
    filename))
  ;; Helper function
  (defun my-org-image-from--ensure-directory (path)
    "create directory if it does not exist and user agrees"
    (when (and (not (file-exists-p path))
               (y-or-n-p
                (format "Directory %s does not exist. Create it?" path)))
      (make-directory path :parents)))
  ;; Imagen from clipboard
  (defun my-org-image-from-screenshot ()
    "Take a screenshot into a time stamped unique-named file
in the same directory as the org-buffer and insert a link to this file.
Based on:  https://stackoverflow.com/questions/17435995/paste-an-image-on-clipboard-to-emacs-org-mode-file-without-saving-it "
    (interactive)
    ;; Ensure path
    (my-org-image-from--ensure-directory (file-name-as-directory my-org-image-from--dir))
    (let* ((image-path (my-org-image-from--generate-filename (file-name-as-directory my-org-image-from--dir))))
      (cond
       ;; Take screenshot on macOS
       ((eq system-type 'darwin)    (call-process "screencapture" nil nil nil "-i" image-path))
       ;; Take screenshot on GNU/Linux
       ((eq system-type 'gnu/linux) (call-process "import" nil nil nil image-path))
       ;; Error
       (t (error "No compatible")))
      ;; Insert into file if correctly taken
      (if (file-exists-p image-path)
          (insert (concat "[[file:" image-path "]]")))))
  ;; Pasting image from clipboard into org-mode
  (defun my-org-image-from-clipboard ()
    "Paste screenshot from clipboard
Based on: https://mfz.github.io/posts/pasting_image_into_org-mode/ "
    (interactive)
    ;; Ensure path
    (my-org-image-from--ensure-directory (file-name-as-directory my-org-image-from--dir))
    (let* ((image-path (my-org-image-from--generate-filename (file-name-as-directory my-org-image-from--dir))))
      (message image-path)
      (shell-command-to-string
       (format "xclip -selection clipboard -t image/png -o > %s" image-path))
      (insert "[[file:" image-path "]]\n"))))

;;;; Funcionts snippets

(defun my-org-insert-include ()
  "Inserta una línea #+INCLUDE: en el buffer actual, permitiendo seleccionar cualquier archivo con ruta relativa."
  (interactive)
  (let* ((file (read-file-name "Selecciona un archivo .org: " nil nil t))
         (relative-file (file-relative-name file (file-name-directory (buffer-file-name)))))
    (when (string-match "\\.org$" file)
      (insert (format "#+INCLUDE: \"%s\"\n" relative-file)))))

(defun my-org-snippet-image-header ()
  "Inserta un encabezado de imagen con atributos para HTML, Org y LaTeX."
  (interactive)
  (insert "#+CAPTION: \n")
  (insert "#+ATTR_HTML: :width 500\n")
  (insert "#+ATTR_ORG: :width 50%\n")
  (insert "#+ATTR_LATEX: :placement [H] :float nil :width 0.85\\textwidth\n")
  (previous-line) (previous-line) (previous-line) (previous-line) (end-of-visual-line))

;;;; org-agenda

(defalias 'org-agenda-file-add    'org-agenda-file-to-front)
(defalias 'org-agenda-file-remove 'org-remove-file)

;; A week view spanning the current day
(setq org-agenda-start-on-weekday nil)
(setq org-agenda-start-day "-10d")
(setq org-agenda-span 30)

(provide 'my-org)
;;; my-org.el ends here
