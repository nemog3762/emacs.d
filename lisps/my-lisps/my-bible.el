;;; my-bible.el --- read the Bible in Emacs -*- lexical-binding: t; -*-

;;; Idea from: "Use Emacs to Insert Bible Verses from any Text" https://youtube.com/watch?v=5fj9ub4yflI

;;;; Variables

;;; Aux vars, *-last
(defvar pattern-last   nil   "Store the last pattern entered by the user.")
(defvar number-last    1     "Store the last `n' by the user to search versicles.")
(defvar chapter-last   1     "Store the last `n' by the user to search versicles.")
(defvar verse1-last    1     "Store the last `n' by the user to search versicles.")
(defvar verse2-last    1     "Store the last `n' by the user to search versicles.")
(defvar version-last   "ntv" "Guardar la última versión.")
(defvar book-last      "Génesis|Gén|Genesis")

;;; Other vars

(defvar txt-path               "~/.config/txt/bibles")
(setq version-list  '("textual" "nvi" "ntv" "rv1960"))
(setq book-list
      '(
        ;; 01
        ("Génesis|Gén|Gen|gen")
        ("Éxodo|Éxo|exo")
        ("Levítico|Lev|lev")
        ("Números|Núm|núm|num")
        ("Deuteronomio|Deut|deut")
        ("Josué|Jos|jos")
        ("Jueces|Jue|jue")
        ("Rut|Rut|rut")
        ("1 Samuel|1 Sam|1Sam")
        ("2 Samuel|2 Sam|2Sam")
        ;; 10
        ("1 Reyes|1 Rey|1Rey")
        ("2 Reyes|2 Rey|2Rey")
        ("1 Crónicas|1 Crón|1Crón|1Cro")
        ("2 Crónicas|2 Crón|2Crón|1Cro")
        ("Esdras|Esd|esd")
        ("Nehemías|Neh|neh")
        ("Ester|Est|est")
        ("Job|Job|job")
        ("Salmos|Sal|sal")
        ("Proverbios|Prov|prov")
        ;; 20
        ("Eclesiastés|Ecl|ecl")
        ("Cantares|Cant|cant")
        ("Isaías|Isa|isa")
        ("Jeremías|Jer|jer")
        ("Lamentaciones|Lam|lam")
        ("Ezequiel|Eze|eze")
        ("Daniel|Dan|dan")
        ("Oseas|Ose|ose")
        ("Joel|Joel|joel")
        ("Amós|Amós|amós")
        ;; 30
        ("Abdías|Abd|abd")
        ("Jonás|Joná|joná")
        ("Miqueas|Miq|miq")
        ("Nahúm|Nah|nah")
        ("Habacuc|Hab|hab")
        ("Sofonías|Sof|sof")
        ("Hageo|Hag|hag")
        ("Zacarías|Zac|zac")
        ("Malaquías|Mal|mal")
        ("Mateo|Mat|mat")
        ;; 40
        ("Marcos|Mar|mar")
        ("Lucas|Luc|luc")
        ("Juan|Juan|juan")
        ("Hechos|Hech|hech")
        ("Romanos|Rom|rom")
        ("1 Corintios|1 Cor|1 cor")
        ("2 Corintios|2 Cor|2 cor")
        ("Gálatas|Gál|gál")
        ("Efesios|Efe|efe")
        ("Filipenses|Fil|fil")
        ;; 50
        ("Colosenses|Col|col")
        ("1 Tesalonicenses|1 Tes|1 tes")
        ("2 Tesalonicenses|2 Tes|2 tes")
        ("1 Timoteo|1 Tim|1 tim")
        ("2 Timoteo|2 Tim|2 tim")
        ("Tito|Tito|tito")
        ("Filemón|Filem|filem")
        ("Hebreos|Heb|heb")
        ("Santiago|Sant|sant")
        ("1 Pedro|1 Ped|1 ped")
        ;;60
        ("2 Pedro|2 Ped|2 ped")
        ("1 Juan|1 Juan|1 juan")
        ("2 Juan|2 Juan|2 juan")
        ("3 Juan|3 Juan|3 juan")
        ("Judas|Jud|jud")
        ("Apocalipsis|Apoc|apoc")
        ))

;; ;;;; Funciones no interactivas

;; ;;; my-bible-insert-content
;; (defun my-bible-insert-content (version libro capitulo)
;;   "Muestra un capítulo o versículo específico en un buffer temporal.
;; Se recibe la versión de la biblia, el nombre del libro, el número del capítulo y, opcionalmente, el número del versículo.
;; Ejemplo de entrada:
;; (my-bible-insert-content ntv 1 1) ==> Mostrar en un buffer temporal de la ntv, del libro 1, el capítulo 1"
;;   (let* (
;;          (ruta-archivo (format "%s/%s.txt" txt-path version))
;;          (lineas (my-bible-file-to-list ruta-archivo))
;;          (contenido "")
;;          (buffer-temporal "*my-bible-contenido*")
;;          )
;;     ;; Recorre líneas compatibles con el regex ingresado
;;     (dolist (linea lineas)
;;       (let ((regex (format "^%s|%s*" libro capitulo)))
;;         ;; Añadir líneas que coinciden al contenido
;;         (when (string-match-p regex linea)
;;           (setq contenido (concat contenido (my-bible-traducir-linea linea) "\n")))))

;;     ;; Preservar el estado de las ventanas y mostrar el contenido en un buffer temporal
;;     (save-window-excursion
;;       (with-output-to-temp-buffer buffer-temporal
;;         (toggle-word-wrap)
;;         (goto-char (point-min))
;;         (princ contenido))

;;       ;; Preguntar al usuario si desea insertar el contenido en el cursor actual
;;       (unwind-protect
;;           (when (y-or-n-p "¿Desea insertar el contenido en la posición actual del cursor? ")
;;             (insert contenido))
;;         ;; Cerrar el buffer temporal después de que el usuario responda
;;         (kill-buffer buffer-temporal)))))

;; ;;;; Funciones interactivas

;; ;;; my-bible-insert-regex
;; (defun my-bible-insert-regex ()
;;   "Insertar resultados de regex en las Escrituras."
;;   (interactive)
;;   (let* (
;;          (version (my-bible-ask-version))
;;          (regex (read-string "Ingresa el regex: " pattern-last))
;;          (ruta-archivo (format "%s/%s.txt" txt-path version))
;;          (lineas (my-bible-file-to-list ruta-archivo))
;;          (contador 0)
;;          )
;;     ;; Filtrar e imprimir las líneas que coinciden con el regex
;;     (dolist (linea lineas)
;;       (when (string-match-p regex linea)
;;         (insert (concat (my-bible-traducir-linea linea) "\n"))
;;         (setq contador (1+ contador))))
;;     ;; Imprimir mensaje sobre la cantidad de versículos encontrados
;;     (message (format "Se encontraron %d versículos que coinciden con el patrón." contador))))

;; ;;; my-bible-get-chapter
;; (defun my-bible-get-chapter ()
;;   "Pedir datos de versión, libro, capítulo, versículo inicial, versículo capítulo final.
;; Si versículo inicial y final es igual a 0, significa todo el capítulo"
;;   (interactive)
;;   (let* (
;;          ;; Obtener información
;;          (version (my-bible-ask-version))
;;          (libro-num (my-bible-ask-book))
;;          (capitulo (my-bible-ask-chapter "Ingresar número de capítulo: "))
;;          ;; Convertir el número del libro al nombre
;;          (libro-nombre (my-bible-traducir-numero-a-nombre-libro libro-num))
;;          )
;;     ;; Imprimir código a buscar
;;     (message (format "[%s] %s %s (%s|%s|%s)" version libro-nombre capitulo version libro-num capitulo))
;;     ;; Insertar texto
;;     (my-bible-insert-content (format "%s" version) libro-num capitulo)))

;; ;;;; Funciones interactivas viejas

;; ;; (defun my-bible-verse-random-message ()
;; ;;   "Mostrar 1 versículos aleatorios del archivo especificado.
;; ;; TODO: Mostrar en el minibuffer más de una línea"
;; ;;   (interactive)
;; ;;   (let* (
;; ;;          (version (my-bible-preguntar-version))
;; ;;          (ruta-archivo (format "%s/%s.txt" txt-path version))
;; ;;          (lineas (my-bible-file-to-list ruta-archivo))
;; ;;          )
;; ;;     ;; Inserta n líneas aleatorias seleccionadas en el buffer actual
;; ;;     (let ((linea (nth (random (length lineas)) lineas)))
;; ;;       (message (my-bible-traducir-linea linea) "\n"))))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ???

;; (defun my-bible-get-verse (version libro capitulo versiculo)
;;   "Obtener un versículo partiendo de un formato específico.
;; Se recibe la versión de la biblia, número del libro, número del capítulo, número del versículo.
;; Ejemplo de entrada:
;; ntv 1 1 1 ==> ntv, libro 1, el capítulo 1, el versículo 1"
;;   (interactive)
;;   (let* (
;;          (ruta-archivo (format "%s/%s.txt" txt-path version))
;;          (lineas (my-bible-file-to-list ruta-archivo))
;;          (regex (format "^%s|%s|%s|" libro capitulo versiculo))
;;          )
;;     ;; Ingresar líneas que coínciden.
;;     (dolist (linea lineas)
;;       (when (string-match-p regex linea)
;;         (format (concat (my-bible-traducir-linea linea) "\n"))))))

;;;; Funciones no interactivas

;;; my-bible-ask-book
(defun my-bible-ask-book ()
  "Escoger el nombre de un libro de 'book-list' de la Biblia
Usa 'completing-read' para escoger dependiendo de 'book-list'"
  (let* (;; Preguntar por el libro
         (selection (completing-read "Escoge un libro: " book-list))
         ;; Dividir la selección por '|'y tomar el primer alias
         (book (car (split-string selection "|"))))
    ;; Retornar 'book'
    book))

;;; my-bible-book-number-to-name
(defun my-bible-book-number-to-name (numero-libro)
  "Al ingresar 'numero-libro' (entre 1 y 66), retornar el nombre del libro.
Test: (my-bible-book-number-to-name 47) > 2 Corintios"
  (let* (
         ;; De lista-de-libros, sacar el elemento por el index
         (libro (nth (1- numero-libro) book-list))
         ;; El comando anterior retorna una lista, sacar el elemento de la lista y tener un string
         (nombre-libro (car libro))
         ;; Del string, quedarse con el primer elemento al ser dividido por |
         (nombre-corto (substring nombre-libro 0 (string-match "|" nombre-libro)))
         )
    ;; Retornar el nombre del libro
    nombre-corto))

;;; my-bible-book-name-to-number
(defun my-bible-book-name-to-number (nombre-libro)
  "Dado un 'nombre-libro', retornar su número en la lista 'book-list'.
Test: (my-bible-book-name-to-number 'Juan'\)  "
  (let* ((index (cl-position-if
                 (lambda (libro)
                   ;; Revisar si `nombre-libro` está en alguna parte del string del libro
                   (string-match-p nombre-libro (car libro)))
                 book-list)))
    (if index
        (1+ index) ;; Retornar índice 1-based
      (message "El libro '%s' no se encontró en la lista" nombre-libro))))

;;; my-bible-ask-version
(defun my-bible-ask-version ()
  "Escoger una versión de la Biblia. Depende de la variable 'version-list'"
  ;; Preguntar por versión y retornarla
  (completing-read "Escoge una versión: " version-list))

;;; my-bible-ask-chapter
(defun my-bible-ask-chapter (&optional prompt)
  "Solicita al usuario que ingrese un número y muestra un mensaje con el número ingresado.
Se hace con el objetivo de poder usar ese número como capítulo o versículo al momento de buscar."
  (let ((numero (read-number (or prompt "Ingresa un número: ") chapter-last))) ;; Pedir un número con un mensaje opcional
    (setq chapter-last numero)                                                 ;; Actualizar la variable para futuros usos
    numero))                                                                   ;; Retornar

;;; my-bible-ask-verse1
(defun my-bible-ask-verse1 (&optional prompt)
  "Solicita al usuario que ingrese un número y muestra un mensaje con el número ingresado.
Se hace con el objetivo de poder usar ese número como capítulo o versículo al momento de buscar."
  (let ((numero (read-number (or prompt "Ingresa un número: ") verse1-last)))  ;; Pedir un número con un mensaje opcional
    (setq verse1-last numero)                                                  ;; Actualizar la variable para futuros usos
    numero))                                                                   ;; Retornar

;;; my-bible-ask-verse2
(defun my-bible-ask-verse2 (&optional prompt)
  "Solicita al usuario que ingrese un número y muestra un mensaje con el número ingresado.
Se hace con el objetivo de poder usar ese número como capítulo o versículo al momento de buscar."
  (let ((numero (read-number (or prompt "Ingresa un número: ") verse2-last)))  ;; Pedir un número con un mensaje opcional
    (setq verse2-last numero)                                                  ;; Actualizar la variable para futuros usos
    numero))                                                                   ;; Retornar

;;; my-bible-file-to-list
(defun my-bible-file-to-list (ruta-archivo)
  "Abre un archivo y convierte cada línea en una lista.
   No sé si se pueda mejorar."
  (with-temp-buffer
    (insert-file-contents ruta-archivo)
    (split-string (buffer-string) "\n" t)))

;;; my-bible-traducir-linea
(defun my-bible-traducir-linea (linea)
  "Convertir de formata una línea de la Biblia
Entrada: 47|12|1|Mi jactancia no servirá de nada, sin embargo, debo seguir adelante. ...
Salida:  2 Corintios 12:1 Mi jactancia no servirá de nada, sin embargo, debo seguir adelante. ...
"
  (let* (
         ;; Dividir de cada línea por |
         (split (split-string linea "|"))
         (libro (my-bible-book-number-to-name (string-to-number (nth 0 split))))
         (capitulo (nth 1 split))
         (versiculo (nth 2 split))
         (texto (nth 3 split))
         )
    ;; Forma de retornar la traducción
    (format "%s %s:%s %s" libro capitulo versiculo texto)))

;;; my-bible-ask-text
(defun my-bible-ask-text (&optional version libro capitulo versiculo)
  "Preguntar por un versículo
Test: (my-bible-ask-text 'ntv 'juan 3 16)
"
  (let* (
         (version (or version (my-bible-ask-version)))
         (libro (or libro (my-bible-ask-book)))
         (capitulo (or capitulo (my-bible-ask-chapter "Ingresar número de capítulo: ")))
         (versiculo (or versiculo (my-bible-ask-verse1 "Ingresar número de versículo inicial: ")))
         )
    ;;(format (format "%s %s:%s [%s]" (my-bible-traducir-numero-a-nombre-libro libro) capitulo versiculo version))))
    (format "%s %s %s %s" version libro capitulo versiculo)))

;;;; Funciones interactivas

;;; my-bible-insert-random-verses
(defun my-bible-insert-random-verses (&optional version numero)
  "Inserta N versículos aleatorios del archivo especificado.
Test: (my-bible-insert-random-verses 'ntv' 1)
"
  (interactive)
  (let* (
         (version (or version (my-bible-ask-version)))
         (numero (or numero (read-number "Ingresa números de versículos aleatorios: " 1)))
         (ruta-archivo (format "%s/%s.txt" txt-path version))
         (lineas (my-bible-file-to-list ruta-archivo))
         )
    ;; Inserta n líneas aleatorias seleccionadas en el buffer actual
    (dotimes (_ numero)
      (let ((linea (nth (random (length lineas)) lineas)))
        (insert (my-bible-traducir-linea linea) "\n")))))

;;; my-bible-verse-message
(defun my-bible-verse-message ()
  "Mostrar en minibuffer un versículo.
Test: (my-bible-verse-message)
"
  (interactive)
  (let* (
         ;; Preguntar por la versión y versículo
         (referencia (my-bible-ask-text))
         (referencia-lista (split-string referencia))
         (version (nth 0 referencia-lista))
         (libro (nth 1 referencia-lista))
         (capitulo (nth 2 referencia-lista))
         (versiculo (nth 3 referencia-lista))
         )
    (my-bible-get-verse version libro capitulo versiculo)))


;;;; Funciones que dependen de binarios

;;; 'my-bible-bin-curl-get-up-to-5-verses'
(when (and (executable-find "curl") (executable-find "grep") (executable-find "sed"))
  (defun my-bin-curl-insert-bible-verse (&optional book chapter verses bible-version)
    "Get until 5 verses of Bible."
    (interactive)
    (let* (
           (book (or book (my-bible-ask-book)))
           (chapter (or chapter (read-string "Chapter?: " "3")))
           (verses (or verses (read-string "Verses?: " "16")))
           (bible-version (or bible-version (my-bible-ask-version)))
           (url (format "https://www.biblegateway.com/passage/?search=%s+%s:%s&version=%s" book chapter verses bible-version))
           (command (format "curl -s '%s' | grep 'og:description' | sed 's/.*content=\"//' | sed 's/\".*//'" url))
           )
      (insert (format "%s %s:%s (%s) %s" book chapter verses bible-version (shell-command-to-string command))))))

(provide 'my-bible)
;;; my-bible.el ends here
