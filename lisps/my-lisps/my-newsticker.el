(use-package newsticker
  :config
  ;; 'plainview' allows look similar to Elfeed
  ;; but is slow with a large number of feeds
  (setq newsticker-frontend 'newsticker-plainview)
  ;; Don't update in background
  (setq newsticker-retrieval-interval 0)
  (setq newsticker-ticker-interval 0)
  ;; Feed list
  (setq newsticker-url-list-defaults nil)  ;;remove default list (i.e. emacswiki))
  (setq newsticker-url-list '(
                              ;; Noticias
                              ("Ahí les va"          "https://odysee.com/$/rss/@ahilesva:e"                       nil nil nil)
                              ("La Pulla"            "https://www.youtube.com/feeds/videos.xml?channel_id=UCu2cUfy1hmjlcfZHzvVuEgg" nil nil nil)
                              ("Me dicen Wally"      "https://www.youtube.com/feeds/videos.xml?channel_id=UCW2vo2hnTrdoV0FkjKktgfg" nil nil nil )
                              ;; Cristo
                              ("BITE"                                  "https://biteproject.com/feed/"                                                nil nil nil)
                              ("Coalición"                             "https://www.coalicionporelevangelio.org/feed/"                                nil nil nil)
                              ("BITE"                                  "https://www.youtube.com/feeds/videos.xml?channel_id=UCSkfXhOqioLNNuN6JPY6yXQ" nil nil nil)
                              ("ResMiChu"                              "https://www.youtube.com/feeds/videos.xml?channel_id=UCbCiicK83dcJT6PMGqvqBDw" nil nil nil)
                              ("CFA"                                   "https://www.youtube.com/feeds/videos.xml?channel_id=UCIBHedQqdaF6tvJSuWaRMkA" nil nil nil)
                              ("Iglesia Bautista El Divino Maestro"    "https://www.youtube.com/feeds/videos.xml?channel_id=UCXZAKJKu3ErNJ89Odx7jOdw" nil nil nil)
                              ;; Historia
                              ("Historia NatGeo"           "https://historia.nationalgeographic.com.es/feeds/rss"                         nil nil nil)
                              ("Pero eso es otra Historia" "https://www.youtube.com/feeds/videos.xml?channel_id=UCBIMW0ZhwULY_x7fdaPRPiQ" nil nil nil)
                              ;; Arte
                              ("Historia arte obras" "https://historia-arte.com/obras.rss"   nil nil nil)
                              ("Hislibris"           "https://www.hislibris.com/wp-rss2.php" nil nil nil)
                              ;; Tec / GNU-Linux / Emacs
                              ("Facilicar el software libre"                       "https://facilitarelsoftwarelibre.blogspot.com/feeds/posts/default?alt=rss"    nil nil nil)
                              ("Hipertextual"                                      "https://hipertextual.com/feed"                                                nil nil nil)
                              ("Unidigest"                                         "https://unixdigest.com/feed.rss"                                              nil nil nil)
                              ("Genbeta"                                           "https://www.genbeta.com/feedburner.xml"                                       nil nil nil)
                              ("Muy Linux"                                         "https://www.muylinux.com/feed/"                                               nil nil nil)
                              ("Veronica Explains"                                 "https://www.youtube.com/feeds/videos.xml?channel_id=UCMiyV_Ib77XLpzHPQH_q0qQ" nil nil nil)
                              ("Distrotube"                                        "https://www.youtube.com/feeds/videos.xml?channel_id=UCVls1GmFKf6WlTraIb_IaJg" nil nil nil)
                              ("The Frugal Computer Guy - Michaels Tech Tutorials" "https://www.youtube.com/feeds/videos.xml?channel_id=UCbZ8wD6pmGb9qHqvx9M4YBw" nil nil nil)
                              ("Kris Occhipinti"                                   "https://www.youtube.com/feeds/videos.xml?channel_id=UCf93fPKwotph47H3_KDcRyg" nil nil nil)
                              ("Emacs Rocks"                                       "https://www.youtube.com/feeds/videos.xml?channel_id=UCkRmQ_G_NbdbCQMpALg6UPg" nil nil nil)
                              ("EmacsConf"                                         "https://www.youtube.com/feeds/videos.xml?channel_id=UCwuyodzTl_KdEKNuJmeo99A" nil nil nil)
                              ;; Resumen
                              ("Axl Kss"            "https://www.youtube.com/feeds/videos.xml?channel_id=UC1P7uWBazrqughX3FV-KLbA" nil nil nil)
                              ("Por si no lo viste" "https://www.youtube.com/feeds/videos.xml?channel_id=UC7vwi-QYvVJLT2mMU1DJvOw" nil nil nil)
                              ("Coffe TV"           "https://www.youtube.com/feeds/videos.xml?channel_id=UCNXmVe2tY_qbmv0y6TsAKlw" nil nil nil)
                              ("Te Cuento una"      "https://www.youtube.com/feeds/videos.xml?channel_id=UCYdRUU2Ao66zKAUN115fxTA" nil nil nil)
                              ("Kristoff"           "https://www.youtube.com/feeds/videos.xml?channel_id=UCevIn3faqGtFlMucnrCINHA" nil nil nil)
                              ("Danna Alquati"      "https://www.youtube.com/feeds/videos.xml?channel_id=UChO9L4Y81951vHNg48ijxrw" nil nil nil)
                              ("Cinema Ivis"        "https://www.youtube.com/feeds/videos.xml?channel_id=UCiBgiLAI6dKSxkjlV4atvTA" nil nil nil)
                              ("Woshingo"           "https://www.youtube.com/feeds/videos.xml?channel_id=UCnuQiFVNBo87ahxLAw2aBew" nil nil nil)
                              ("Te lo resumo"       "https://www.youtube.com/feeds/videos.xml?channel_id=UCw7Bz6EHxlnOoBUBlJZCWCw" nil nil nil)
                              ("Yo te Cuento"       "https://www.youtube.com/feeds/videos.xml?channel_id=UC908odK8_F3hEK-GP-t55Kg" nil nil nil)
                              ;; Otrosl
                              ("The Comments Section with Brett Cooper" "https://www.youtube.com/feeds/videos.xml?channel_id=UC7bYyWCCCLHDU0ZuNzGNTtg" nil nil nil)
                              ("The Babylon Bee"                        "https://www.youtube.com/feeds/videos.xml?channel_id=UCyl5V3-J_Bsy3x-EBCJwepg" nil nil nil)
                              ("Mente Humana"                           "https://www.youtube.com/feeds/videos.xml?channel_id=UC5TawXio93r576Rl0UglVDg" nil nil nil)
                              ("Kiun B"                                 "https://www.youtube.com/feeds/videos.xml?channel_id=UCASHFqClADQZTTHhlleOLiA" nil nil nil)
                              ("SizeMatters"                            "https://www.youtube.com/feeds/videos.xml?channel_id=UC6h-HID9dV2BAGSMy4_J84g" nil nil nil)
                              ("La gata de Schrödinger"                 "https://www.youtube.com/feeds/videos.xml?channel_id=UCoXtmmnLCbXDiSo8GxsmOzA" nil nil nil)
                              ("Astro recargado"                        "https://www.youtube.com/feeds/videos.xml?channel_id=UC_unaScAv1u2f2BLmKq7REw" nil nil nil)
                              ("Ximena Vásquez"                         "https://www.youtube.com/feeds/videos.xml?channel_id=UCz0m8CrRgplHkDrOtd2qjvg" nil nil nil)
                              ("El Gran Patriarca"                      "https://www.youtube.com/feeds/videos.xml?channel_id=UCkbPR8sZ97K0TMeY8PQc2BQ" nil nil nil)
                              ("jefillysh"                              "https://www.youtube.com/feeds/videos.xml?channel_id=UCTfQ65AxquXmih3r6rZmK-Q" nil nil nil)
                              ("Ter"                                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCCNgRIfWQKZyPkNvHEzPh7Q" nil nil nil)
                              ))
  ;; Don't mark automatically items as old, also as read
  (setq newsticker-automatically-mark-items-as-old nil)
  ;; How to get the feeds
  ;; (setq newsticker-retrieval-method 'intern)
  ;; (setq newsticker-retrieval-method 'extern)
  ;; (setq newsticker-wget-name "wget")
  ;; (setq newsticker-wget-arguments '("--disable" "--silent" "--location" "--proxy" "socks5://127.0.0.1:7890"))
  ;; (setq newsticker-wget-arguments '("--disable" "--silent" "--location"))
  )
