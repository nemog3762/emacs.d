
;;; my-org-html-export-select-css
;; (defun my-org-html-export-select-css ()
;;   "Export Org using a selected CSS from the internet."
;;   (interactive)
;;   (let* ((css-options '(("LaTeX"           . "https://latex.now.sh/style.css")
;;                         ("Worg"            . "https://orgmode.org/worg/style/worg.css")
;;                         ("Solarized light" . "http://thomasf.github.io/solarized-css/solarized-light.min.css")
;;                         ("Solarized dark"  . "http://thomasf.github.io/solarized-css/solarized-dark.min.css")))
;;          (choice (completing-read "Choose a CSS style: " (mapcar #'car css-options)))
;;          (css-url (cdr (assoc choice css-options))))
;;    (let ((org-html-head (format "<link rel=\"stylesheet\" type=\"text/css\" href=\"%s\"/>" css-url)))
;;      (org-html-export-to-html))))

;;; my-org-create-copy-resources-directory
;; (defun my-org-create-copy-resources-directory ()
;;   "Make dir 'resources' in current path."
;;   (interactive)
;;   (let (
;;         ;; Get path from 'resources' in emacs directory
;;         (source-dir (expand-file-name "resources/" user-emacs-directory))
;;         ;; Create path for the 'assets' directory in the current directory
;;         (target-dir (expand-file-name "src/" default-directory))
;;         )
;;     ;; Create 'target-dir' if it doesn't exist
;;     (unless (file-directory-p target-dir)
;;       (make-directory target-dir))
;;     ;; Copy from 'source-dir' to 'target-dir'
;;     (if (file-directory-p source-dir)
;;         (copy-directory source-dir target-dir t t t)
;;       (message "El directorio fuente no existe."))))

;;; Agenda Custom Command (example from docs)
;; (progn
;;   (setq org-agenda-custom-commands
;;         '(
;;           ("1" "Agenda and all TODOs"
;;            (
;;             (agenda "")
;;             (alltodo "")
;;             )))))

;;; Agenda Custom Command
;; https://github.com/james-stoup/emacs-org-mode-tutorial?tab=readme-ov-file#enhanced-agenda-2
;; (push
;;  ;; James's Super View
;;  '("2" "James's Super View"
;;    (
;;     (agenda ""
;;             (
;;              (org-agenda-remove-tags t)
;;              (org-agenda-span 7)
;;              ))
;;     (alltodo ""
;;              (
;;               ;; Remove tags to make the view cleaner
;;               (org-agenda-remove-tags t)
;;               (org-agenda-prefix-format "  %t  %s")
;;               (org-agenda-overriding-header "CURRENT STATUS")
;;               ;; Define the super agenda groups (sorts by order)
;;               (org-super-agenda-groups
;;                '(
;;                  ;; Filter where tag is CRITICAL
;;                  (:name "Critical Tasks"
;;                         :tag "CRITICAL"
;;                         :order 0
;;                         )
;;                  ;; Filter where TODO state is IN-PROGRESS
;;                  (:name "Currently Working"
;;                         :todo "IN-PROGRESS"
;;                         :order 1
;;                         )
;;                  ;; Filter where TODO state is PLANNING
;;                  (:name "Planning Next Steps"
;;                         :todo "PLANNING"
;;                         :order 2
;;                         )
;;                  ;; Filter where TODO state is BLOCKED or where the tag is obstacle
;;                  (:name "Problems & Blockers"
;;                         :todo "BLOCKED"
;;                         :tag "obstacle"
;;                         :order 3
;;                         )
;;                  ;; Filter where tag is @write_future_ticket
;;                  (:name "Tickets to Create"
;;                         :tag "@write_future_ticket"
;;                         :order 4
;;                         )
;;                  ;; Filter where tag is @research
;;                  (:name "Research Required"
;;                         :tag "@research"
;;                         :order 7
;;                         )
;;                  ;; Filter where tag is meeting and priority is A (only want TODOs from meetings)
;;                  (:name "Meeting Action Items"
;;                         :and (:tag "meeting" :priority "A")
;;                         :order 8
;;                         )
;;                  ;; Filter where state is TODO and the priority is A and the tag is not meeting
;;                  (:name "Other Important Items"
;;                         :and (:todo "TODO" :priority "A" :not (:tag "meeting"))
;;                         :order 9
;;                         )
;;                  ;; Filter where state is TODO and priority is B
;;                  (:name "General Backlog"
;;                         :and (:todo "TODO" :priority "B")
;;                         :order 10
;;                         )
;;                  ;; Filter where the priority is C or less (supports future lower priorities)
;;                  (:name "Non Critical"
;;                         :priority<= "C"
;;                         :order 11
;;                         )
;;                  ;; Filter where TODO state is VERIFYING
;;                  (:name "Currently Being Verified"
;;                         :todo "VERIFYING"
;;                         :order 20
;;                         ))))))) org-agenda-custom-commands)

;;; Agenda Custom Command
;; https://github.com/james-stoup/emacs-org-mode-tutorial?tab=readme-ov-file#agenda-custom-command
;; (progn
;;   (defun air-org-skip-subtree-if-priority (priority)
;;     "Skip an agenda subtree if it has a priority of PRIORITY.

;;   PRIORITY may be one of the characters ?A, ?B, or ?C."
;;     (let ((subtree-end (save-excursion (org-end-of-subtree t)))
;;           (pri-value (* 1000 (- org-lowest-priority priority)))
;;           (pri-current (org-get-priority (thing-at-point 'line t))))
;;       (if (= pri-value pri-current)
;;           subtree-end
;;         nil)))
;;   (setq org-agenda-skip-deadline-if-done t)
;;   (push
;;    '("3" "Daily agenda and all TODOs"
;;      ((tags "PRIORITY=\"A\""
;;             ((org-agenda-skip-function
;;               '(org-agenda-skip-entry-if 'todo 'done))
;;              (org-agenda-overriding-header "High-priority unfinished tasks:")))
;;       (agenda #1=""
;;               ((org-agenda-span 7)))
;;       (alltodo #1#
;;                ((org-agenda-skip-function
;;                  '(or
;;                    (air-org-skip-subtree-if-priority 65)
;;                    (air-org-skip-subtree-if-priority 67)
;;                    (org-agenda-skip-if nil
;;                                        '(scheduled deadline))))
;;                 (org-agenda-overriding-header "ALL normal priority tasks:")))
;;       (tags "PRIORITY=\"C\""
;;             ((org-agenda-skip-function
;;               '(org-agenda-skip-entry-if 'todo 'done))
;;              (org-agenda-overriding-header "Low-priority Unfinished tasks:"))))
;;      ((org-agenda-compact-blocks nil))) org-agenda-custom-commands ))

;;;; my-presentation
;; Based on 'org-present' (https://github.com/rlister/org-present)
;; (progn
;;   (defun my-org-presentation-narrow-previous ()
;;     "Move to the next subtree."
;;     (interactive)
;;     (widen)
;;     (outline-previous-heading)
;;     (my-org-where-am-i)
;;     (org-narrow-to-subtree))
;;   (defun my-org-presentation-narrow-next ()
;;     "Move to the previous subtree"
;;     (interactive)
;;     (widen)
;;     (outline-next-heading)
;;     (my-org-where-am-i)
;;     (org-narrow-to-subtree))
;;   (defun my-org-presentation-toggle ()
;;     "Toggle org narrow subtreee / show everything"
;;     (interactive)
;;     (if (buffer-narrowed-p)
;;         (widen)
;;       (org-narrow-to-subtree)))
;;   (add-hook 'org-mode-hook
;;             (lambda ()
;;               ;; Keys
;;               (local-set-key (kbd "<f7>") #'my-org-presentation-toggle)
;;               (local-set-key (kbd "<f8>") #'my-org-presentation-narrow-previous)
;;               (local-set-key (kbd "<f9>") #'my-org-presentation-narrow-next)
;;               ;; (local-set-key (kbd "<f10>") #'org-side-tree)
;;               )))
