;; Useful resource:
;; https://caiorss.github.io/Emacs-Elisp-Programming/Packages.html

;;;; Faces

(defface my-face-bg-yellow
  '((t (:foreground "black" :background "yellow" :box t))) "Background yellow")

(defface my-face-bg-blue
  '((t (:foreground "black" :background "deep sky blue" :box t))) "Background blue")

(defface my-face-bg-red
  '((t (:foreground "black" :background "red" :box t))) "Background red")

(defface my-face-bg-orange
  '((t (:foreground "black" :background "orange" :box t))) "Background orange")

;;;; General

(add-hook 'prog-mode-hook
          (lambda ()
            ;; Tamaño de fuente
            (buffer-face-set '(:height 100))
            ;; Espaciado de líneas (ninguno)
            (setq-local line-spacing nil)
            ;; Color del indicador de columna
            (add-to-list 'face-remapping-alist
                         '(fill-column-indicator :foreground "red"))
            (setq-default fill-column 80)
            (display-fill-column-indicator-mode t)
            ;; Documentación
            (global-eldoc-mode)
            (setq eldoc-echo-area-use-multiline-p 5)
            ;; Atajo específico para prog-mode
            (define-key prog-mode-map (kbd "C-1") 'my-comment-or-uncomment)
            ;; Highlight line
            (hl-line-mode)))

;;;; lisp-mode

(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (add-hook 'after-save-hook #'check-parens nil t)
            (highlight-phrase "^;;; .*$"  'my-face-bg-blue)
            (highlight-phrase "^;;;; .*$" 'my-face-bg-yellow)
            (highlight-phrase "disabled"  'my-face-bg-red)
            (local-set-key (kbd "M-p")  'beginning-of-defun)
            (local-set-key (kbd "M-n")  'end-of-defun)
            ))

;;;; sh-mode

(add-hook 'sh-mode-hook
          (lambda ()
            (add-hook 'after-save-hook #'check-parens nil t)
            (highlight-phrase "^### .*$"   'my-face-bg-blue)
            (highlight-phrase "^#### .*$"  'my-face-bg-yellow)
            ))

;;;; python-mode

;; Set python interpreter
(setq my-python-path "~/miniconda3/envs/njat/bin/ipython")
(when (file-executable-p my-python-path)
  (defun my-python-run-env-njat ()
    "Run python from miniconda env (njat)"
    (interactive)
    (run-python my-python-path nil t)))

;; Activate some things
(add-hook 'python-mode
          (lambda ()
            ;; Disable tabs, use spaces
            (setq indent-tabs-mode nil)
            ;; Distance between tab stops
            (setq tab-width 4)
            ;; indentation offset for Python.
            (setq python-indent-offset 4)
            (hs-minor-mode)
            ))

(add-hook 'inferior-python-mode
          (lambda ()
            ;; Ajustar altura al 80%
            (setq-local buffer-face-mode-face '(:height 80))
            (buffer-face-mode t)
            ;; Keys
            (local-set-key (kbd "<up>")    'comint-previous-input)
            (local-set-key (kbd "<down>")  'comint-next-input)
            ))

(defun my-python-shell-auto-scroll ()
  "Scroll to the last line in the Python shell buffer after sending input."
  (when (eq major-mode 'inferior-python-mode)
    (goto-char (point-max))
    (recenter -1)))
(add-hook 'comint-output-filter-functions #'my-python-shell-auto-scroll)

;;; 'html-mode'

(with-eval-after-load 'mhtml-mode
  (define-key mhtml-mode-map (kbd "C-1") 'my-comment-or-uncomment))
