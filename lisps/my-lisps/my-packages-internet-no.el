;;; my-packages-internet-no.el --- my functions for packages -*- lexical-binding: t; -*-

;;;; Basic

;;; 'which-key' (built-in in Emacs 30)
(use-package which-key
  :custom
  (which-key-idle-delay 0.2)
  :config
  (which-key-mode 1))

;;; amx (require s)
;; Show the shortcuts of functions with 'M-x'
(use-package amx
  :disabled t
  :init
  (require 's)
  :config
  (define-key icomplete-fido-mode-map (kbd "<SPC>") 'minibuffer-complete-word)
  (define-key icomplete-fido-mode-map (kbd "<left>")  'backward-char)
  (define-key icomplete-fido-mode-map (kbd "<right>") 'forward-char)
  (amx-mode))

;;;; Org

;;; 'org-autolist' auto insert number o bullet with RET in list
(use-package org-autolist
  :defer t
  :hook
  (org-mode . org-autolist-mode))

;;; 'toc-org' for TOC in org-mode
(use-package toc-org
  :defer t
  :hook
  (org-mode . toc-org-mode))

;;; 'org-appear' make invisible parts of Org elements appear visible
(use-package org-appear
  :defer t
  :hook
  (org-mode . org-appear-mode))

;;; org-tree-slide
(use-package org-tree-slide)

;;; 'ox-epub' export to .epub from org-mode
;; (when (require 'ox-epub nil t))
;; ;; 'org-mind-map'
;; ;; https://github.com/the-ted/org-mind-map
;; (defun org-mind-map-init ()
;;   "Init function to load necessary things for org-mind-map package"
;;   (interactive)
;;   (require 'ox-org)
;;   (require 'org-mind-map)
;;   ;; Options
;;   (setq org-mind-map-include-text nil) ;; no include text
;;   ;; Layout type
;;   ;; 'dot'   Default; Directed Graph
;;   ;; 'neatzo' Undirected Spring Graph
;;   ;; 'twopi' Radial Layout
;;   ;; 'circo' Circular Layout
;;   ;; 'fdp'   Undirected Spring Force-Directed
;;   (setq org-mind-map-engine "dot"))

;;;; dired

;;; 'dired-preview'
(use-package dired-preview
  :init
  (defun my-dired-preview-to-the-right ()
    "My preferred `dired-preview-display-action-alist-function'."
    '((display-buffer-in-side-window)
      (side . right)
      (width . 0.4)))
  :config
  ;; Put on right the preview
  (setq dired-preview-display-action-alist #'my-dired-preview-to-the-right)
  ;; Other configs
  (setq dired-preview-delay 0.2)
  (setq dired-preview-max-size (expt 2 40))
  (setq dired-preview-ignored-extensions-regexp
        (concat "\\."
                "\\("
                "gz\\|"
                "zst\\|"
                "tar\\|"
                "xz\\|"
                "rar\\|"
                "zip\\|"
                "iso\\|"
                "mp4\\|"
                "epub"
                "\\)")))

;;; 'disk-usage'
;; https://gitlab.com/ambrevar/emacs-disk-usage
(use-package disk-usage)

;;;; Git

;;; 'dired-git-info'
(use-package dired-git-info
  :commands (dired-git-info-mode))

;;;; Games / fun

;;; 'selectric-mode'
(use-package selectric-mode
  :commands selectric-mode)

;;; 'xkcd'
(use-package xkcd
  :defer t
  :commands (xkcd))

;;; 'emvaders' or space-invaders
(use-package emvaders
  :defer t
  :commands (space-invaders)
  :config
  (defalias 'space-invaders 'emvaders))

;;; 'sudoku'
(use-package sudoku
  :defer t
  :commands (sudoku))

;;; '2048-game'
(use-package 2048-game
  :commands (2048-game))

;;; 'minesweeper' (buscaminas)
(use-package minesweeper)

;;;; Buffers

(use-package pc-bufsw
  ;; https://www.emacswiki.org/emacs/ControlTABbufferCycling
  ;; https://github.com/ibukanov/pc-bufsw
  :config
  (require 'pc-bufsw)
  ;; By defaults use 'C-<tab>' and 'C-S-<tab>'
  ;; (global-set-key (kbd "<f6>") 'pc-bufsw-lru) ;; Prev
  ;; (global-set-key (kbd "<f7>") 'pc-bufsw-mru) ;; Next
  (setq pc-bufsw-quit-time 3)
  (setq pc-bufsw-selected-buffer-face '((t :background "black" :foreground "white")))
  (setq pc-bufsw-decorator-left "[")
  (setq pc-bufsw-decorator-right "]")
  (pc-bufsw 't))

;;;; Programming

;;; 'rainbow-mode'
(use-package rainbow-mode
  :hook
  (prog-mode . rainbow-mode)
  (org-mode . rainbow-mode))

;;; 'rainbow-delimiters'
(use-package rainbow-delimiters
  :custom-face
  (rainbow-delimiters-depth-1-face   ((t (:foreground "green" ))))
  (rainbow-delimiters-depth-2-face   ((t (:foreground "blue"  ))))
  (rainbow-delimiters-depth-3-face   ((t (:foreground "red"   ))))
  (rainbow-delimiters-depth-4-face   ((t (:foreground "dark green" ))))
  (rainbow-delimiters-depth-5-face   ((t (:foreground "orange"))))
  (rainbow-delimiters-depth-6-face   ((t (:foreground "violet"))))
  (rainbow-delimiters-depth-7-face   ((t (:foreground "purple"))))
  (rainbow-delimiters-depth-8-face   ((t (:foreground "yellow"))))
  (rainbow-delimiters-unmatched-face ((t (:background "red"   ))))
  :hook
  (prog-mode . rainbow-delimiters-mode))

;;; 'yasnippet'
;; https://github.com/joaotavora/yasnippet
;; Can use others snippets with https://elpa.gnu.org/packages/yasnippet-classic-snippets.html
(use-package yasnippet
  :init
  (setq yas-snippet-dirs `(,(concat user-emacs-directory "snippets")))
  ;; :config
  ;; (yas-global-mode)
  :hook
  (prog-mode . yas-minor-mode)
;; (org-mode  . yas-minor-mode))
  )

;;; python 'python-cell'
;; MATLABf-like cells in python.
;; https://github.com/twmr/python-cell.el/
;; 'Ctrl-Return' send contents of the current cell to an inferior python process
;; 'Ctrl-Down' move to the beginning of the next cell
;; 'Ctrl-Up' move to the beginning of the previous cell
(use-package python-cell
  :hook (python-mode . python-cell-mode)
  :init
  ;; Configurar el patrón de separación de celdas
  (setq python-cell-cellbreak-regexp
        "^[[:space:]]*\\(#\\(?:#[[:space:]].*\\| <\\(?:\\(?:code\\|markdown\\)cell\\)>\\|[[:space:]]*%%.*\\)$\\)")
  :custom-face
  (python-cell-highlight-face ((t (:background "cornsilk" :extend t :inherit nil))))
  (python-cell-cellbreak-face ((t (:weight bold :extend t :foreground "red" :box t))))
  :bind
  (:map python-mode-map
        ;; ("<f6>" . python-cell-shell-send-cell)
        ("M-n"  . python-cell-forward-cell)
        ("M-p"  . python-cell-backward-cell)
        ("<f7>" .  (lambda ()
                     (interactive)
                     (python-cell-shell-send-cell)
                     (python-cell-forward-cell)
                     ))
        ("<f8>" .  python-cell-shell-send-cell)
        ("<f9>" . 'python-shell-send-region)
        ))

;;; python 'highlight-indentation'
;; https://github.com/antonj/Highlight-Indentation-for-Emacs
(use-package highlight-indentation
  :config
  (set-face-background 'highlight-indentation-face "#e3e3d3")
  (set-face-background 'highlight-indentation-current-column-face "#c3b3b3")
  :hook
  (python-mode . highlight-indentation-mode)
  (python-mode . highlight-indentation-current-column-mode))


;;; 'indent-guide.el' show vertical lines to guide indentation
(use-package indent-guide
  :hook
  ;; (indent-guide-global-mode)
  (prog-mode  . indent-guide-mode))

;;; 'quickrun' run the curren file
(use-package quickrun
  :defer t
  :commands (quickrun))

;;; 'minimap'
(use-package minimap
  :commands (minimap-mode)
  :init
  ;; Configuración de minimap
  (setq minimap-window-location 'right)
  (setq minimap-minimum-width 0)
  (setq minimap-width-fraction 0.15)
  (setq minimap-always-recenter -1)
  (setq minimap-update-delay 0.1))

;;; 'highlight-define' defined Emacs Lisp symbols in source code.
;; Recognizes Lisp function, built-in function, macro, face and variable names
(use-package highlight-defined
  :custom
  (setq highlight-defined-face-use-itself t)
  :hook
  (help-mode       . highlight-defined-mode)
  (emacs-lisp-mode . highlight-defined-mode))

;;; scilab
;; http://ftp.sun.ac.za/ftp/pub/mirrors/scilab/www.scilab.org/contrib/displayContribution.php%3FfileID=203.html
(use-package scilab-mode
  :config
  (global-font-lock-mode t)
  (setq auto-mode-alist (cons '("\\(\\.sci$\\|\\.sce$\\)" . scilab-mode)
                              auto-mode-alist))
  (setq scilab-mode-hook '(lambda () (setq fill-column 74))))

;;;; Others

;;; emacs-everywhere
;; emacsclient --eval "(emacs-everywhere)"
(use-package emacs-everywhere)

;;; flyspell + completing read
;; https://github.com/d12frosted/flyspell-correct/tree/master?tab=readme-ov-file#flyspell-correct-completing-read-interface
(use-package flyspell-correct
  :after flyspell
  :bind (:map flyspell-mode-map ("C-," . flyspell-correct-wrapper)))

;;; lorem-ipsum
(use-package lorem-ipsum)

;;; 'highlight-thing' señalar o resaltar la palabra bajo el cursor
(use-package highlight-thing
  :commands (highlight-thing-mode)
  :init
  (defalias 'toggle-highlight-word-under-cursor 'highlight-thing-mode)
  :custom
  (highlight-thing-delay-seconds 0.1)
  (highlight-thing-case-sensitive-p nil)
  :hook
  (eww-mode . highlight-thing-mode))

;;; 'hippie-completing-read' for get 'hippie-expand' with 'completing-read'
;; Not built-in
;; https://github.com/duckwork/hippie-completing-read
(use-package hippie-completing-read
  :defer t
  :commands (hippie-expand)
  ;; 'C-t' runs 'hippie-completing-read'.
  ;; By defaults 'C-t' runs 'transpose-chars'. I don't use that function.
  :bind ("C-t" . hippie-completing-read))

;;; 'olivetti'
(use-package olivetti
  :commands (olivetti-mode)
  :config
  (set-face-attribute 'olivetti-fringe nil :background "white"))

;;; 'focus'
(use-package focus
  :commands (focus-mode))

;;; 'key-chord'
;; Config 'jk', 'kj', 'JK' and 'KJ' as '<ESC>' in 'evil'
(use-package key-chord
  :after evil
  :init
  ;; Configuración de delay para detectar combinaciones de teclas
  (setq key-chord-two-keys-delay 0.5)
  :config
  ;; Definición de combinaciones para salir al estado normal en Evil desde el estado de inserción
  (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "kj" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "JK" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "KJ" 'evil-normal-state)
  ;; Activar el modo de `key-chord`
  (key-chord-mode 1))

;;; 'celestial-mode-line'
(use-package celestial-mode-line
  :config
  (unless calendar-latitude  (setq calendar-latitude   10.95))
  (unless calendar-longitude (setq calendar-longitude -74.78))
  (setq global-mode-string '("" celestial-mode-line-string display-time-string))
  (celestial-mode-line-start-timer))

;;; 'iscroll' for smoot scrolling
(use-package iscroll
  :defer t
  :hook
  (eww-mode . iscroll-mode)
  (org-mode . iscroll-mode))

;;; 'sr-speedbar'
(use-package sr-speedbar
  :defer t
  :commands (sr-speedbar-toggle)
  :init
  ;; No agrupar variables y tags por prefijo.
  (require 'speedbar)
  (setq speedbar-tag-hierarchy-method '(speedbar-simple-group-tag-hierarchy))
  (setq speedbar-use-images nil)       ;; Icons
  (setq sr-speedbar-width 30)          ;; Width of speedbar-window
  (setq sr-speedbar-right-side nil)    ;; Show tree on the left side
  (setq speedbar-show-unknown-files t) ;; Show all files
  (setq pop-up-windows nil)            ;; nil (if 't', always split the buffer vertically)
  :config
  ;; Config of 'sr-speedbar'
  (set-face-attribute 'speedbar-file-face nil      :height 100)
  (set-face-attribute 'speedbar-directory-face nil :height 100)
  (set-face-attribute 'speedbar-button-face nil    :height 100)
  (set-face-attribute 'speedbar-highlight-face nil :height 100)
  (set-face-attribute 'speedbar-selected-face nil  :height 100)
  (set-face-attribute 'speedbar-separator-face nil :height 100)
  (set-face-attribute 'speedbar-tag-face nil       :height 100))

;;; 'darkroom'
(use-package darkroom
  :defer t
  :commands (darkroom-mode))

;;; 'move-text'
(use-package move-text
  :bind
  (("M-<up>"   . move-text-up)
   ("M-<down>" . move-text-down))
  :config
  (defun indent-region-advice (&rest ignored)
    "Indent after moving"
    (let ((deactivate deactivate-mark))
      (if (region-active-p)
          (indent-region (region-beginning) (region-end))
        (indent-region (line-beginning-position) (line-end-position)))
      (setq deactivate-mark deactivate)))
  (advice-add 'move-text-up   :after 'indent-region-advice)
  (advice-add 'move-text-down :after 'indent-region-advice))

;;; 'markdown-mode'
(use-package markdown-mode
  :defer t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . gfm-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "markdown2")
  :custom
  (visual-line-column 80)
  (markdown-fontify-code-blocks-natively t)
  (markdown-enable-math t))

;;; erc
(use-package erc
  :config
  (setq erc-server "irc.libera.chat"
        erc-nick "nemo-asimov"             ; Change this!
        erc-user-full-name "Isaac Asimov"  ; And this!
        erc-track-shorten-start 8
        erc-autojoin-channels-alist '(("irc.libera.chat" "#systemcrafters" "#emacs"))
        erc-kill-buffer-on-part t
        erc-auto-query 'bury))

;;; 'keyfreq'
;; Idea from: http://xahlee.info/emacs/emacs/command-frequency.html
(use-package keyfreq
  :config
  (require 'keyfreq)
  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1)
  ;; exclude commands
  (setq keyfreq-excluded-commands
        '(
          backward-char
          dired-next-line
          dired-previous-line
          forward-char
          left-char
          left-word
          next-line
          org-self-insert-command
          previous-line
          right-char
          right-word
          self-insert-command
        )))

;;; anotate
(use-package annotate
  :defer t
  :commands (annotate-file anotar-archivo))

(provide 'my-packages-internet-no)
;;; my-packages-internet-no.el ends here
