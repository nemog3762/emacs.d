;;; my-dired.el --- my dired configs -*- lexical-binding: t; -*-

(require 'dired)

;;;; TODO

;; https://emacs.dyerdwelling.family/emacs/20240120084016-emacs--dired-async-mode/

;;;; faces

;;; directory afce
(set-face-attribute 'dired-directory nil :weight 'bold)

;;;; Functions for movement

;;; change directory quickly
(defun dired-ge () (interactive) (dired (expand-file-name user-emacs-directory)))
(defun dired-open (linux-path windows-path darwin-path)
  (let* ((path (cond
               ((eq system-type 'gnu/linux) linux-path)
               ((eq system-type 'windows-nt) windows-path)
               ((eq system-type 'darwin) darwin-path)
               (t (error "Sistema operativo no soportado")))))
    (dired path)))
(defun dired-g. () (interactive) (dired-open "~/gits" "D:\\gits" "~/Desktop/NJAT/gits"))
(defun dired-g1 () (interactive) (dired-open "~/Google Drive" "D:\\Google Drive" "~/Desktop/NJAT/Google Drive"))
(defun dired-g2 () (interactive) (dired-open "~/Google Drive/svm_ezkernel" "D:\\Google Drive\\svm_ezkernel" "~/Desktop/NJAT/Google Drive/svm_ezkernel"))
(defun dired-g3 () (interactive) (dired-open "~/OneDrive" "D:\\OneDrive" "~/OneDrive"))
(defun dired-gc () (interactive) (dired-open "~/.config" "D:\\gits" "~/.config"))
(defun dired-gd () (interactive) (dired-open "~/Documentos" "D:\\" "~/Documents"))
(defun dired-gh () (interactive) (dired-open "~/" "D:\\" "~/Desktop/NJAT/"))
(defun dired-gi () (interactive) (dired-open "~/Imágenes" "D:\\Imágenes" "~/Pictures"))
(defun dired-gm () (interactive) (dired-open "~/Música" "D:\\Música" "~/Music"))
(defun dired-go () (interactive) (dired-open "~/Descargas" "D:\\Descargas" "~/Downloads"))
(defun dired-gv () (interactive) (dired-open "~/Vídeos/" "D:\\Vídeos" "~/Movies"))
(defun dired-g7 () (interactive) (dired-open "/" "C:\\" "/"))

(when (string-equal system-type "windows-nt")
  (defun my-dired-windows ()
    "Change to other disks (depending on available drives)"
    (interactive)
    (let* ((letters (seq-filter #'file-directory-p '("B:\\" "C:\\" "D:\\" "E:\\" "F:\\" "G:\\" "H:\\")))
           (drive-path (completing-read "Choose a letter: " letters)))
      (if (file-directory-p drive-path)
          (dired drive-path)
        (message "Error: %s doesn't exist" drive-path)))))

;;;; Other functions

;;; my-dired-open-file-manager
(defun my-dired-open-file-manager ()
  "Show current file in the desktop file manager."
  (interactive)
  (pcase system-type
    ('windows-nt (w32-shell-execute "explore" (convert-standard-filename default-directory)))
    ('darwin     (shell-command "open ."))
    ('gnu/linux  (call-process-shell-command "xdg-open ."))))

;;; dired-remove-subtree-view
(defun dired-remove-subtree-view ()
  "Remove tree view dired"
  (interactive )
  (let ((i (point))
        (cur-dir (dired-current-directory)))
    (beginning-of-buffer)
    (while (search-forward cur-dir nil t)
      (dired-kill-subdir))
    (goto-char i)))

;;; my-dired-fuzzy-open
(defun my-dired-fuzzy-open ()
  "Open the file under cursor with a binary in PATH."
  (interactive)
  (unless (eq major-mode 'dired-mode)
    (error "This function only works in dired"))
  (let* ((file (dired-get-file-for-visit))
         (dirs (pcase system-type
                ('windows-nt '("C:/Program Files/" "C:/Program Files (x86)/" "C:/Windows/System32/"))
                ('gnu/linux  '("/usr/bin/" "/usr/local/bin/"))
                ('darwin     '("/usr/bin/" "/Applications/"))))
         (programs (apply #'append
                          (mapcar (lambda (dir)
                                    (when (file-directory-p dir)
                                      (directory-files dir t "^[^.].*")))
                                  dirs)))
         (program (completing-read "Select program: " programs)))
    (pcase system-type
     ('windows-nt (start-process "dired-run" nil "cmd" "/c" "start" program file))
     ('gnu/linux  (call-process shell-file-name nil 0 nil shell-command-switch (format "%s %s" program (shell-quote-argument file))))
     ('darwin     (start-process "dired-run" nil "open" "-a" program file)))))

;;; my-dired-switch-buffer
(defun my-dired-switch-buffer ()
  "Select a Dired buffer to switch to, using 'completing-read'."
  (interactive)
  (let* ((dired-buffers
          (mapcar (lambda (buffer)
                    (with-current-buffer buffer
                      (when (eq major-mode 'dired-mode)
                        (buffer-name buffer))))
                  (buffer-list)))
         (dired-buffers (delq nil dired-buffers)) ; remove nil values
         (selected-buffer (completing-read "Select Dired buffer: " dired-buffers nil t)))
    (if selected-buffer
        (switch-to-buffer selected-buffer)
      (message "No Dired buffers available"))))

;;; my-dired-do-rename
(defun my-dired-do-rename ()
  "Rename the current file in Dired."
  (interactive)
  (let* ((filename (file-name-nondirectory (dired-get-file-for-visit)))
         (new-name (read-string "New filename: " filename)))
    ;; Rename
    (if (not (string= filename new-name))
        (progn
          (rename-file filename new-name t)
          (revert-buffer)
          (message "%s" new-name)))))

;;; my-dired-create-directory
(defun my-dired-create-directory ()
  "Ask name of directory to create in 'dired'."
  (interactive)
  (dired-create-directory (read-string "Name of new DIR: "))
  (revert-buffer))

;;; my-dired-create-empty-file
(defun my-dired-create-empty-file ()
  "Ask name of directory to create in 'dired'."
  (interactive)
  (dired-create-empty-file (read-string "Name of new FILE: "))
  (revert-buffer))

;;; my-dired-sort
(defun my-dired-sort ()
  "Prompt for a sort option and apply it in Dired."
  (interactive)
  (let* (
         (prompt "Choose sort option:
  s: Sort by Size
  x: Sort by eXtension
  t: Sort by Time
  n: Sort by Name
  d: Sort by Name (grouping Dirs first)")
         (choice (read-char prompt))
         (switches (assoc-default choice
                                  '((?s . "S")
                                    (?x . "X")
                                    (?t . "t")
                                    (?n . "")
                                    (?d . " --group-directories-first")))))
    (if switches
        (dired-sort-other (concat dired-listing-switches switches))
      (message "Invalid choice"))))

;;; my-dired-delete-latex-tex-aux-files
(defun my-dired-delete-latex-tex-aux-files ()
  "In Dired, delete auxiliary files associated with a selected .tex file.
If no .tex file is selected, do nothing."
  (interactive)
  (let ((selected-file (dired-get-file-for-visit))
        ;; Lista de extensiones de archivos auxiliares a borrar
        (aux-extensions '(".log" ".out" ".synctex.gz" ".aux" ".toc" ".fls" ".fdb_latexmk")))
    (if (and selected-file (string-match-p "\\.tex\\'" selected-file))
        (let ((base-name (file-name-sans-extension selected-file)))
          (dolist (ext aux-extensions)
            (let ((file-to-delete (concat base-name ext)))
              (when (file-exists-p file-to-delete)
                (delete-file file-to-delete)
                ;; (message "Deleted: %s" file-to-delete)
                (revert-buffer)))))
      (message "No .tex file selected or not in Dired mode."))))

;;;; Functions for file size without 'du'

;;; my-format-file-size
(defun my-format-file-size (size)
  "Format SIZE (in bytes) as a human-readable string in KB, MB, or GB."
  (cond
   ((>= size (* 1024 1024 1024)) (format "%.2f GB" (/ (float size) (* 1024 1024 1024))))
   ((>= size (* 1024 1024)) (format "%.2f MB" (/ (float size) (* 1024 1024))))
   ((>= size 1024) (format "%.2f KB" (/ (float size) 1024)))
   (t (format "%d B" size))))

;;; my-dired-show-file-attributes
(defun my-dired-show-file-attributes ()
  "Display detailed attributes of the file at point in dired."
  (interactive)
  (let* ((file (dired-get-file-for-visit)) ; Obtener el archivo seleccionado
         (attributes (file-attributes file))) ; Obtener atributos del archivo
    (if attributes
        (let ((details
               (format "
%s: %s
Size:               %s
Permissions:        %s
Last accessed:      %s
Last modified:      %s
Last status change: %s
"
                       (if (eq t (nth 0 attributes)) "Directory" "File") ; Type
                       file
                       (my-format-file-size (nth 7 attributes)) ; Tamaño
                       (nth 8 attributes) ; Permisos
                       (format-time-string "%Y-%m-%d %H:%M:%S" (nth 4 attributes)) ; Último acceso
                       (format-time-string "%Y-%m-%d %H:%M:%S" (nth 5 attributes)) ; Última modificación
                       (format-time-string "%Y-%m-%d %H:%M:%S" (nth 6 attributes)) ; Cambio de estado
                       ;; (nth 1 attributes) ; Number of links
                       ;; (nth 2 attributes) ; Owner UID
                       ;; (nth 3 attributes) ; Group GID
                       ;; (nth 10 attributes) ; Inode
                       ;; (nth 11 attributes) ; Device
                       )))
          (message details)))))

;;;; Functions with binary

;;; my-dired-get-size
(when (executable-find "du")
  (defun my-dired-get-size ()
    "Show the total size of marked files in Dired. Requires 'du' command."
    (interactive)
    (let* ((files (dired-get-marked-files)) ;; Obtiene archivos seleccionados en Dired
           ;; Convierte la lista de archivos en una cadena para pasar a `du`
           (files-string (mapconcat #'shell-quote-argument files " "))
           (output (shell-command-to-string (format "du -sch %s" files-string))))
      (message "%s" output))))

;;; my-dired-bin-quick-preview
(defun my-dired-bin-quick-preview ()
  "Show a quick look of the selected file."
  (interactive)
  (let ((file (dired-get-file-for-visit)))
    (pcase (or (executable-find "qlmanage")
               (executable-find "sushi"))
      ("qlmanage" (start-process "emacs-preview" nil "qlmanage" "-p" file))
      ("sushi"    (start-process "emacs-preview" nil "sushi" file))
      (_          (message "No preview tool found for your operating system.")))))

;;;; Configs

;; Limitar a dired a un solo buffer cuando se ingresa a un directorio
(setq dired-kill-when-opening-new-dired-buffer t)

;; Actualizar dired cuando se modifica un directorio
(setq global-auto-revert-non-file-buffers t)

;; Delete to trash
(setq delete-by-moving-to-trash t)

;; Always Recursion (no preguntar al ejecutar acciones a directorios)
(setq dired-recursive-deletes 'always)

;; Always Recursion (no preguntar al ejecutar acciones a directorios)
(setq dired-recursive-copies  'always)

;; Con 2 paneles, se sugiere el otro panel al copiar y mover
(setq dired-dwim-target t)

;; No mostrar archivos ocultos al iniciar
(require 'dired-x)
(setq dired-omit-files "^\\...+$")
(add-hook 'dired-mode-hook 'dired-omit-mode)

;;; Put directories first in dired without external packages
(require 'ls-lisp)
(setq ls-lisp-use-insert-directory-program nil)
(setq ls-lisp-dirs-first t)                              ;; Show Dir First
(setq ls-lisp-ignore-case 't)                            ;; Ignore Case Order
(setq dired-listing-switches "-l --size --human-readable --no-group --all -1 -v -N")

;;; Hide absolute path on dired
;; https://xenodium.com/hide-another-detail/
(progn
  (defun hide-dired-details-include-all-subdir-paths ()
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward dired-subdir-regexp nil t)
        (let* ((match-bounds (cons (match-beginning 1) (match-end 1)))
               (path
                (file-name-directory (buffer-substring (car match-bounds)
                                                       (cdr match-bounds))))
               (path-start (car match-bounds))
               (path-end (+ (car match-bounds) (length path)))
               (inhibit-read-only t))
          (put-text-property path-start path-end
                             'invisible 'dired-hide-details-information)))))
  (use-package dired
    :hook ((dired-mode . dired-hide-details-mode)
           (dired-after-readin
            . hide-dired-details-include-all-subdir-paths))))

;;; Compress / descompress

(setq
 ;; Controls the compression shell command for ‘dired-do-compress-to’.
 dired-compress-file-alist
 '(
   ("\\.7z\\'"  . "7z a -r %o %i")
   ("\\.zip\\'" . "zip %o -r --filesync %i")
   ("\\.tar\\.gz\\'" . "tar -c %i | gzip -c9 > %o")
   ("\\.tar\\.xz\\'" . "tar -c %i | xz -c9 > %o")
   )
 ;; Controls the compression shell command for ‘dired-do-compress-to’.
 dired-compress-files-alist
 '(
   ("\\.7z\\'"  . "7z a -r %o %i")
   ("\\.zip\\'" . "zip %o -r --filesync %i")
   ("\\.tar\\.gz\\'" . "tar -c %i | gzip -c9 > %o")
   ("\\.tar\\.xz\\'" . "tar -c %i | xz -c9 > %o")
   )
 ;; Default compress to ...
 dired-compress-directory-default-suffix ".zip"
 dired-compress-file-default-suffix      ".zip"
 ;; Auto-revert buffer in Dired after compressing
 dired-do-revert-buffer t
 )

(defun my-dired-compress ()
  "Compress in '.zip', '.7z' or 'tar.xz'"
  (interactive)
  (let ((key (read-char "Compress to (z)ip, (7)z, tar.(x)z, tar.(g)z: ")))
    (cond
     ;; Compress to 'zip'
     ((char-equal key ?z)
      (progn
        (setq dired-compress-directory-default-suffix ".zip")
        (setq dired-compress-file-default-suffix ".zip")
        (dired-do-compress)))
     ;; Compress to '7z'
     ((char-equal key ?7)
      (if (executable-find "7z")  ;; Check if 7z is installer
          (progn
            (setq dired-compress-directory-default-suffix ".7z")
            (setq dired-compress-file-default-suffix ".7z")
            (dired-do-compress))
        (message "7z is not installed!")))
     ;; Compress to 'tar.xz'
     ((char-equal key ?x)
      (if (executable-find "tar")  ;; Check if tar is installed
          (progn
            (setq dired-compress-directory-default-suffix ".tar.xz")
            (setq dired-compress-file-default-suffix ".tar.xz")
            (dired-do-compress))
        (message "tar is not installed!")))
     ;; Compress to 'tar.gz'
     ((char-equal key ?g)
      (if (executable-find "tar")  ;; Check if tar is installed
          (progn
            (setq dired-compress-directory-default-suffix ".tar.gz")
            (setq dired-compress-file-default-suffix ".tar.gz")
            (dired-do-compress))
        (message "tar is not installed!")))
     ;; On error...
     (t (message "Option no valid!.")))))

;;;; Hooks

(add-hook 'dired-mode-hook (lambda ()
                             ;; No divide long lines
                             (visual-line-mode -1)
                             ;; Hide details by default
                             (dired-hide-details-mode 1)
                             ;; Líneas largas no se dividen en 2
                             (setq truncate-lines t)
                             ;; Size
                             (buffer-face-set '(:height 120))
                             ))

;; Enable hl-line-mode for dired
;; hl-line-mode is a local hook.
;; global-hl-line-mode is a global hook.
;; Turn on:
(add-hook 'dired-after-readin-hook 'hl-line-mode)

;;;; Teclado y mouse
(add-hook 'dired-mode-hook
          (lambda ()
            ;; Mouse
            (local-set-key [mouse-2] 'dired-mouse-find-file)
            (local-set-key [mouse-3] 'my-dired-fuzzy-open)
            ;; Fila FN
            (local-set-key (kbd "<deletechar>") 'dired-do-delete)
            ;; (local-set-key (kbd "C-<tab>")      'my-dired-switch-buffer)
            (local-set-key (kbd "<return>")     #'dired-find-file)
            ;; Fila números
            (local-set-key (kbd "'")           #'dired-hide-details-mode)
            (local-set-key (kbd "¿")           'my-dired-get-size)
            (local-set-key (kbd "¡")           'my-dired-get-size)
            (local-set-key (kbd "<backspace>") #'dired-up-directory)
            ;; Fila 1
            (local-set-key (kbd "q")     'kill-this-buffer)
            (local-set-key (kbd "W")     'my-copy-yank-to-clipboard)
            (local-set-key (kbd "r")     'my-dired-do-rename)
            (local-set-key (kbd "R")     'dired-do-rename)
            (local-set-key (kbd "T")     'my-terminal-here)
            (local-set-key (kbd "I")     #'dired-remove-subtree-view)
            (local-set-key (kbd "i")     'my-dired-show-file-attributes)
            (local-set-key (kbd "+")     'my-dired-create-directory)
            ;; Fila 2
            (local-set-key (kbd "d")     'dired-do-delete)
            (local-set-key (kbd "f")     'my-jump-to-file-or-directory)
            (local-set-key (kbd "g")     nil)
            (local-set-key (kbd "g.")    'dired-g.)
            (local-set-key (kbd "g1")    'dired-g1)
            (local-set-key (kbd "g2")    'dired-g2)
            (local-set-key (kbd "g3")    'dired-g3)
            (local-set-key (kbd "gc")    'dired-gc)
            (local-set-key (kbd "gd")    'dired-gd)
            (local-set-key (kbd "ge")    'dired-ge)
            (local-set-key (kbd "gg")    (kbd "M-< C-n C-n C-n"))
            (local-set-key (kbd "gh")    'dired-gh)
            (local-set-key (kbd "gi")    'dired-gi)
            (local-set-key (kbd "gm")    'dired-gm)
            (local-set-key (kbd "go")    'dired-go)
            (local-set-key (kbd "gv")    'dired-gv)
            (local-set-key (kbd "g7")    'dired-g7)
            (local-set-key (kbd "gt")    (lambda () (interactive) (call-interactively #'bookmark-jump)))
            (local-set-key (kbd "G")     nil)
            (local-set-key (kbd "G")     (kbd "M-> C-p"))
            (local-set-key (kbd "h")     #'dired-up-directory)
            (local-set-key (kbd "j")     #'dired-next-line)
            (local-set-key (kbd "J")     #'find-file)
            (local-set-key (kbd "k")     #'dired-previous-line)
            (local-set-key (kbd "l")     #'dired-find-file)
            (local-set-key (kbd "ñ")     'my-bin-quick-preview)
            (local-set-key (kbd "{")     'my-dired-switch-buffer)
            (local-set-key (kbd "}")     'my-dired-create-empty-file)
            ;; Fila 3
            (local-set-key (kbd "z")     'my-dired-compress)
            (local-set-key (kbd "b")     'my-dired-fuzzy-open)
            (local-set-key (kbd "B")     'wdired-change-to-wdired-mode)
            (local-set-key (kbd "n")     nil)
            (local-set-key (kbd "ncc")   #'dired-do-compress)
            (local-set-key (kbd "nct")   #'dired-do-compress-to)
            (local-set-key (kbd "ng")    #'my-dired-open-file-manager)
            (local-set-key (kbd ".")     #'dired-omit-mode)
            ;; Fila barra espaciadora
            (local-set-key (kbd "SPC")   #'dired-mark)
            (local-set-key (kbd "S-SPC") #'dired-unmark-backward)
            (local-set-key (kbd "M-s")   'nil)
            ))

(provide 'my-dired)
;;; my-dired.el ends here
