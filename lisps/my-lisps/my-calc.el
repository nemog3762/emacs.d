;;; my-calc.el --- my functions for calculations -*- lexical-binding: t; -*-

;;; Usefull constants

(defconst my-calc-binary-octal-decimal-hexadecimal-table
  "
|  N | Binary | Octal | Hexadecimal |      |  N | Binary | Octal | Hexadecimal |
|----+--------+-------+-------------|      |----+--------+-------+-------------|
|  0 |   0000 |     0 |           0 |      | 16 |  10000 |    20 |          10 |
|  1 |   0001 |     1 |           1 |      | 17 |  10001 |    21 |          11 |
|  2 |   0010 |     2 |           2 |      | 18 |  10010 |    22 |          12 |
|  3 |   0011 |     3 |           3 |      | 19 |  10011 |    23 |          13 |
|  4 |   0100 |     4 |           4 |      | 20 |  10100 |    24 |          14 |
|  5 |   0101 |     5 |           5 |      | 21 |  10101 |    25 |          15 |
|  6 |   0110 |     6 |           6 |      | 22 |  10110 |    26 |          16 |
|  7 |   0111 |     7 |           7 |      | 23 |  10111 |    27 |          17 |
|  8 |   1000 |    10 |           8 |      | 24 |  11000 |    30 |          18 |
|  9 |   1001 |    11 |           9 |      | 25 |  11001 |    31 |          19 |
| 10 |   1010 |    12 |           A |      | 26 |  11010 |    32 |          1A |
| 11 |   1011 |    13 |           B |      | 27 |  11011 |    33 |          1B |
| 12 |   1100 |    14 |           C |      | 28 |  11100 |    34 |          1C |
| 13 |   1101 |    15 |           D |      | 29 |  11101 |    35 |          1D |
| 14 |   1110 |    16 |           E |      | 30 |  11110 |    36 |          1E |
| 15 |   1111 |    17 |           F |      | 31 |  11111 |    37 |          1F |
" "Table of binary, octal, decimal and hexadecimal from 0d to 31d")

(defconst my-calc-bcd-table
  "
|    | 8421 BCD  | 4221 BCD  | 5421 BCD  |
|  N | 8421 8421 | 4221 4221 | 5421 5421 |
|----+-----------+-----------+-----------|
|  0 | 0000 0000 | 0000 0000 | 0000 0000 |
|  1 | 0000 0001 | 0000 0001 | 0000 0001 |
|  2 | 0000 0010 | 0000 0010 | 0000 0010 |
|  3 | 0000 0011 | 0000 0011 | 0000 0011 |
|  4 | 0000 0100 | 0000 0100 | 0000 0100 |
|  5 | 0000 0101 | 0000 0101 | 0000 0101 |
|  6 | 0000 0110 | 0000 0110 | 0000 0110 |
|  7 | 0000 0111 | 0000 0111 | 0000 0111 |
|  8 | 0000 1000 | 0000 1000 | 0000 1000 |
|  9 | 0000 1001 | 0000 1001 | 0000 1001 |
| 10 | 0001 0000 | 0001 0000 | 0001 0000 |
| 11 | 0001 0001 | 0001 0001 | 0001 0001 |
| 12 | 0001 0010 | 0001 0010 | 0001 0010 |
| 13 | 0001 0011 | 0001 0011 | 0001 0011 |
| 14 | 0001 0100 | 0001 0100 | 0001 0100 |
| 15 | 0001 0101 | 0001 0101 | 0001 0101 |
| 16 | 0001 0110 | 0001 0110 | 0001 0110 |
| 17 | 0001 0111 | 0001 0111 | 0001 0111 |
| 18 | 0001 1000 | 0001 1000 | 0001 1000 |
| 19 | 0001 1001 | 0001 1001 | 0001 1001 |
| 20 | 0010 0000 | 0010 0000 | 0010 0000 |
" "Table of BCD")

;;;; Here only calculations

;;; my-calc-days-since-date
(defun my-calc-days-since-date (&optional date-string)
  "Calculate and display the time elapsed since DATE-STRING in days, weeks, months, and years.
DATE-STRING should be in the format YYYY-MM-DD."
  (interactive)
  (let* ((date-string (or date-string "2024-11-16"))
         (parsed-date (parse-time-string date-string))
         (input-day (nth 3 parsed-date))
         (input-month (nth 4 parsed-date))
         (input-year (nth 5 parsed-date))
         (input-time (encode-time 0 0 0 input-day input-month input-year))
         (current-time (current-time))
         (current-date (decode-time current-time))
         (current-day (nth 3 current-date))
         (current-month (nth 4 current-date))
         (current-year (nth 5 current-date))
         (seconds-difference (float-time (time-subtract current-time input-time))) ;; Diferencia en segundos
         (days-difference (floor (/ seconds-difference (* 24 60 60))))             ;; Diferencia en días
         (weeks-difference (/ days-difference 7.0))                                ;; Diferencia en semanas
         (months-difference (if (>= current-day input-day)
                               (+ (* (- current-year input-year) 12)
                                  (- current-month input-month))
                             (+ (* (- current-year input-year) 12)
                                (- current-month input-month 1))))
         (years-difference (/ days-difference 365.25)))                            ;; Diferencia en años (considerando años bisiestos)
    (message "Since %s: %d days, %.2f weeks, %d months, %.2f years"
             date-string days-difference weeks-difference months-difference years-difference)))

(when (fboundp 'my-calc-days-since-date)
  (defun display-startup-echo-area-message ()
    (my-calc-days-since-date)))

;;; my-calc-bmi
(defun my-calc-bmi ()
  "Calculate adult BMI.
Source: https://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/index.html"
  (interactive)
  (let* ((peso (read-number "Weight in kilograms: " 70))
         (altura-cm (read-number "Height in cms: " 170))
         (bmi (/ peso (* (/ altura-cm 100.0) (/ altura-cm 100.0)))))
    (message "Below - 18.5     Underweight
18.5  – 24.9     Healthy Weight
25.0  – 29.9     Overweight
30.0  - Above    Obesity
BMI: %.2f" bmi)))

;;; my-calc-random-number-dice-dado
(defun my-calc-random-number-dice-dado (&optional caras)
  "Print a random number between 1 and N (N by default is 6)."
  (interactive)
  (let ((num-caras (or caras (read-number "Number max: " 6))))
    (message "%s" (+ 1 (random num-caras)))))

;;; my-calc-random-speakers
(defun my-calc-random-speakers ()
  "Get a random number M between 1 and N and list of M numbers from 1 to N."
  (interactive)
  (let* (
         (number (read-number "Enter the number of members: " 4))
         (m (1+ (random number)))
         (numbers (number-sequence 1 number))
         (result '()))
    (while (> m 0)
      (let ((random-index (random (length numbers))))
        (setq result (cons (nth random-index numbers) result))
        (setq numbers (append (seq-take numbers random-index)
                              (seq-drop numbers (1+ random-index))))
        (setq m (1- m))))
    (message "Members: %d\nSpeakers: %d\nOrder: %s" number (length result) result)
    result))

;;; my-calc-test-result
(defun my-calc-test-result ()
  "Calculate test result in range 0-5, 0-10 and 0-100"
  (interactive)
  (let* ((correct (max 1 (string-to-number (read-string "Correct answers? [1]: "))))
         (total   (max 1 (string-to-number (read-string "Total of questions? [1]: "))))
         (ratio   (/ correct (float total))))
    (message "%s of %s
Result 0-5:   %.2f
Result 0-10:  %.2f
Result 0-100: %.2f"
             correct total
             (* ratio 5) (* ratio 10) (* ratio 100))))

;;; my-calc-radix-convert
(defun my-calc-radix-convert (&optional input base-1 base-2)
  "Convert a number from BASE-1 to BASE-2."
  (interactive)
  (let* ((input  (or input (read-string "Write number: ")))
         (base-1 (or base-1 (string-to-number (read-string "Specify base of origin (default 10): " "10"))))
         (base-2 (or base-2 (string-to-number (read-string "Specify base of destiny (default 10): " "10"))))
         (number (string-to-number input base-1))) ;; Convert input to a number using base-1
    ;; Convert to the target base and display the result
    (message "Result in base %d: %s"
             base-2
             (cond
              ((= base-2 10) (number-to-string number)) ;; Decimal
              ((= base-2 16) (format "%X" number))      ;; Hexadecimal
              ((= base-2 8) (format "%o" number))       ;; Octal
              ((= base-2 2) (let ((bin ""))
                              (while (> number 0)
                                (setq bin (concat (number-to-string (% number 2)) bin))
                                (setq number (/ number 2)))
                              bin))                      ;; Binary
              (t (error "Unsupported destination base: %d" base-2))))))

(provide 'my-calc)
;;; my-calc.el ends here
