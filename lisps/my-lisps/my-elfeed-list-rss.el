(setq elfeed-feeds
      '(
        ;;; Noticias
        ( "https://morss.it/https://politepol.com/fd/Fzt3QsmcJlTq.xml" news )                   ;; Zona Cero
        ( "https://odysee.com/$/rss/@ahilesva:e"                 video news )                   ;; Ahí les va
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCu2cUfy1hmjlcfZHzvVuEgg" news ) ;; La Pulla
        ;;; Cristo
        ( "https://biteproject.com/feed/"                                                Cristo   )
        ( "https://www.coalicionporelevangelio.org/feed/"                                Cristo   )
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCSkfXhOqioLNNuN6JPY6yXQ" Cristo   )  ;; BITE
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCbCiicK83dcJT6PMGqvqBDw" Cristo   )  ;; ResMiChu
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCIBHedQqdaF6tvJSuWaRMkA" Cristo   )  ;; CFA
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCXZAKJKu3ErNJ89Odx7jOdw" Cristo   )  ;; Iglesia Bautista El Divino Maestro
        ( "https://iglesiaevangelicabautistaibi.com/feed/"                               Cristo   )  ;; Iglesia Bautista de IBI El Salvador
        ;;; Historia
        ( "https://historia.nationalgeographic.com.es/feeds/rss"                         Historia )
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCBIMW0ZhwULY_x7fdaPRPiQ" Historia )  ;; Pero eso es otra Historia"
        ;;; Arte
        ( "https://historia-arte.com/obras.rss" )
        ( "https://www.hislibris.com/wp-rss2.php" )
        ;;; Emacs
        ( "http://xahlee.info/emacs/emacs/blog.xml"  ) ;; Xah Emacs
        ( "https://notxor.nueva-actitud.org/rss.xml" ) ;; Notxor tiene un blog
        ( "https://elblogdelazaro.org/index.xml"     ) ;; El Blog de Lázaro
        ( "https://sachachua.com/blog/feed"          ) ;; Sacha Chua
        ;;; Tec / GNU-Linux / Emacs
        ( "https://facilitarelsoftwarelibre.blogspot.com/feeds/posts/default?alt=rss"     )
        ( "https://fullcirclemagazine.org/index.xml"                                      )
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UC5TawXio93r576Rl0UglVDg"  ) ;; Mente Humana
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCyl5V3-J_Bsy3x-EBCJwepg"  ) ;; The Babylon Bee
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCASHFqClADQZTTHhlleOLiA"  ) ;; Kiun B
        ;;; Otros
        ( "https://feeds2.feedburner.com/TheArtOfManliness"                              ) ;; The Art of Manliness
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCoXtmmnLCbXDiSo8GxsmOzA" ) ;; La gata de Schrödinger
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCWt_tMCPFbCHIy3xoomKkXw" ) ;; Kaltenberger Ritterturnier
        ( "https://www.youtube.com/feeds/videos.xml?channel_id=UCY1U4aQVlmjvs96OrNwWEfQ" ) ;; David Tuiran - El Profe Curioso
        ))
