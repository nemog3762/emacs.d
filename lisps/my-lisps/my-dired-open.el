;;; my-dired-open.el --- my dired configs -*- lexical-binding: t; -*-

(require 'dired)

;; Definir una variable para almacenar una lista de extensiones de archivos.
(defvar my-dired-open-ignore-extensions '(
                                          "kdbx"
                                          "pdf" "PDF"               ;; pdf
                                          "zip" "7z" "rar" "xz"     ;; Comprimidos
                                          "doc" "xls" "ppt"         ;; Office (old)
                                          "docx" "xlsx" "pptx"      ;; Office (new)
                                          "odt" "ods" "odp" "odg"   ;; LibreOffice
                                          "mp3" "m4a" "ogg" "wav"   ;; Audio
                                          "mp4" "MP4" "mkv" "avi"   ;; Vídeo
                                          "JPG" "JPEG" "jpg" "jpeg" "png" "webp" ;; Imagen
                                          "dmg"
                                          ;;"epub"
                                          )
  "Extensiones para abrir con 'my-open-in-default-app' en 'dired'.")
(defun xah-open-in-external-app (&optional Fname)
  "Open the current file or dired marked files in external app.
When called in emacs lisp, if Fname is given, open that.
URL `http://xahlee.info/emacs/emacs/emacs_dired_open_file_in_ext_apps.html'
Version: 2019-11-04 2023-04-05 2023-06-26"
  (interactive)
  (let (xfileList xdoIt)
    (setq xfileList
          (if Fname
              (list Fname)
            (if (eq major-mode 'dired-mode)
                (dired-get-marked-files)
              (list buffer-file-name))))
    (setq xdoIt (if (<= (length xfileList) 10) t
                  (y-or-n-p "Open more than 10 files? ")))
    (when xdoIt
      (cond
       ((eq system-type 'windows-nt)
        (let ((xoutBuf (get-buffer-create "*xah open in external app*"))
              (xcmdlist
               (list "PowerShell" "-Command" "Invoke-Item" "-LiteralPath")))
          (mapc (lambda (x)
                  (message "%s" x)
                  (apply 'start-process
                         (append
                          (list "xah open in external app" xoutBuf) xcmdlist
                          (list
                           (format "'%s'"
                                   (if
                                       (string-match "'" x)
                                       (replace-match "`'" t t x) x))) nil)))
                xfileList)))
       ((eq system-type 'darwin)
        (mapc (lambda
                (xfpath)
                (shell-command
                 (concat "open " (shell-quote-argument xfpath)))) xfileList))
       ((eq system-type 'gnu/linux)
        (mapc (lambda (xfpath)
                (call-process shell-file-name nil 0 nil
                              shell-command-switch
                              (format "%s %s"
                                      "xdg-open"
                                      (shell-quote-argument xfpath))))
              xfileList))
       ((eq system-type 'berkeley-unix)
        (mapc
         (lambda
           (xfpath)
           (let
               ((process-connection-type nil))
             (start-process "" nil "xdg-open" xfpath))) xfileList))))))
(defalias 'my-open-in-default-app 'xah-open-in-external-app)
;; Abrir archivos con extensiones específicas usando 'my-open-in-default-app'
(defun my-dired-open-with-my-default-app ()
  "Open files in 'my-dired-open-ignore-extensions' using 'my-open-in-default-app'"
  ;; Obtener la ruta completa del archivo
  (let ((file-path (dired-get-file-for-visit))
        ;; Obtener la extensión del archivo
        (file-extension (file-name-extension (dired-get-file-for-visit))))
    ;; Si la extensión del archivo está en 'my-dired-open-ignore-extensions'
    (when (member file-extension my-dired-open-ignore-extensions)
      ;; Abrir el archivo con 'my-open-in-default-app'
      (my-open-in-default-app file-path)
      ;; Indicar que el archivo se ha manejado.
      t)))
;; Definir un consejo (advice) para la función `dired-find-file`.
(defun my-dired-find-file-advice (orig-fun &rest args)
  "Advice for 'dired-find-file' to open files with 'my-open-in-default-app'."
  ;; Si el archivo no se abre con 'my-open-in-default-app'
  (unless (my-dired-open-with-my-default-app)
    ;; Proceder con la función original para abrir el archivo (abrir en dired)
    (apply orig-fun args)))

;; Aplicar el consejo a 'dired-find-file', rodeándolo con función personalizada.
;; Puede ser aplicado con 'dired-find-file' o 'dired-find-alternate-file'
(advice-add 'dired-find-alternate-file :around #'my-dired-find-file-advice)
(advice-add 'dired-find-file :around #'my-dired-find-file-advice)

(provide 'my-dired-open)
;;; my-dired-open.el ends here
