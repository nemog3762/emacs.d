(require 'elfeed)

(global-set-key (kbd "<f1>e")    'elfeed)

;;; my functions

(defun my-elfeed-mark-all-as-read ()
  "Mark all feed as read"
  (interactive)
  (mark-whole-buffer)
  (elfeed-search-untag-all-unread))

(defun my-elfeed-eww ()
  "Open URL feed in eww"
  (interactive)
  (let* ((link (cond
                ;; Dentro del feed, obtener la URL
                ((string-equal major-mode "elfeed-show-mode")
                 (elfeed-entry-link elfeed-show-entry))
                ;; En la lista de todos los feeds
                ((string-equal major-mode "elfeed-search-mode")
                 (elfeed-entry-link (elfeed-search-selected :single))))))
    (eww link)))

;; Hide cursor and show as 'less'
;; https://karthinks.com/software/more-less-emacs/
(defvar-local hide-cursor--original nil)
(define-minor-mode hide-cursor-mode
  "Hide or show the cursor.
When the cursor is hidden `scroll-lock-mode' is enabled, so that
the buffer works like a pager."
  :global nil
  :lighter "H"
  (if hide-cursor-mode
      (progn
        (scroll-lock-mode 1)
        (setq-local hide-cursor--original
                    cursor-type)
        (setq-local cursor-type nil))
    (scroll-lock-mode -1)
    (setq-local cursor-type (or hide-cursor--original
                                t))))

;; Declickbait elfeed
;; Basado en: https://karthinks.com/software/declickbait-elfeed/
(defun elfeed-title-transform (title)
  "Transform TITLE, all letters in downcase."
  (let* (;; Expresión regular para eliminar ciertos caracteres de puntuación
         (trim "\\(?:\\(?:\\.\\.\\.\\|[!?]\\)+\\)")
         ;; Divide el título en palabras
         (arr (split-string title nil t trim))
         ;; Copia la tabla de sintaxis actual
         (s-table (copy-syntax-table)))
    ;; Modifica la sintaxis para tratar apóstrofes como parte de la palabra
    (modify-syntax-entry ?\' "w" s-table)
    (with-syntax-table s-table
      (mapconcat (lambda (word)
                   (downcase word)) ;; Convertir cada palabra a minúsculas
                 arr " "))))        ;; Unir las palabras en una sola cadena
(defun elfeed-declickbait-entry (entry)
  (let ((title (elfeed-entry-title entry)))
    (setf (elfeed-meta entry :title)
          (elfeed-title-transform title))))
(add-hook 'elfeed-new-entry-hook #'elfeed-declickbait-entry)

(progn
  (defun timu-elfeed-filter-include-tag ()
    "Use `completing-read' to select tags to include `+'.
The function reads the tags from the `elfeed' db."
    (interactive)
    (let ((filtered-tag (completing-read "Select Tags to include: "
                                         (elfeed-db-get-all-tags))))
      (progn
        (setq elfeed-search-filter
              (concat elfeed-search-filter " +" filtered-tag))
        (elfeed-search-update--force))))
  (defun timu-elfeed-filter-exclude-tag ()
    "Use `completing-read' to select tags to exclude `-'.
The function reads the tags from the `elfeed' db."
    (interactive)
    (let ((filtered-tag (completing-read "Select Tags to exclude: "
                                         (elfeed-db-get-all-tags))))
      (progn
        (setq elfeed-search-filter
              (concat elfeed-search-filter " -" filtered-tag))
        (elfeed-search-update--force)))))

(defun my-elfeed-mpv ()
  (interactive)
  (let* (
         (entry (elfeed-search-selected :single))
         (url (elfeed-entry-link entry))
         )
    (call-process shell-file-name nil 0 nil
                  shell-command-switch
                  (format "mpv %s"
                          (shell-quote-argument url)))))

(defun my-elfeed-mpv ()
  "Open the selected Elfeed entry's link in mpv asynchronously."
  (interactive)
  (let* ((entry (elfeed-search-selected :single))
         (url (elfeed-entry-link entry)))
    (if url
        (start-process "mpv-process" nil "mpv" url)
      (message "No URL found for the selected entry."))))


;;; Options

;; Emacs 28.2 error in macOS Ventura: `image-type: Invalid image type ‘svg’`
(when (eq system-type 'darwin)
  (setq image-types '(svg png gif tiff jpeg xpm xbm pbm)))

(setq
 ;; Incrementar tiempo de consultas.
 url-queue-timeout 30
 ;; Ancho del texto (eso se omite con olivetti para centrar el texto)
 shr-width 70

 ;; Ubicación del cache de elfeed
 elfeed-db-directory
 (cond
  ((string-equal system-type "windows-nt") "C:/elfeed")
  ((string-equal system-type "gnu/linux")  "~/.cache/elfeed")
  ((string-equal system-type "darwin")     "~/.cache/elfeed")
  (t (error "Unsupported operating system")))

 ;; Title width
 elfeed-search-title-max-width 120
 elfeed-search-title-min-width 80
 )

;;; Aliases
(defalias 'elfeed-copy-yank-url 'elfeed-show-yank)

;;; Hooks

;; Mark all YouTube entries
(add-hook 'elfeed-new-entry-hook
          (elfeed-make-tagger :feed-url "youtube\\.com"
                              :add '(video youtube)))

;; Mark all Reddit entries
(add-hook 'elfeed-new-entry-hook
          (elfeed-make-tagger :feed-url "reddit\\.com"
                              :add '(reddit)))

;; Entries older than 2 weeks are marked as read
(add-hook 'elfeed-new-entry-hook
          (elfeed-make-tagger :before "2 weeks ago"
                              :remove 'unread))

;; Al abrir elfeed
;; En la lista de feeds
(add-hook 'elfeed-search-mode-hook
          (lambda ()
            (elfeed-update)
            (visual-line-mode -1)
            (local-set-key (kbd "<RET>")   'elfeed-search-show-entry)
            (local-set-key (kbd "<right>") 'elfeed-search-show-entry)
            (local-set-key (kbd "G")       (kbd "M-> C-p"))
            (local-set-key (kbd "a")       'my-elfeed-mark-all-as-read)
            (local-set-key (kbd "v")       'my-elfeed-mpv)
            (local-set-key (kbd "d")       'elfeed-update)
            (local-set-key (kbd "e")       'my-elfeed-eww)
            (local-set-key (kbd "g")       'beginning-of-buffer)
            (local-set-key (kbd "j")       'next-line)
            (local-set-key (kbd "k")       'previous-line)
            (local-set-key (kbd "l")       'elfeed-search-show-entry)
            (local-set-key (kbd "x")       'timu-elfeed-filter-exclude-tag)
            (local-set-key (kbd "z")       'timu-elfeed-filter-include-tag)
            (local-set-key (kbd "h")        nil)
            (local-set-key (kbd "q")        (lambda () (interactive)
                                              (kill-this-buffer)
                                              (if (get-buffer "*elfeed-log*")
                                                (kill-buffer "*elfeed-log*"))
                                              (if (get-buffer "*elfeed-tube-log*")
                                                (kill-buffer "*elfeed-tube-log*"))
                                              ))))

;; Al mostrar el contenido de un feed
(add-hook 'elfeed-show-mode-hook
          (lambda ()
            (hide-cursor-mode)
            ;; Keyboard
            (local-set-key (kbd "G")      (kbd "M->"))
            (local-set-key (kbd "j")      'next-line)
            (local-set-key (kbd "k")      'previous-line)
            (local-set-key (kbd "{")      'backward-paragraph)
            (local-set-key (kbd "}")      'forward-paragraph)
            (local-set-key (kbd "h")      'elfeed-kill-buffer)
            (local-set-key (kbd "<left>") 'elfeed-kill-buffer)
            (local-set-key (kbd "e")      'my-elfeed-eww)
            ))
