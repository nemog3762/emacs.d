;;;; defult config of key shortcuts

;;; General

;; 'C-a' move to beginning of logical line ('beginning-of-line') instead of 'beginning-of-visual-line'
(global-set-key (kbd "C-a") 'beginning-of-line)

;; 'C-e' move to end of logical line ('end-of-line') instead of 'move-end-of-line'
(global-set-key (kbd "C-e") 'end-of-line)

;; delete line with `C-k'
(global-set-key (kbd "C-k") 'kill-whole-line)

;; 'cua-mode'
;; By default: copy, cut and paste with 'M-w', 'C-w' and 'M-y'.
;; 'cua-mode' allows  copy, cut and paste with 'C-c', 'C-x' y 'C-v'
(cua-mode 1)

;; Press 'ESC' once to quit promts, by default, has to be 3 times.
(global-set-key [escape] 'keyboard-escape-quit)

;; Fast 'M-x'
;; In GNU/Linux 'menu' key execute 'M-x'
;; In Windows the key name is 'apps'
;; With this, 'apps' is the same as 'menu'.
(when (eq system-type 'windows-nt)
  (global-set-key (kbd "<apps>") 'execute-extended-command))

;;; Replace

;; 'C-r' runs 'query-replace'. By defaults if 'M-%'
(global-set-key (kbd "C-r")  'query-replace)

;; 'C-S-r' runs 'query-replace-regexp'
(global-set-key (kbd "C-S-r") 'query-replace-regexp)

;;; Zoom or zum

(defun text-scale-normal-size ()
  "Set the height of the default face in the current buffer to its default value."
  (interactive)
  (text-scale-increase 0))
(global-set-key (kbd "C-=")            'text-scale-normal-size)
(global-set-key (kbd "C-+")            'text-scale-increase)
(global-set-key (kbd "C--")            'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>")   'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

;;; Delete

;; Delete Word Without Copying to Clipboard/kill-ring and in a normal way!
;; Version 1: http://xahlee.info/emacs/emacs/emacs_kill-ring.html (delete too much)
;; Version 2: https://stackoverflow.com/a/77762026
;; Version 3: Based on version 2 but doesn't kill to ring
(defun my-delete-forward-word ()
  "Delete forward until encountering a space, newline, or punctuation.
With argument ARG, delete that many times."
  (interactive)
  (cond
   ((looking-at (rx (char word)) 1)  (delete-region (point) (save-excursion (forward-word) (point))))
   ((looking-at (rx (char blank)) 1) (delete-horizontal-space))
   (t (delete-char 1))))
(defun my-delete-backward-word ()
  "Delete backward word."
  (interactive)
  (cond
   ((looking-back (rx (char word)) 1)  (delete-region (save-excursion (backward-word) (point)) (point)))
   ((looking-back (rx (char blank)) 1) (delete-horizontal-space t))
   (t (delete-char -1))))
;; bind them to emacs's default shortcut
(global-set-key (kbd "C-<delete>")    'my-delete-forward-word)
(global-set-key (kbd "C-<backspace>") 'my-delete-backward-word)

;;; set all function keys as 'nil'
(dotimes (i 12)
  (global-set-key (kbd (format "<f%s>" (1+ i))) nil))

;;; Others
(progn
  ;; f1
  (global-set-key (kbd "M-<f1>")   'repeat-complex-command)
  (global-set-key (kbd "<f1><f1>") 'my-toggle-notes)
  (global-set-key (kbd "<f1>d")    'dired-jump)
  (global-set-key (kbd "<f1>o")    'my-choose-open)
  (global-set-key (kbd "<f1>w")    'eww)
  (global-set-key (kbd "<f1>c")    'quick-calc)
  (global-set-key (kbd "<f1>bs")   'bookmark-set)
  (global-set-key (kbd "<f1>bj")   'bookmark-jump)
  (global-set-key (kbd "<f1>bd")   'bookmark-delete)
  ;; f2
  (global-set-key (kbd "<f2><f1>") 'clone-frame)
  (global-set-key (kbd "<f2><f2>") 'other-frame)
  (global-set-key (kbd "<f2><f3>") 'delete-frame)
  ;; f3
  (global-set-key (kbd "C-<f3>")   'kmacro-start-macro-or-insert-counter)
  (global-set-key (kbd "<f3>i")    'my-bin-image-viewer)
  (global-set-key (kbd "<f3>o")    'my-choose-open)
  (global-set-key (kbd "<f3>th")   'my-terminal-here)
  (global-set-key (kbd "<f3>ti")   'my-terminal-in)
  (global-set-key (kbd "<f3>r")    'my-bin-ripgrep)
  ;; f4
  (global-set-key (kbd "<f4>")    'my-window-toggle-cycle-layout)
  (global-set-key (kbd "C-<f4>")  'kmacro-end-or-call-macro)
  ;; f5
  (global-set-key (kbd "<f5>")    'my-lookup-search-web)
  ;; Buffer related
  (global-set-key (kbd "<f6>") 'my-buffer-previous-user)
  (global-set-key (kbd "<f7>") 'my-buffer-next-user)
  (global-set-key (kbd "<f8>") 'my-switch-to-last-buffer)
  (global-set-key (kbd "<f9>") 'switch-to-buffer)
  ;; f12
  ;; (global-set-key (kbd "<f12>")  'toggle-viper-mode)
  )

;;; mouse

;; sample setting for mouse button and wheel
;; Emacs 29 syntax
;; For emacs 28, use global-set-key
(cond
 ((eq system-type 'windows-nt)
  (keymap-global-set "<mouse-4>" 'describe-char))
 ((eq system-type 'darwin) ; Mac
  (keymap-global-set "<mouse-4>" 'describe-char))
 ((eq system-type 'gnu/linux)
  (keymap-global-set "<mouse-8>" 'describe-char)))

;; (keymap-global-set "M-<wheel-up>"   'previous-buffer)
;; (keymap-global-set "M-<wheel-down>" 'next-buffer)
