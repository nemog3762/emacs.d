;; Si es con opciones por defecto, se usan los comandos 'isearch', 'icomplete', 'hippie-expand'
;; Si es con complementos externos, se usan los paquetes 'ivy', 'counsel', 'swiper', 'company', entre otros.
;; Paquetes como 'ivy' no son compatibles con 'icomplete'. Solo se puede activar o usar uno al tiempo.

;;; Vars
(setq read-file-name-completion-ignore-case t)
(setq read-buffer-completion-ignore-case t)
(setq completion-ignore-case t)
(setq icomplete-in-buffer t)

;;; Completion styles
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Completion-Styles.html
;; Built-in options: 'basic', 'partial-completion', 'emacs22', 'substring', 'flex', 'initials'
;; Externals options: 'orderless'

;; Use 'orderless' if possible, or use 'flex'
(if (require 'orderless nil t)
    (setq completion-styles '(orderless))
  ;; default emacs 29
  (setq completion-styles '(basic partial-completion emacs22)))

;;; word completion

;; 'hippie-expand' for word completion
;; 1. 'built-in'
;; 2. 'hippie-expand' expande la palabra bajo el cursor a cualquiera existente en cualquier buffer.
;; 3. Alternativa es el complemento 'company', pero presenta algunos bugs.

;; Only expand words
(setq hippie-expand-try-functions-list '(
                                         try-expand-dabbrev
                                         try-expand-dabbrev-all-buffers
                                         ;; try-expand-dabbrev-from-kill
                                         try-complete-lisp-symbol-partially
                                         try-complete-lisp-symbol
                                         try-complete-file-name-partially
                                         try-complete-file-name
                                         ;; try-expand-all-abbrevs
                                         ;; try-expand-list
                                         ;; try-expand-line
                                         ))
(global-set-key (kbd "C-t") 'hippie-expand)

;;; 'icomplete'
;; (icomplete-mode)
;; (setq icomplete-separator "\n")
;; (setq icomplete-hide-common-prefix t)
;; (setq icomplete-in-buffer t)
;; (define-key icomplete-minibuffer-map (kbd "<right>") 'icomplete-forward-completions)
;; (define-key icomplete-minibuffer-map (kbd "<left>")  'icomplete-backward-completions)
;; (define-key icomplete-minibuffer-map (kbd "<down>")  'icomplete-forward-completions)
;; (define-key icomplete-minibuffer-map (kbd "<up>")    'icomplete-backward-completions)
;; (define-key icomplete-minibuffer-map (kbd "RET")     'icomplete-force-complete-and-exit)
;; (define-key icomplete-minibuffer-map (kbd "<tab>")   'icomplete-force-complete)

;;; 'icomplete-vertical' ('built-in')
(fido-mode 0)
(setq icomplete-prospects-height 10)
(setq icomplete-show-matches-on-no-input t)
;; Enable
(icomplete-vertical-mode t)
;; Maps
(define-key icomplete-minibuffer-map (kbd "RET")   'icomplete-force-complete-and-exit)
(define-key icomplete-minibuffer-map (kbd "<tab>") 'icomplete-force-complete)

;;; fido-mode ('built-in')

;; ;; Activate to view the num candidates in minibuffer
;; ;; https://robbmann.io/posts/007_emacs_3_discovering/
;; ;; Turn on FIDO (Fake IDO) mode
;; (fido-mode)
;; ;; Have TAB complete using the first option and continue, instead of popping up the *Completions* buffer
;; (define-key icomplete-minibuffer-map [remap minibuffer-complete] 'icomplete-force-complete)
;; ;; Sometimes I have to customize this icomplete-compute-delay variable to 0.0 to avoid delay before the M-x minibuffer pops up
;; (setq icomplete-compute-delay 0.0)
;; ;; Set the display to be a vertical list of items, instead of a horizontal one
;; (fido-vertical-mode)
;; ;; Keys
;; (define-key icomplete-fido-mode-map (kbd "<SPC>") 'minibuffer-complete-word)
;; (define-key icomplete-fido-mode-map (kbd "<left>")  'backward-char)
;; (define-key icomplete-fido-mode-map (kbd "<right>") 'forward-char)
