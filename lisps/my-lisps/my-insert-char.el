;; Ideas from:
;; http://xahlee.info/emacs/emacs/emacs_insert_unicode.html
;; http://xahlee.info/emacs/emacs/xmsi-math-symbols-input.html

(defvar my-insert-char-alist
  '(
    ("pound" . "£")
    ("cent"  . "¢")
    ;; Math
    ("micro"   . "µ")
    ("alpha"   . "α")
    ("beta"    . "β")
    ("gamma"   . "γ")
    ("delta"   . "δ")
    ("epsilon" . "ε")
    ("zeta"    . "ζ")
    ("eta"     . "η")
    ("theta"   . "θ")
    ("iota"    . "ι")
    ("kappa"   . "κ")
    ("lambda"  . "λ")
    ("mu"      . "μ")
    ("nu"      . "ν")
    ("xi"      . "ξ")
    ("omicron" . "ο")
    ("pi"      . "π")
    ("rho"     . "ρ")
    ("sigma"   . "σ")
    ("tau"     . "τ")
    ("upsilon" . "υ")
    ("phi"     . "φ")
    ("chi"     . "χ")
    ("psi"     . "ψ")
    ("omega"   . "ω")
    ;; Esperanto
    ("c1" . "ĉ")
    ("g1" . "ĝ")
    ("h1" . "ĥ")
    ("j1" . "ĵ")
    ("s1" . "ŝ")
    ("u1" . "û")
    ;; Others
    ("ampersand"   . "&")
    ("arroba"      . "@")
    ("brackets"    . "[]")
    ("parentheses" . "()")
    ("numeral"     . "#")
    ("hashtag"     . "#")
    ("bar"         . "|")
    ("backslash"   . "\\"))
  "Alist of word conversions. Each key is replaced by its associated value and vice versa.")

(defun my-insert-char-from-alist ()
  "Insert a character from `my-insert-char-alist` using `completing-read`.
Allows inserting either the key or the value."
  (interactive)
  (let* ((choices (append (mapcar 'car my-insert-char-alist)
                          (mapcar 'cdr my-insert-char-alist)))
         (selection (completing-read "Choose a character to insert: " choices nil t)))
    (when selection
      (insert selection))))

(defun my-char-convert ()
  "Convert the word at point based on `my-insert-char-alist`.
It works in both directions: key to value or value to key."
  (interactive)
  (let* ((bounds (bounds-of-thing-at-point 'word))
         (word (when bounds (buffer-substring-no-properties (car bounds) (cdr bounds))))
         ;; Find the replacement either as a key or value
         (replacement (or (assoc-default word my-insert-char-alist)
                          (car (rassoc word my-insert-char-alist)))))
    (if (and bounds replacement)
        (progn
          (delete-region (car bounds) (cdr bounds))
          (insert replacement))
      (message "No conversion found for '%s'" (or word "current word")))))

(defvar my-insert-char-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "S-SPC") 'my-char-convert)
    map)
  "Keymap for `my-insert-char-mode`.")

;;;###autoload
(define-minor-mode my-insert-char-mode
  "A minor mode to convert words based on a predefined alist using Shift + SPACE.
Supports bi-directional conversion (key to value and value to key)."
  :lighter " CharConv"
  :keymap my-insert-char-mode-map)

(provide 'my-insert-char-mode)
