;;; my-dired-ranger.el --- my dired ranger configs -*- lexical-binding: t; -*-

;; Example of config:
;;
;; (use-package my-dired-ranger
;;   :after dired
;;   :bind
;;   (:map dired-mode-map
;;         ("y" . my-dired-ranger-copy)
;;         ("p" . my-dired-ranger-paste)
;;         ("m" . my-dired-ranger-move)))

(setq my-dired-marked-files-paths nil)

(defun my-dired-ranger-action (action)
  "Generic function to copy or move marked files in Dired.
ACTION can be 'copy or 'move."
  (interactive)
  (if my-dired-marked-files-paths
      (let ((target-dir (dired-current-directory)))
        (dolist (file my-dired-marked-files-paths)
          (cond
           ((eq action 'copy) (dired-copy-file file target-dir t))
           ((eq action 'move) (dired-rename-file file target-dir t))))
        (setq my-dired-marked-files-paths nil)
        (revert-buffer))
    (message "Can't perform action, list is empty!")))

(defun my-dired-ranger-copy ()
  "Save the path of marked files and show the amount."
  (interactive)
  (setq my-dired-marked-files-paths (dired-get-marked-files))
  (message "%d files copied." (length my-dired-marked-files-paths)))

(defun my-dired-ranger-paste ()
  "Paste the list of copied files into the current directory."
  (interactive)
  (my-dired-ranger-action 'copy))

(defun my-dired-ranger-move ()
  "Move the list of copied files to the current directory."
  (interactive)
  (my-dired-ranger-action 'move))

(provide 'my-dired-ranger)
;;; my-dired-ranger.el ends here
