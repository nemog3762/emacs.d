;;; my-bin.el --- my functions for binaries in PATH -*- lexical-binding: t; -*-

;;;; Path

;;; Path in Windows
;; Install chocolatey 'https://chocolatey.org/'
;; Guide: 'https://conasa.grupocibernos.com/blog/instalar-programas-desde-la-consola-de-windows-con-chocolatey'
(when (string-equal system-type "windows-nt")
  (setq chocolatey-root "C:/ProgramData/chocolatey")
  (setq chocolatey-lib (concat chocolatey-root "/" "lib"))
  (setq chocolatey-bin (concat chocolatey-root "/" "bin"))
  (setq exec-path
        (append '(
                  "C:/Python27"
                  "C:/ProgramData/chocolatey/bin"
                  "C:/Users/tom.purl/AppData/Local/Programs/Python/Python36/Scripts"
                  )
                exec-path)))

;;; Path in GNU/Linux
(when (string-equal system-type "gnu/linux")
  (setq exec-path (append '("~/.local/bin") exec-path)))

;;;; Flyspell

;;; Flyspell (ortografía)

;; Load 'flyspell-mode'
(defun my-flyspell-config ()
  (interactive)
  ;; Hooks
  (add-hook 'text-mode-hook 'flyspell-mode)
  (add-hook 'prog-mode-hook 'flyspell-prog-mode)
  (add-hook 'org-mode-hook  'flyspell-mode)
  ;; Keys
  (global-set-key (kbd "C->") 'flyspell-goto-next-error)
  (global-set-key (kbd "C-<") '(lambda () (interactive) (flyspell-goto-next-error t))))

;;; Flyspell Windows
(when (and (string-equal system-type "windows-nt")
           (file-exists-p "C:/msys64/mingw64/bin/aspell.exe"))
  ;; Guía: 'https://emacs.stackexchange.com/questions/41892/aspell-with-emacs-26-1-on-ms-windows'
  ;; 1. Instalar MSYS2 'https://www.msys2.org/'
  ;; 2. En el menú ejecutar 'MSYS2 MinGW 64-bit'
  ;; 3. Se puede buscar con el comando =pacman -Ss aspell=
  ;; 4. Instalar aspell con los diccionarios.
  ;;    pacman -S mingw64/mingw-w64-x86_64-aspell
  ;;    pacman -S mingw64/mingw-w64-x86_64-aspell-es
  ;;    pacman -S mingw64/mingw-w64-x86_64-aspell-en
  (setq ispell-program-name "C:/msys64/mingw64/bin/aspell.exe")
  (setq ispell-dictionary "es")
  (my-flyspell-config))

;;; Flyspell GNU/Linux.
(when (string-equal system-type "gnu/linux")
  ;; Configuración de Flyspell para verificar español usando Aspell
  (setq ispell-program-name "aspell")
  (setq ispell-dictionary "es")
  (my-flyspell-config))

;;; Flyspell macOS
;; Install 'homebrew' with 'https://brew.sh/'
;; Install 'aspell' with 'brew install aspell'
;; Link 'https://formulae.brew.sh/formula/aspell'
(when (and (string-equal system-type "darwin")
           (or (file-exists-p "/usr/local/bin/aspell")
               (file-exists-p "/opt/homebrew/bin/aspell")))
  ;; Cargar librería
  (require 'flyspell)
  ;; Ubicación de aspell
  (cond
   ((file-exists-p "/usr/local/bin/aspell")
    (setq ispell-program-name "/usr/local/bin/aspell"))
   ((file-exists-p "/opt/homebrew/bin/aspell")
    (setq ispell-program-name "/opt/homebrew/bin/aspell")))
  ;; Otras configuraciones
  (setq flyspell-issue-message-flag nil)
  (setq ispell-local-dictionary "es_CO")
  (setq ispell-extra-args '("--lang=es_CO"))
  (my-flyspell-config))

;;;; Other functions

;;; my-bin-helper-1
(defun my-bin-helper-1 (cmd &optional buffer-name)
  "Ejecuta un comando de shell CMD con un término de búsqueda y muestra el resultado en un buffer temporal.
Si BUFFER-NAME no se proporciona, se usa 'my-bin-helper-1' como nombre por defecto del buffer.
La función solicita un término de búsqueda al usuario, ejecuta CMD con el término, y muestra el
resultado en un buffer temporal. Luego, pregunta si deseas insertar el resultado en la posición
actual del cursor y finalmente cierra el buffer temporal."
  (let* (
         (input (my-get-search-term))
         (output (shell-command-to-string (format "%s %s" cmd (shell-quote-argument input))))
         (temp-buffer (generate-new-buffer (format "*output-%s-%s*" (or buffer-name "my-bin-helper") (format-time-string "%Y-%m-%d-%H-%M-%S"))))
         ;; (prev-window-config (current-window-configuration))
         )
    ;; Crear en buffer temporal la respuesta
    (with-current-buffer temp-buffer
      (insert (concat "**********\nInput:" input "\n\n"))
      (insert output)
      (toggle-lines-wrap)
      (goto-char (point-min)))
    ;; Mostrar el buffer temporal
    (display-buffer temp-buffer '(display-buffer-pop-up-window))))

;;; my-bin-tgpt
(when (executable-find "tgpt")
  (defun my-bin-tgpt ()
    "Execute tgpt -q"
    (interactive)
    (my-bin-helper-1 "tgpt -q" "tgpt")))

;;; my-bin-google-gemini
(when (executable-find "google-gemini")
  (defun my-bin-google-gemini ()
    "Insert output from Google Gemini"
    (interactive)
    (let* ((query (my-get-search-term "Ask to Google Gemini: "))
           (output (shell-command-to-string (format "google-gemini %s" query))))
      (insert output))))

;;; my-bin-suckless-sent
(when (executable-find "sent")
  (defun my-bin-suckless-sent ()
    "Send the current buffer or file to 'sent' (suckless presentation tool)."
    (interactive)
    (let ((filename (or (and (derived-mode-p 'dired-mode) (dired-get-file-for-visit))
                        (buffer-file-name))))
      (when filename
        (start-process "sent-process" nil "sent" filename)))))

;;; my-bin-ripgrep
(when (executable-find "rg")
  (defun my-bin-ripgrep ()
    "Search using ripgrep ('rg'), select one file and open at line."
    (interactive)
    (let* (x
           ;; Opciones de ripgrep
           (rg-prefix "rg --column --line-number --no-heading --color=never --smart-case --hidden")
           ;; Construcción completa del comando rg
           (rg-command (concat rg-prefix " " (shell-quote-argument "")))
           ;; Ejecuta el comando de ripgrep para obtener los resultados
           (rg-results (shell-command-to-string rg-command))
           ;; Divide los resultados por líneas para crear una lista de opciones
           (candidates (split-string rg-results "\n" t))
           ;; Usa completing-read para seleccionar uno de los resultados
           (selected (completing-read "Select match: " candidates nil t)))
      ;; Si se seleccionó un resultado, extrae la información
      (when (not (string= selected ""))
        (let* ((parts (split-string selected ":"))      ;; Divide 'selected'
               (file (nth 0 parts))                     ;; Nombre de archivo
               (line (string-to-number (nth 1 parts)))) ;; Número de línea
          (find-file file)                              ;; Abre el archivo
          (goto-line line)                              ;; Ir a la línea especificada
          (message "Opened file: %s at line %d" file line))))))

;;; my-bin-music-play
(defun my-bin-music-play ()
  "Open a music file or directory with Audacious or VLC on GNU/Linux."
  (interactive)
  (let* ((music-player (or (executable-find "audacious")
                           (executable-find "vlc")))
         (music-dir (or (and (file-directory-p "~/Música") "~/Música")
                        (and (file-directory-p "D:/Music") "D:/Music")))
         (selection (when music-dir
                      (completing-read "Select file/dir: "
                                       (directory-files-recursively music-dir ".*" t)))))
    (if (and music-player selection)
        (call-process shell-file-name nil 0 nil shell-command-switch
                      (format "%s %s" music-player (shell-quote-argument (expand-file-name selection))))
      (message "Error: No music player found or no valid music directory."))))

;;; my-bin-df-disk-free
(when (executable-find "df")
  (defun my-bin-df-disk-free ()
    "Display the free space on disk using 'df'."
    (interactive)
    (message "%s" (shell-command-to-string "df -h --total -x tmpfs"))))

;;; my-bin-image-viewer
(defun my-bin-image-viewer ()
  "Display images using 'gpicview', 'nsxiv', or 'sxiv'. Faster than 'image-dired'"
  (interactive)
  (let* ((program (or (executable-find "gpicview")
                      (executable-find "nsxiv")
                      (executable-find "sxiv"))))
    (if program
        (call-process shell-file-name nil 0 nil shell-command-switch (format "%s %s" program (if (string-match-p "nsxiv\\|sxiv" program) "-t ." ".")))
      (message "No suitable image viewer installed!"))))

;;; my-bin-goldendict
(when (executable-find "goldendict")
  (defun my-bin-goldendict ()
    "Search text in GoldenDict"
    (interactive)
    (let* ((text (my-get-search-term "Search in GoldenDict: ")))
      (call-process shell-file-name nil 0 nil shell-command-switch (format "goldendict %s" (shell-quote-argument text))))))

;;; my-bin-run-app (for 'gnu/linux')
(when (string-equal system-type "gnu/linux")
  (defun my-bin-run-app ()
    "Search for .desktop files in standard locations and execute the selected one."
    (interactive)
    (let* (
           (default-dirs '("/usr/share/applications" "/usr/local/share/applications" "~/.local/share/applications"))
           (expanded-dirs (mapcar #'expand-file-name default-dirs))
           (desktop-files (seq-reduce
                           (lambda (acc dir)
                             (append acc (when (file-exists-p dir) (directory-files-recursively dir "\\.desktop$"))))
                           expanded-dirs
                           nil))
           (selected-file (completing-read "Select .desktop file: " desktop-files nil t))
           )
      (if (and selected-file (file-exists-p selected-file))
          (call-process shell-file-name nil 0 nil
                        shell-command-switch
                        (concat "gio launch " (shell-quote-argument selected-file)))
        (message "Invalid file selected!")))))

;;; my-bin-csv-or-tsv-stats
(when (and (executable-find "awk") (executable-find "wc"))
  (defun my-bin-csv-or-tsv-stats ()
    "Show the number of rows and columns for the CSV or TSV file at point in Dired.
Uses `wc -l` to count rows and `awk` to count columns from the first line."
    (interactive)
    (let* ((file (or (and (derived-mode-p 'dired-mode) (dired-get-file-for-visit))
                         (buffer-file-name)))
           (delimiter (if (string-equal (file-name-extension file) "tsv") "\t" ","))
           (rows (string-to-number
                  (shell-command-to-string
                   (format "cat %s | wc -l" (shell-quote-argument file)))))
           (columns (string-to-number
                     (shell-command-to-string
                      (format "cat %s | head -n 1 | awk -F'%s' '{print NF}'"
                              (shell-quote-argument file) delimiter)))))
      (message "File:    %s\nRows:    %d\nColumns: %d"
               (file-name-nondirectory file) rows columns))))

;;; my-bin-dragon-drag-and-drop
(when (executable-find "dragon")
  (defun my-bin-dragon-drag-and-drop ()
  "Execute 'dragon --and-exit --all --on-top' with the current file or marked files in Dired."
  (interactive)
  (let ((files
         (if (eq major-mode 'dired-mode)
             (dired-get-marked-files)   ; Obtener archivos marcados en Dired
           (list (buffer-file-name))))) ; Archivo actual si no está en Dired
    (if (not files)
        (message "No files selected.")
      (let ((command
             (concat "dragon --and-exit --all --on-top "
                     (mapconcat #'shell-quote-argument files " "))))
        ;; Ejecutar el comando en el shell
        (call-process shell-file-name nil 0 nil shell-command-switch command)
        ;; (message "Command executed: %s" command)
        )))))

;;; my-bin-kompare
(when (executable-find "kompare")
  (defun my-bin-kompare ()
    "Send 2 files to kompare."
    (interactive)
    (let* (
            (file1 (expand-file-name (read-file-name "Select file 1: ")))
            (file2 (expand-file-name (read-file-name "Select file 2: ")))
            (command (format "kompare %s %s" (shell-quote-argument file1) (shell-quote-argument file2)))
            )
    (call-process shell-file-name nil 0 nil shell-command-switch command))))

;;; my-bin-qrencode-qr
(when (executable-find "qrencode")
  (defun my-bin-qrencode-qr ()
    "Display a text in QR code"
    (interactive)
    (call-process shell-file-name nil 0 nil shell-command-switch
                  (format "qrencode '%s' --size=24 --dpi=300 --output=/tmp/qr.png" (my-get-search-term)))
    (find-file "/tmp/qr.png")))

;;;; Windws

;;; Everything
;; Based on https://www.emacswiki.org/emacs/Everything

(provide 'my-bin)
;;; my-bin.el ends here
