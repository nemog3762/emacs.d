;;; my-nxml.el --- my nxml config -*- lexical-binding: t; -*-

(require 'nxml-mode)

;;; Functions

(defun my-nxml-where-am-i ()
  "Display the hierarchy of XML elements the point is on as a
path. from http://www.emacswiki.org/emacs/NxmlMode"
  (interactive)
  (let ((path nil))
    (save-excursion
      (save-restriction
        (widen)
        (while
            (and (< (point-min) (point)) ;; Doesn't error if point is at
                                         ;; beginning of buffer
                 (condition-case nil
                     (progn
                       (nxml-backward-up-element) ; always returns nil
                       t)
                   (error nil)))
          (setq path (cons (xmltok-start-tag-local-name) path)))
        (if (called-interactively-p t)
            (message "/%s" (mapconcat 'identity path "/"))
          (format "/%s" (mapconcat 'identity path "/")))))))

;;; Config - Autoclose new tags
;; https://borretti.me/article/better-xml-editing-for-emacs

(defun my-in-start-tag-p ()
  ;; Check that we're at the end of a start tag. From the source code of
  ;; `nxml-balanced-close-start-tag`.
  (let ((token-end (nxml-token-before))
	    (pos (1+ (point)))
	    (token-start xmltok-start))
    (or (eq xmltok-type 'partial-start-tag)
		(and (memq xmltok-type '(start-tag
					             empty-element
					             partial-empty-element))
		     (>= token-end pos)))))
(defun my-finish-element ()
  (interactive)
  (if (my-in-start-tag-p)
      ;; If we're at the end of a start tag like `<foo`, complete this to
      ;; `<foo></foo>`, then move the point between the start and end tags.
      (nxml-balanced-close-start-tag-inline)
    ;; Otherwise insert an angle bracket.
    (insert ">")))
(define-key nxml-mode-map (kbd ">") 'my-finish-element)

;;; Config - RET automatic indent between tags
;; https://borretti.me/article/better-xml-editing-for-emacs

(defun my-nxml-newline ()
  "Insert a newline, indenting the current line and the newline appropriately in nxml-mode."
  (interactive)
  ;; Are we between an open and closing tag?
  (if (and (char-before) (char-after)
           (char-equal (char-before) ?>)
           (char-equal (char-after) ?<))
      ;; If so, indent it properly.
      (let ((indentation (current-indentation)))
        (newline)
        (indent-line-to (+ indentation 4))
        (newline)
        (indent-line-to indentation)
        (previous-line)
        (end-of-line))
    ;; Otherwise just insert a regular newline.
    (newline)))
(define-key nxml-mode-map (kbd "RET") 'my-nxml-newline)

;;; Config - Setup hide-show
;; https://lgfang.github.io/mynotes/emacs/emacs-xml.html#sec-8-2

(add-hook 'nxml-mode-hook (lambda() (hs-minor-mode 1)))
(add-to-list 'hs-special-modes-alist
             '(nxml-mode
               "<!--\\|<[^/>]*[^/]>" ;; regexp for start block
               "-->\\|</[^/>]*[^/]>" ;; regexp for end block
               "<!--"
               nxml-forward-element
               nil))
(defun lgfang-toggle-level ()
  "mainly to be used in nxml mode"
  (interactive) (hs-show-block) (hs-hide-level 1))
(eval-after-load "nxml-mode"
  '(progn
     (define-key nxml-mode-map (kbd "M-'") 'lgfang-toggle-level)
     (define-key nxml-mode-map [mouse-3]   'lgfang-toggle-level)))

;;; hooks

;; Generals
(add-hook 'nxml-mode-hook (lambda()
                            ;; Tamaño de fuente
                            (buffer-face-set '(:height 100))
                            ;; Espaciado de líneas
                            (setq-local line-spacing 0.0)
                            ;; Highlight line
                            (hl-line-mode)
                            ))

;;; bind
(add-hook 'nxml-mode-hook
          (lambda ()
            (local-set-key (kbd "C-1") 'my-comment-or-uncomment)
            (local-set-key (kbd "M-n") 'nxml-forward-element)
            (local-set-key (kbd "M-p") 'nxml-backward-element)
            ))

(provide 'my-nxml)
;;; my-nxml.el ends here
