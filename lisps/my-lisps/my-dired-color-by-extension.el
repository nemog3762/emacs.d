;;; my-dired-color-by-extension.el --- my dired configs -*- lexical-binding: t; -*-

(require 'dired)

;;; Color by file extension
;; List for extension and foreground

(defvar dired-extension-colors
  '(
    ("pdf" . "red") ("PDF" . "red")
    ("odt" . "blue") ("doc"  . "blue") ("docx" . "blue")
    ("odp" . "orange") ("ppt"  . "orange") ("pptx" . "orange")
    ("png" . "purple") ("jpg"  . "purple") ("jpeg"  . "purple")
    ("csv" . "green") ("ods"  . "green") ("xls"  . "green") ("xlsx" . "green")
    ("zip" . "orange3") ("7z" . "orange3") ("tar.xz" . "orange3") ("tar.gz" . "orange3")
    ("mp4" . "maroon1")
    )
  "List of file extensions and their corresponding foreground colors.")
;; Function to create faces dynamically according 'dired-extension-colors'
(defun dired-create-faces-from-extensions ()
  (dolist (ext dired-extension-colors)
    (let* ((ext-name (car ext))
           (color (cdr ext))
           (face-name (intern (concat "dired-" ext-name "-face"))))
      (eval `(defface ,face-name
               '((t (:foreground ,color)))
               ,(concat "Face used for \"." ext-name "\" extension."))))))
;; Execute the function 'dired-create-faces-from-extensions' and create faces
(dired-create-faces-from-extensions)
;; Function to add rules from 'dired-extension-colors' to 'dired-font-lock-keywords'
(defun dired-add-extension-font-lock-keywords ()
  (dolist (ext dired-extension-colors)
    (let* ((ext-name (car ext))
           (face-name (intern (concat "dired-" ext-name "-face"))))
      (add-to-list 'dired-font-lock-keywords
                   `(eval . (list ,(concat "\\(\\." ext-name "\\)$")
                                  '(1 ',face-name)))))))
;; Excecute the function 'dired-add-extension-font-lock-keywords'
(dired-add-extension-font-lock-keywords)

(provide 'my-dired-color-by-extension)
;;; my-dired-color-by-extension.el ends here
