;;; Leader key setup

;; Define my leader key
(defvar my-leader-key ",")

;; Leader key bindings
(setq my-leader-key-bindings
      '((global
         ;; Global leader key bindings
         ("SPC"  . (lambda () (interactive) (insert ", ")))
         (","    . execute-extended-command)
         ;; Window management
         ("wt"   . my-window-split-toggle)
         ("ws"   . window-swap-states)
         ("wb"   . balance-windows)
         ("wd"   . delete-window)
         ("wo"   . delete-other-windows)
         ("wv"   . split-window-right)
         ("wh"   . split-window-below)
         ;; Buffers
         ("bk"   . kill-buffer)
         ("bs"   . switch-to-buffer)
         ("bl"   . my-switch-to-last-buffer)
         ("bo"   . my-buffer-kill-others)
         ("be"   . my-buffer-kill-buffers-emacs)
         ("l"    . my-buffer-next-user)
         ("k"    . my-switch-to-last-buffer)
         ("m"    . my-buffer-previous-user)
         ("ñ"    . switch-to-buffer)
         ;; External packages
         ("d"    . dired-jump)
         ;; Tabs
         ("tt"   . tab-bar-new-tab)
         ("tw"   . tab-bar-close-tab)
         ("td"   . tab-bar-detach-tab)
         ("ta"   . tab-bar-move-tab-to-frame)
         ("to"   . tab-bar-close-other-tabs)
         ;; Other
         ("s"    . yas-insert-snippet)
         ("j"    . delete-indentation)        ;; Join lines
         ("i"    . isearch-forward))          ;; Incremental search

        (org-mode
         ;; Org-mode specific bindings
         (".j"   . org-goto)
         (".l"   . org-insert-link)
         (".si"  . my-org-snippet-image-header)
         (".c"   . my-org-image-from-clipboard)
         )

        (emacs-lisp-mode
         ;; elisp
         (".e" . eval-last-sexp)
         (".b" . elisp-eval-region-or-buffer)
         )

        (dired-mode
         ;; dired
         (".i" . image-dired)
         )
        ))

;; Helper function to create leader keymap
(defun my-create-leader-keymap (bindings)
  "Create a leader keymap from BINDINGS."
  (let ((keymap (make-sparse-keymap)))
    (dolist (binding bindings)
      (define-key keymap (kbd (car binding)) (cdr binding)))
    keymap))

;; Setup global leader keymap
(setq my-global-leader-keymap (my-create-leader-keymap (alist-get 'global my-leader-key-bindings)))
(global-set-key (kbd my-leader-key) my-global-leader-keymap)

;; Mode-specific leader key setup function
(defun my-setup-mode-leader-keys ()
  "Set up leader key bindings for the current mode."
  (let ((mode-specific-bindings (alist-get major-mode my-leader-key-bindings)))
    (when mode-specific-bindings
      (let ((mode-leader-keymap (copy-keymap my-global-leader-keymap)))
        ;; Add mode-specific bindings on top of global bindings
        (dolist (binding mode-specific-bindings)
          (define-key mode-leader-keymap (kbd (car binding)) (cdr binding)))
        ;; Set leader key for the mode
        (define-key (current-local-map) (kbd my-leader-key) mode-leader-keymap)))))

;; Add mode-specific leader keys on mode hooks
(add-hook 'after-change-major-mode-hook #'my-setup-mode-leader-keys)
