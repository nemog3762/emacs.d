;; 'isearch' search spaces as regex
;; 1. By default, 'isearch' search a space as a literally space.
;; 2. Change to treat a spaces as '-' or '_' with regex.
(setq search-whitespace-regexp "[-_ \t\n]+")

;;; 'isearch' keys

;; 'C-s' runs 'isearch-forward'

;; 'C-S-s' runs 'isearch-backward'
(global-set-key (kbd "C-S-s") 'isearch-backward)

;; 'C-M-r' runs 'isearch-forward-regexp'
(global-set-key (kbd "C-M-r") 'isearch-forward-regexp)

;; 'C-M-S-r' runs 'isearch-backward-regexp'
(global-set-key (kbd "C-M-S-r") 'isearch-backward-regexp)

;;; 'isearch' change between results using arrow keys
;; http://xahlee.info/emacs/emacs/emacs_isearch_by_arrow_keys.html
(define-key isearch-mode-map (kbd "<up>")                'isearch-ring-retreat)
(define-key isearch-mode-map (kbd "<down>")              'isearch-ring-advance)
(define-key isearch-mode-map (kbd "<left>")              'isearch-repeat-backward)
(define-key isearch-mode-map (kbd "<right>")             'isearch-repeat-forward)
(define-key minibuffer-local-isearch-map (kbd "<left>")  'isearch-reverse-exit-minibuffer)
(define-key minibuffer-local-isearch-map (kbd "<right>") 'isearch-forward-exit-minibuffer)

;;; 'isearch' show n of N results
(defun my-isearch-update-post-hook()
  (let (suffix num-before num-after num-total)
    (setq num-before (count-matches isearch-string (point-min) (point))
          num-after (count-matches isearch-string (point) (point-max))
          num-total (+ num-before num-after)
          suffix (if (= num-total 0)
                     ""
                   (format " [%d of %d]" num-before num-total))
          isearch-message-suffix-add suffix
          )
    (isearch-message)))
(add-hook 'isearch-update-post-hook 'my-isearch-update-post-hook)

;;; 'isearch' search word under cursor
;; Source 'http://xahlee.info/emacs/emacs/modernization_isearch.html'
(defun xah-search-current-word ()
  "Call `isearch' on current word or selection.
“word” here is A to Z, a to z, and hyphen [-] and lowline [_], independent of syntax table.
URL `http://xahlee.info/emacs/emacs/modernization_isearch.html'
Created: 2010-05-29
Version: 2025-02-05"
  (interactive)
  (let (xbeg xend)
    (if (region-active-p)
        (setq xbeg (region-beginning) xend (region-end))
      (save-excursion
        (skip-chars-backward "-_A-Za-z0-9")
        (setq xbeg (point))
        (right-char)
        (skip-chars-forward "-_A-Za-z0-9")
        (setq xend (point))))
    (when (< xbeg (point)) (goto-char xbeg))
    (isearch-mode t)
    (isearch-yank-string (buffer-substring-no-properties xbeg xend))))
(defalias 'my-search-current-word 'xah-search-current-word)
(global-set-key (kbd "C-w") 'xah-search-current-word)

;;; 'isearch' Whitespace Regex
;; http://xahlee.info/emacs/emacs/emacs_isearch_space.html
(setq search-whitespace-regexp "[-_ *\t\n]+")
(defun xah-toggle-search-whitespace ()
  "Set `search-whitespace-regexp' to nil or includes hyphen lowline tab newline.
URL `http://xahlee.info/emacs/emacs/emacs_isearch_space.html'
Created: 2019-02-22
Version 2024-06-16"
  (interactive)
  (if search-whitespace-regexp
      (progn
        (setq search-whitespace-regexp nil)
        (message "search-whitespace-regexp set to nil."))
    (progn
      (setq search-whitespace-regexp "[-_ \t\n]+")
      (message "search-whitespace-regexp set to hyphen lowline whitespace"))))
(defalias 'toggle-search-whitespace-regex 'xah-toggle-search-whitespace)

;;; 'isearch' use the current selection for the initial search (if set)
(defadvice isearch-mode
    (around isearch-mode-default-string
            (forward &optional regexp op-fun recursive-edit word-p) activate)
  (if (and transient-mark-mode mark-active (not (eq (mark) (point))))
      (progn
        (isearch-update-ring (buffer-substring-no-properties (mark) (point)))
        (deactivate-mark)
        ad-do-it
        (if (not forward)
            (isearch-repeat-backward)
          (goto-char (mark))
          (isearch-repeat-forward)))
    ad-do-it))

;;; 'occur' from isearch
(defun my-occur-from-isearch ()
  (interactive)
  (let ((query (if isearch-regexp
                   isearch-string
                 (regexp-quote isearch-string))))
    (isearch-update-ring isearch-string isearch-regexp)
    (let (search-nonincremental-instead)
      (ignore-errors (isearch-done t t)))
    (occur query)))

;;; 'occur' config to be 20% of frame height
(progn
  (defun my-occur-window-setup ()
    "Open *Occur* window to occupy 25% of the frame height."
    (let ((occur-window (get-buffer-window "*Occur*" 'visible)))
      (when occur-window
        (with-selected-window occur-window
          (window-resize occur-window
                         (- (truncate (* 0.25 (frame-height)))
                            (window-height)))))))
  (defun my-occur ()
    "Run `occur` and adjust the window size to 20% of the frame height."
    (interactive)
    (call-interactively #'occur)
    (my-occur-window-setup))
  (add-hook 'occur-hook 'my-occur-window-setup))

;;; 'occur' open at bottom
;; Config 'display-buffer-alist' to show 'occur' in the bottom
;; https://protesilaos.com/codelog/2024-02-08-emacs-window-rules-display-buffer-alist/
(defun my-select-window (window &rest _)
  "Select WINDOW for display-buffer-alist"
  (select-window window))
(setq display-buffer-alist
      '(((or . ((derived-mode . occur-mode)))
         (display-buffer-reuse-mode-window display-buffer-at-bottom)
         (body-function . my-select-window)
         (dedicated . t)
         (preserve-size . (t . t)))))

;;; Keyboard
;; Open occur from isearch
(define-key isearch-mode-map (kbd "<return>") 'my-occur-from-isearch)
;; Keys in occur
(define-key occur-mode-map (kbd "<up>")       'previous-error-no-select)
(define-key occur-mode-map (kbd "<down>")     'next-error-no-select)
(define-key occur-mode-map (kbd "k")          'previous-error-no-select)
(define-key occur-mode-map (kbd "j")          'next-error-no-select)
(define-key occur-mode-map (kbd "<return>")   'kill-this-buffer)
