(require 'url-http)

(setq shr-use-fonts  nil)
(setq shr-use-colors nil)
(setq shr-indentation 2)
(setq shr-width 80)
(setq eww-search-prefix "https://duckduckgo.com/html/?q=")
(setq eww-auto-rename-buffer 'title)
(setq url-user-agent "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3739.0 Safari/537.36\n")
(setq browse-url-browser-function (cond
                                   ((eq system-type 'windows-nt) 'browse-url-default-windows-browser)
                                   ((eq system-type 'darwin)     'browse-url-default-macosx-browser)
                                   (t 'browse-url-generic))      ;; Linux u otros
      browse-url-generic-program "firefox")

;; use diff browsers depending on url
;; (setq browse-url-handlers '(
;;                             ("google.com"      . browse-url-default-browser)
;;                             ("youtube.com"     . browse-url-default-browser)
;;                             ("localhost"       . browse-url-default-browser)
;;                             ("lectulandia.com" . browse-url-default-browser)
;;                             ("odysee.com"      . browse-url-default-browser)
;;                             (".pdf"             . browse-url-default-browser)
;;                             ("."               . eww-browse-url)
;;                             ))

(add-hook 'eww-mode-hook
          (lambda ()
            ;;Keys
            (local-set-key (kbd "M-<left>")  #'eww-back-url)
            (local-set-key (kbd "M-<right>") #'eww-next-url)
            (local-set-key (kbd "q")         nil)
            (local-set-key (kbd "e")         #'eww-readable)
            (local-set-key (kbd "j")         #'next-line)
            (local-set-key (kbd "k")         #'previous-line)
            (local-set-key (kbd "<f6>")      #'eww-browse-with-external-browser)
            ))

;;; Functions

(defun my-eww-weather-clima ()
  "Show weather report in eww."
  (interactive)
  (eww (format "https://wttr.in/%s?m?2?F?q?T"
               (completing-read "Choose or write a city: "
                                '("BAQ" "BOG" "CDMX") nil nil nil nil "CDMX"))))
