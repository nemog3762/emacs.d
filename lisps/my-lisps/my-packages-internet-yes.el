;;; my-packages-internet-yes.el --- my functions for packages -*- lexical-binding: t; -*-

;;;; Basic

;;; 'vertico'
(use-package vertico
  :disabled
  :ensure t
  :defer nil
  :custom
  (vertico-count 10) ;; Show N candidates
  (vertico-resize t) ;; Grow and shrink the Vertico minibuffer
  (vertico-cycle t)  ;; Enable cycling for `vertico-next/previous'
  :init
  (icomplete-mode 0)
  (icomplete-vertical-mode 0)
  :config
  (vertico-mode)
  (vertico-mouse-mode)
  (vertico-reverse-mode))

;;; 'marginalia'
(use-package marginalia
  :disabled
  :ensure t
  :hook (after-init . marginalia-mode))

;;;; elfeed

;;; 'elfeed'
(use-package elfeed
  :ensure t
  :defer t
  :commands (elfeed)
  :init
  (load "my-elfeed-list-rss" t)
  (load "my-elfeed" t))

;;; elfeed-tube
(use-package elfeed-tube
  :ensure t
  :after (elfeed)
  :demand t
  :config
  (elfeed-tube-setup))

;;;; Git

;;; 'magit' git interface
(use-package magit
  :ensure t
  :disabled
  :defer t
  :commands (magit magit-status magit-log)
  :init
  (defalias 'gs 'magit-status)
  (defalias 'gl 'magit-log)
  :config
  (setq magit-log-arguments '("-n256" "--graph" "--decorate" "--color"))
  ;; Show diffs per word, looks nicer!
  (setq magit-diff-refine-hunk t))

;;; 'diff-hl' highlight git changes
(use-package diff-hl
  :ensure t
  :defer t
  :custom-face
  (diff-hl-change ((t (:background "yellow1" :foreground "black"))))
  (diff-hl-delete ((t (:background "red"                        ))))
  (diff-hl-insert ((t (:background "green"                      ))))
  :hook
  (org-mode   . diff-hl-mode)
  (dired-mode . diff-hl-dired-mode)
  (prog-mode  . diff-hl-mode))

;;;; Org

;;; ox-reveal
(use-package ox-reveal
  :disabled
  :ensure t
  :config
  (setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js@4.1.0/")
  (setq org-reveal-mathjax t))

;;;; Games or fun

;;; 'nyan-mode'
(use-package nyan-mode
  :ensure t
  :commands (nyan-mode))

;;; 'reddigg' reddit reader
(use-package reddigg
  :ensure t
  :defer t
  :config
  (defalias 'reddit 'reddigg-view-sub)
  (setq reddigg-subs '(colombia emacs googleplaydeals opendirectories
                                everymanshouldknow unixporn)))

;;;; lsp

;;; 'lsp-mode'
(use-package lsp-mode
  :disabled
  :ensure t
  :if (eq system-type 'gnu/linux)
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")  ;; Or 'C-l', 's-l'
  :config
  (require 'lsp-pyls)
  (lsp-enable-which-key-integration t)
  ;; A guide on disabling/enabling lsp-mode features
  ;; https://emacs-lsp.github.io/lsp-mode/tutorials/how-to-turn-off/
  (setq lsp-enable-symbol-highlighting nil)
  ;; Disable all warnings
  ;; https://emacs.stackexchange.com/a/54322
  (setq lsp-pyls-plugins-pycodestyle-enabled nil)
  :hook
  (python-mode . lsp)
  )

;;; 'lsp-treemacs'
(use-package lsp-treemacs
  :disabled
  :if (eq system-type 'gnu/linux)
  :ensure t
  )

;;; 'lsp-ui'
(use-package lsp-ui
  :disabled
  :ensure t
  :config
  (setq lsp-ui-flycheck-enable nil)
  (add-to-list 'lsp-ui-doc-frame-parameters '(no-accept-focus . t))
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references))

;;;; Productivity / user experience

;;; 'neotree' alternative to 'sr-speedbar'
(use-package neotree
  :ensure t
  :defer t
  :commands (neotree-toggle)
  :custom
  (setq neo-theme 'arrow)
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow)))

;;; 'swiper' (better 'isearch')
(use-package swiper
  :disabled
  :defer t
  :ensure t
  :config
  (global-set-key (kbd "C-s")   'swiper)
  (global-set-key (kbd "C-S-s") 'swiper-backward)
  ;; <backspace> don't quit swipper
  (setq ivy-on-del-error-function #'ignore))

;;; 'evil'
(use-package evil
  :disabled
  :ensure t
  :commands (evil-mode evil-local-mode)
  :init
  (setq
   evil-normal-state-tag   (propertize " [N] " 'face '((:background "green"         :foreground "black")))
   evil-emacs-state-tag    (propertize " [E] " 'face '((:background "MediumPurple1" :foreground "black")))
   evil-insert-state-tag   (propertize " [I] " 'face '((:background "red"           :foreground "white")))
   evil-motion-state-tag   (propertize " [M] " 'face '((:background "blue"          :foreground "white")))
   evil-visual-state-tag   (propertize " [V] " 'face '((:background "magenta"       :foreground "black")))
   evil-operator-state-tag (propertize " [O] " 'face '((:background "purple"        :foreground "white")))
   )
  :config
  (define-key evil-normal-state-map (kbd "j") #'evil-next-visual-line)
  (define-key evil-normal-state-map (kbd "k") #'evil-previous-visual-line)
  ;; :bind (("<f12>" . evil-local-mode))
  )

;;; 'undo-tree'
(use-package undo-tree
  :ensure t
  :custom
  ;; Prevent undo tree files from polluting everywhere
  (undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo")))
  (undo-tree-visualizer-timestamps t)
  (undo-tree-visualizer-diff t)
  (undo-tree-auto-save-history nil)
  :init
  (global-undo-tree-mode))

;;; 'corfu' for completing
(use-package corfu
  :disabled
  :ensure t
  :defer t
  :custom
  (corfu-cycle t)                ;; Enable cycling for 'corfu-next/previous'
  (corfu-auto t)                 ;; Enable automatic auto completion
  (corfu-auto-prefix 2)
  (corfu-auto-delay 0.3)
  (corfu-popupinfo-delay '(0.5 . 0.2))
  :config
  ;; (corfu-echo-mode)      ;; displays a brief cadidate
  (corfu-history-mode)   ;; remembers selected candidates and sorts the candidates by their history position.
  (corfu-popupinfo-mode)
  :hook
  (prog-mode . corfu-mode)
  (org-mode  . corfu-mode))

;;;; GUI

;;; 'minions' hide minor modes in the mode line
(use-package minions
  :disabled
  :ensure t
  :custom
  (setq minions-prominent-modes '(flyspell-mode))
  :config
  (minions-mode)
  (display-battery-mode))

;;; 'dashboard'
(use-package dashboard
  :disabled
  :ensure t
  :config
  (defun my-toggle-dashboard ()
    "Toggle between '*dashboard*' and the last buffer."
    (interactive)
    (let ((dashboard-buffer (get-buffer "*dashboard*"))
          (prev-buffer (other-buffer)))
      (cond
       ;; Si estamos en el buffer *dashboard*, volvemos al buffer anterior
       ((eq (current-buffer) dashboard-buffer) (switch-to-buffer prev-buffer))
       ;; Si no estamos en el buffer *dashboard*, abrimos el dashboard
       (t (dashboard-open)))))
  ;; Set ASCII message
  (setq dashboard-banner-ascii "            _______
           /       /_
          /  -/-  / /
         /   /   / /
        /_______/ /
       ((______| /
        `-------`")
  (setq dashboard-banner-ascii "ECASI")
  ;; Set the banner
  (setq dashboard-startup-banner 'ascii) ;; 'official, 'logo, 'ascii
  ;; Content is not centered by default. To center, set
  (setq dashboard-center-content t)
  ;; By default org-agenda entries are filter by time, only showing those task
  ;; with DEADLINE, SCHEDULE-TIME or TIMESTAMP .
  ;; To show all agenda entries (except DONE)
  (setq dashboard-filter-agenda-entry 'dashboard-no-filter-agenda)
  ;; items
  (setq dashboard-items '(
                          (recents   . 5)
                          (bookmarks . 5)
                          ;; (projects  . 5)
                          (agenda    . 5)
                          ;; (registers . 5)
                          ))
  ;; widgets list
  (setq dashboard-startupify-list '(
                                    dashboard-insert-banner
                                    ;;dashboard-insert-newline
                                    ;; dashboard-insert-banner-title
                                    ;;dashboard-insert-newline
                                    ;;dashboard-insert-navigator
                                    ;;dashboard-insert-newline
                                    dashboard-insert-init-info
                                    dashboard-insert-items
                                    ;;dashboard-insert-newline
                                    ;;dashboard-insert-footer
                                    ))

  ;; Keyboard
  (global-set-key (kbd "<f1><f2>") 'my-toggle-dashboard)
  ;; Hook
  (dashboard-setup-startup-hook))

;;;; For programation

;;; 'impatient-mode' for html preview
(use-package impatient-mode
  :disabled
  :ensure t
  :config
  (defun my-html-preview ()
    "Start httpd server, enable impatient-mode, and open preview in browser."
    (interactive)
    ;; Iniciar el servidor HTTP si no está activo
    (unless (httpd-running-p)
      (httpd-start))
    ;; Activar impatient-mode en el buffer actual
    (impatient-mode 1)
    ;; Abrir la URL de preview en el navegador
    (browse-url "http://localhost:8080/imp/")))

;;; python 'numpydoc'
(use-package numpydoc
  :disabled
  :ensure t
  :defer t
  :commands (numpydoc-generate)
  :custom
  (numpydoc-insertion-style 'prompt)
  (numpydoc-insert-return-without-typehint nil))

;;;; Dired

;;; 'treemacs-icons-dired'
(use-package treemacs-icons-dired
  :ensure t
  :if (display-graphic-p)
  :after (dired)
  :config
  (treemacs-icons-dired-mode))

;;;; Others

;;; 'nov' read .epub
(use-package nov
  :if (eq system-type 'gnu/linux)
  :ensure t
  :defer t
  :mode ("\\.epub\\'" . nov-mode)
  :bind (:map nov-mode-map
              ("DEL" . nov-history-back)
              ("TAB" . shr-next-link)
              ("{"   . (lambda () (interactive)
                         (backward-paragraph)
                         (recenter-top-bottom 1)))
              ("}"   . (lambda () (interactive)
                         (forward-paragraph)
                         (recenter-top-bottom 1)))
              ("h"   . nil)
              ("j"   . next-line)
              ("k"   . previous-line)
              ("l"   . nil)
              ("q"   . nil)
              ("Q"   . kill-this-buffer)
              )
  )

;;; 'yeetube'
(use-package yeetube
  :ensure t
  :defer t
  :config
  ;; Don't open invidious, only YouTube
  (setq yeetube-invidious-instances '("youtube.com"))
  :hook
  (yeetube-mode . hl-line-mode)
  :bind (:map yeetube-mode-map
              ("j" . next-line)
              ("k" . previous-line)))

;;; 'writeroom-mode'
(use-package writeroom-mode
  :ensure t
  :config
  (define-key writeroom-mode-map (kbd "C-M-<") #'writeroom-decrease-width)
  (define-key writeroom-mode-map (kbd "C-M->") #'writeroom-increase-width)
  (define-key writeroom-mode-map (kbd "C-M-=") #'writeroom-adjust-width))

;;;; Unused

;;; 'eat'
(use-package eat
  :ensure t)

;;; 'multiple-cursors'
;; Never like it or understand.
(use-package multiple-cursors
  :defer t
  :ensure t
  :bind
  ("C->" . 'mc/mark-next-lines)
  ("C-<" . 'mc/mark-previous-lines)
  ("C-r" . 'mc/mark-next-word-like-this))

(provide 'my-packages-internet-yes)
;;; my-packages-internet-yes.el ends here
