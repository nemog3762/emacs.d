;;; my-choose.el --- my calendar config for Emacs -*- lexical-binding: t; -*-

;;;; config

(setq read-file-name-completion-ignore-case t)
(setq read-buffer-completion-ignore-case t)

;;;; vars

(defvar my-choose-file-radio     "/home/user/.config/txt/radios.txt")
(defvar my-choose-file-bookmarks "/home/user/.config/txt/bookmarks.txt")

;;;; non-interactive functions

;;; my-choose-select-line
(defun my-choose-select-line (file-path)
  "Prompt to select a line from the file and return it."
  (interactive "fSelect file: ")
  (completing-read "Select a line: "
                  (split-string (with-temp-buffer
                                  (insert-file-contents file-path)
                                  (buffer-string)) "\n" t)))

;;; my-choose-get-url
(defun my-choose-get-url (selected-line)
  "From a line, return the URL."
  (when (string-match "\\(https?://[^ \n]+\\|www\\.[^ \n]+\\)" selected-line)
    (match-string 0 selected-line)))

;;; my-choose-a-url
(defun my-choose-a-url (file)
  "Choose a line, then a url in the line"
  (if (file-exists-p file)
      (let* (
             ;; Select a line with other function
             ;; Get 'url' from the 'selected-line'
             (url (my-choose-get-url (my-choose-select-line file))))
        ;; Open URL in the default web browser if is not nil
        (if url
            (browse-url url)
          (message "No URL found in the selected line.")))
    (message "The file '%s' does not exist." file)))

;;; my-choose-file-in-home
(defun my-choose-file-in-home ()
  "Select a file in HOME"
  (let* (
         ;; Define command
         (find-command
          (concat "find $HOME \\( "
                  "-type d -name '.cache' -o "
                  "-type d -name '.config' -o "
                  "-type d -name '.FBReader' -o "
                  "-type d -name '.mozilla' -o "
                  "-type d -name '.npm' -o "
                  "-type d -name '.Telegram' -o "
                  "-type d -name '.var' -o "
                  "-type d -name '.zotero' -o "
                  "-type d -name '.Zotero_linux-x86_64' -o "
                  "-type d -name '.zsnes' -o "
                  "-type d -name 'anaconda3' -o "
                  "-type d -name 'miniconda3' -o "
                  "-type d -name 'node_modules' -o "
                  "-type d -name '.local' "  ;; The last doesn't include '-o'
                  "\\) -prune -o -type f -o -type d"))
         ;; Execute command
         (results (split-string (shell-command-to-string find-command) "\n" t))
         ;; Solicitar al usuario que seleccione un archivo o directorio
         (selection (completing-read "Select file/dir: " results nil t))
         )
    selection))

;;;; Interactive functions

;;; my-choose-radio
(defun my-choose-radio ()
  "Choose a radio"
  (interactive)
  (my-choose-a-url my-choose-file-radio))

;;; my-choose-bookmark
(defun my-choose-bookmark ()
  "Choose a radio"
  (interactive)
  (my-choose-a-url my-choose-file-bookmarks))

;;; my-choose-file-open
(when (and (executable-find "find") (executable-find "xdg-open"))
  (defun my-choose-file-open ()
    "Find and open a file or directory from $HOME using the 'find' command, excluding certain directories."
    (interactive)
    (let* (
           ;; Solicitar al usuario que seleccione un archivo o directorio
           (selection (my-choose-file-in-home))
           )
      ;; Si hay una selección válida, abrirla con xdg-open
      (when (and selection (not (string-empty-p selection)))
        (call-process shell-file-name nil 0 nil
                      shell-command-switch
                      (format "%s %s"
                              "xdg-open"
                              (shell-quote-argument selection)))))))

;;; my-choose-file-dragon-drag-and-drop
(when (and (executable-find "find") (executable-find "dragon"))
  (defun my-choose-file-dragon-drag-and-drop ()
    "Find and drag-and-drop a file or directory from $HOME using the 'find' command, excluding certain directories."
    (interactive)
    (let* (
           ;; Solicitar al usuario que seleccione un archivo o directorio
           (selection (my-choose-file-in-home))
           )
      ;; Si hay una selección válida, abrirla con xdg-open
      (when (and selection (not (string-empty-p selection)))
        (call-process shell-file-name nil 0 nil
                      shell-command-switch
                      (format "%s %s"
                              "dragon --all --on-top --and-exit"
                              (shell-quote-argument selection)))))))

(provide 'my-choose)
;;; my-choose.el ends here
