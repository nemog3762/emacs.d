;;;; General configs

;; Activate server
;; Autostart 'emacsclient' in S.O.
;; Useful for: https://github.com/tecosaur/emacs-everywhere
(server-start)

;; Prevent Emacs from closing
(setq confirm-kill-emacs 'y-or-n-p)

;; Type `y' and `n' instead of `yes' or `no'
(setq use-short-answers t)

;; Cambiar orden 'C-l' o 'recenter-top-bottom'
;; Por defecto es middle top bottom
(setq recenter-positions '(top middle bottom))

;; Comportamiento de scrolling del mouse
;; Scroll one line at a time (less "jumpy" than defaults)
(when (display-graphic-p)
  (setq mouse-wheel-scroll-amount '(1 ((shift) . hscroll))
        mouse-wheel-scroll-amount-horizontal 1
        mouse-wheel-progressive-speed nil))
(setq scroll-step 1
      scroll-margin 0
      scroll-conservatively 100000
      auto-window-vscroll nil
      scroll-preserve-screen-position t)

;; display or not the help screen on startup
(setq inhibit-startup-screen t)

;; Establecer el mensaje inicial del scratch buffer a una cadena vacía.
;; By default:
;; This buffer is for text that is not saved, and for Lisp evaluation.
;; To create a file, visit it with C-x C-f and enter text in its buffer.
(setq initial-scratch-message "")

;; Quitar el mensaje "You can run the command 'xxx' with ..." en el minibuffer
(setq suggest-key-bindings nil)

;; Deshabilitar toda la creación de GUI al hacer preguntas
(setq use-dialog-box nil)

;; Enable elisp debugging in your .emacs file when a error occur
(setq debug-on-error t)

;; Update files if the file is edited externally
(global-auto-revert-mode t)

;; Delete trailing whitespaces
(add-hook 'write-file-hooks 'delete-trailing-whitespace)

;; Movimiento con Ctrl+Flechas
;; 'global-subword-mode' para moverse en 'camelCase'.
;; 'global-superword-mode' para tratar 'x_y' como una palabra.
(global-subword-mode 1)

;; world clock
(setq world-clock-list '(
                         ("Etc/UTC"              "UTC")
                         ("America/Bogota"       "Bogotá")
                         ("America/Mexico_City"  "CDMX")
                         ("America/New_York"     "New York")
                         ("Europe/Berlin"        "Berlin")
                         )
      world-clock-time-format "%a, %d %b %H:%M %Z")
(defalias 'hora-mundial 'world-clock)

;; Browser
(setq browse-url-generic-program
      (cond
       ((eq system-type 'darwin) "open")
       ((eq system-type 'gnu/linux) (executable-find "firefox"))
       ))

;; With 'sort-lines' ignore case
(setq sort-fold-case t)

;;; Warnings

;; Disable warnings of libraries
(setq warning-minimum-level :emergency)

;; Disable warnings of same commands
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'erase-buffer 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)

;;; wrap words and long lines

;; Don't divide words at word boundary
(setq-default word-wrap t)

;; Show a long line in 2 or more lines?
;; 'nil' = no
;; 't' = yes
;; Not recommended. If you do this, dired filenames or url may show at next line
(setq-default truncate-lines t)

;; Turn on/off line wrap for current buffer.
;; To disable in a mode, use '(visual-line-mode -1)'
(global-visual-line-mode 1)

;;; Custom file location

;; Location of 'custom.el'
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

;; If 'custom-.el' exist, load it, if not, don't show errors
(load custom-file 'noerror 'nomessage)

;;; Backups

;; Emacs tiene un 'auto-save-mode', pero el sistema no hace respaldos de forma moderna.
;; 'auto-save-mode' periodicamente guarda una copia del archivo con un nombre similar a '#filename#'
;; Cuando se guarda un archivo, se borra el archivo '#temp-file#'
;; Si se cierra el programa o se apaga el computador, Emacs detecta el archivo temporal y preguntará si se quiere restaurar.
;; El problema es que son archivos de enlaces simbólicos a archivos no existentes y interferir con programas como Insync o rsync.

;; Deshabilitar backups nativos de Emacs.

;; Deshabilitar la creación de archivos de respaldo #auto-save#
(setq auto-save-default nil)

;; Aunque (setq auto-save-default nil), aún se puede pueden generar archivos temporales.
(setq create-lockfiles    nil)

;; Deshabilitar archivos de backup con el mismo nombre de archivo y finaliza con =~=.
(setq make-backup-files   nil)

;; Borrar respaldos antiguos de forma silenciosa
(setq delete-old-versions t)

;; Evitar que backup de Emacs cambie la fecha de creación del archivo original.
;; Cuando Emacs hace un backup, renombra el archivo original como el archivo de respaldo y genera un archivo nuevo. Esto hace que se cambie la fecha de creación del archivo.
(setq backup-by-copying t)

;; Cambiar lugar donde se guardan backups en caso de estar habilitados.
(setq backup-directory-alist `(("." . "~/.emacs.d/saves")))

;;; Show line number

(require 'display-line-numbers)

;; Configuración de la apariencia del número de línea actual
(set-face-attribute 'line-number-current-line nil
                    :foreground "red"
                    :weight 'bold)

;; Activar números de línea en modos específicos
(add-hook 'prog-mode-hook         'display-line-numbers-mode)
(add-hook 'LaTeX-mode-hook        'display-line-numbers-mode)
(add-hook 'org-mode-hook          'display-line-numbers-mode)
(add-hook 'nxml-mode-hook         'display-line-numbers-mode)
(add-hook 'markdown-mode-hook     'display-line-numbers-mode)

;; Alias para alternar la visibilidad de los números de línea
(defalias 'toggle-line-numbers 'display-line-numbers-mode)

;;; brackets

;; highlight brackets
(show-paren-mode t)

;; Insertar automáticamente el cierre de delimitadores () {} [].
(electric-pair-mode t)

;; highlight style.
;; options: 'parenthesis', 'expression' y 'mixed'
(setq show-paren-style 'parenthesis)

;; Incluir nuevos delimitadores a los que vienen por defecto.
(setq electric-pair-pairs
      '(
        (?\" . ?\")
        (?\¿ . ?\?)
        (?\¡ . ?\!)
        ))

;;; Asociar extensiones de archivos con modos específicos
(add-to-list 'auto-mode-alist '("\\.csv\\'" . fundamental-mode))
(add-to-list 'auto-mode-alist '("\\.m\\'"   . octave-mode))
(add-to-list 'auto-mode-alist '("\\.txt\\'" . org-mode))

;;;; Text

;;; Text Editing

;; make typing delete/overwrites selected text
(delete-selection-mode t)

;;; Tabulation

;; Set default tab char's display width to 4 spaces. Default is 8
(setq-default tab-width 4)

;; make indent commands use space only (never tab character)
;; default is t
;; t means it may use tab, resulting mixed space and tab
(setq-default indent-tabs-mode nil)

;;;; History

;;; History for recent files

;; Activate
(recentf-mode t)

;; Aumentar el registro de archivos recientes de Emacs.
(setq recentf-max-saved-items 100)

;; Excluir archivos determinados del historial.
;; Luego de modificar esta lista y si ya se tiene un historial
;; se debe hacer limpieza con 'recentf-cleanup'
(add-to-list 'recentf-exclude ".jpeg$")
(add-to-list 'recentf-exclude ".jpg$")
(add-to-list 'recentf-exclude ".png$")
(add-to-list 'recentf-exclude ".mp4$")
(add-to-list 'recentf-exclude ".pdf$")
(add-to-list 'recentf-exclude ".pdmp$")
(add-to-list 'recentf-exclude ".elfeed/index$")
(add-to-list 'recentf-exclude "emms/history$")
(add-to-list 'recentf-exclude "bookmarks$")
(add-to-list 'recentf-exclude ".custom.el$")
(add-to-list 'recentf-exclude "ellama-sessions$")

;; Acceder al historial de archivos recientes (sin auto completado)xs
(global-set-key (kbd "C-x C-t") 'recentf-open-files)

;; Acceder al historial de archivos recientes (con auto completado)
(global-set-key (kbd "C-x C-r") 'recentf-open)

;;; History in files

;; Al abrir un archivo, continuar en la mísma línea al cerrar el archivo.
(save-place-mode 1)

;;; History in minibuffer

;; Activar historial de comandos en mini-buffers.
;; Con 'Down/M-n' y 'Up/M-p' se navega en el historial.
(setq history-length 100)
(savehist-mode 1)

;;;; GUI

;;; Frame

;; Maximize Emacs frame on start up
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; macOS - make that title frame looks like a macOS native window.
(when (equal system-type 'darwin)
  (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t)))

;; Fringes are the narrow areas on the left and right sides of the Emacs window.
;; By disabling them, you can utilize the full width of the window for your text.
;; make the left fringe 4 pixels wide and the right disappear
(fringe-mode '(8 . 0))

;;; Windows

;; Balance window splits automatically
(setf window-combination-resize t)

;; Make it so keyboard-escape-quit doesn't delete-other-windows
(require 'cl-lib)
(defadvice keyboard-escape-quit
    (around keyboard-escape-quit-dont-delete-other-windows activate)
  (cl-letf (((symbol-function 'delete-other-windows) (lambda () nil)))
    ad-do-it))

;;; Buffers

;; Commands and functions for buffers:
;; 'list-buffers' or 'C-x_C-b'
;; 'kill-buffer' or 'C-x_k'
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Group dired buffers
;; https://emacs.stackexchange.com/questions/202/close-all-dired-buffers
(setq-default ibuffer-saved-filter-groups
              `(
                ("Default"
                 ("Dired"     (mode . dired-mode))
                 ("Image"     (mode . image-mode))
                 ("Org"       (mode . org-mode))
                 ("eww"       (mode . eww-mode))
                 ("Temporary" (name . "\*.*\*"))
                 )))
(add-hook 'ibuffer-mode-hook
          #'(lambda ()
              (ibuffer-switch-to-saved-filter-groups "Default")))

;; 'q' kill the buffer
(add-hook 'ibuffer-mode-hook
          (lambda ()
            (local-set-key (kbd "q")  'kill-this-buffer)))

;;; Minibuffer

;; Avoid cursor enter in minibuffer message
(setq minibuffer-prompt-properties
      '(read-only t cursor-intangible t face minibuffer-prompt))

(add-hook 'minibuffer-setup-hook
          (lambda ()
            (local-set-key (kbd "<f11>")  #'previous-history-element)
            (local-set-key (kbd "<f12>")  #'next-history-element)
            (local-set-key (kbd "<prior>")  #'previous-history-element)
            (local-set-key (kbd "<next>")  #'next-history-element)
            ))

;;;; Modes

;;; help-mode

(with-eval-after-load 'help-mode
  (define-key help-mode-map (kbd "q") #'kill-this-buffer)
  (define-key help-mode-map (kbd "j") #'next-line)
  (define-key help-mode-map (kbd "k") #'previous-line))

;;; doc-view-mode

;; Configuración de doc-view
(setq doc-view-resolution 400)
(setq doc-view-mupdf-use-svg t)
(setq doc-view-continuous t)

;; Función para establecer teclas locales en doc-view-mode
(defun my-doc-view-mode-setup ()
  "Custom keybindings for `doc-view-mode`."
  (local-set-key (kbd "k") #'doc-view-previous-line-or-previous-page)
  (local-set-key (kbd "j") #'doc-view-next-line-or-next-page))

;; Agregar la función al hook de doc-view-mode
(add-hook 'doc-view-mode-hook #'my-doc-view-mode-setup)

;;; image-dired

;; Configuración para image-dired
(setq image-dired-thumb-size 320)
(setq image-dired-thumb-margin 10)
(setq image-dired-thumb-relief 0)

;; Configurar teclas personalizadas para image-dired-thumbnail-mode
(defun my-image-dired-thumbnail-setup ()
  "Custom keybindings for `image-dired-thumbnail-mode`."
  (define-key image-dired-thumbnail-mode-map (kbd "q") #'kill-this-buffer)
  (define-key image-dired-thumbnail-mode-map (kbd "k") #'image-dired-previous-line)
  (define-key image-dired-thumbnail-mode-map (kbd "j") #'image-dired-next-line)
  (define-key image-dired-thumbnail-mode-map (kbd "l") #'image-dired-forward-image)
  (define-key image-dired-thumbnail-mode-map (kbd "h") #'image-dired-backward-image))

;; Agregar la configuración al hook de image-dired-thumbnail-mode
(add-hook 'image-dired-thumbnail-mode-hook #'my-image-dired-thumbnail-setup)
