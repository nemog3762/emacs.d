@echo off

:: Directorio donde se ubica este script
:: En esa ruta debe estar la carpeta llamada '.emacs.d'
set HOME=%cd%

:: Ruta del programa de Emacs
:: Puede ser instalado o portable
:: Descaga de Emacs para Windows en http://ftp.gnu.org/gnu/emacs/windows/
:: set FILE="C:\Program Files\Emacs\emacs-28.2\bin\runemacs.exe"
set FILE=emacs-29.4\bin\runemacs.exe

:: Ejecutar Emacs basado en las variables 'FILE' and 'HOME'
:: 'FILE': ejecutable de Emacs
:: 'HOME': riuta donde está .emacs.d
%FILE%

:: Agregar pausa
:: pause
