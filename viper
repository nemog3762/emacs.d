;; Faces
(eval-after-load 'viper
  '(progn
     ;; Definir los estados de viper con colores
     (setq viper-emacs-state-id   (concat (propertize "<E>" 'face '(:background "magenta" :weight bold))  " "))
     (setq viper-vi-state-id      (concat (propertize "<V>" 'face '(:background "yellow" :weight bold)) " "))
     (setq viper-insert-state-id  (concat (propertize "<I>" 'face '(:background "blue" :weight bold)) " "))
     (setq viper-replace-state-id (concat (propertize "<R>" 'face '(:background "red" :weight bold)) " "))
     ;; Asegurarse de que la variable de modo sea "segura" para usar propiedades
     (put 'viper-mode-string 'risky-local-variable t)
     ))

;; Visible bell?
(setq visible-bell nil)

;; Al inicio de una línea, presionar BACKSPACE para borrar y pasar a la línea anterior.
(setq viper-ex-style-editing nil)

;; No bell with multiple ESC
(setq viper-no-multiple-ESC nil)

;; Config startup
(setq viper-inhibit-startup-message 't)
(setq viper-expert-level '1)

;; Run M-x
(define-key viper-vi-global-user-map (kbd ",,") 'execute-extended-command)

;; Mimic to vim in visual mode
(define-key viper-vi-global-user-map (kbd "gg") 'beginning-of-buffer)
(define-key viper-vi-global-user-map (kbd "j")  'next-line)
(define-key viper-vi-global-user-map (kbd "k")  'previous-line)
(define-key viper-vi-global-user-map (kbd "zz") 'viper-line-to-middle)
(define-key viper-vi-global-user-map (kbd "zt") 'viper-line-to-top)
(define-key viper-vi-global-user-map (kbd "zb") 'viper-line-to-bottom)
;; (define-key viper-vi-global-user-map (kbd "v")  'set-mark-command)
