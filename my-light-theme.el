;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Begin

(deftheme my-light "Just a white background and bold var names.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Vars

;; General
(defvar my-foreground-color         "black"   "Foreground color for the theme.")
(defvar my-background-color         "white"   "Background color for the theme.")
(defvar my-constant-face-color      "black"   "Color for constants.")
(defvar my-variable-name-face-color "red"     "Color for variable names.")

;; Borde tanto de las pestañes como de la 'mode-line'
(defvar box-config                  '(:line-width 1 :color "white"))

;; Cursor
(defvar my-cursor-color             "magenta"     "Cursor color")
(defvar my-cursor-select-text       "light green" "Cursor color")

;; Colores para la barra de tabs, no las configuraciones individuales
(defvar fg-color-active-tab-bar      my-background-color)
;;(defvar bg-color-active-tab-bar    "black")
(defvar bg-color-active-tab-bar      my-background-color)

;; Colores de tab activa)
(defvar fg-color-tab-bar-tab         "black")
(defvar bg-color-tab-bar-tab         "MediumPurple1")

;; Colores de tab inactiva
(defvar fg-color-inactive-tab-bar    "black")
(defvar bg-color-inactive-tab-bar    my-background-color)

;; Colores de mode-line
(defvar fg-color-active-mode-line    "black")
(defvar bg-color-active-mode-line    "MediumPurple1")
(defvar fg-color-inactive-mode-line  "black")
(defvar bg-color-inactive-mode-line  "gray")
(defvar tab-bar-inactive-fg-color    "red")

;;; Toggle

(defun my-toggle-theme-background-and-foreground-color ()
  "Toggle background and foreground
Based on: 'http://xahlee.info/emacs/emacs/emacs_toggle_background_color.html'"
  (interactive)
  (if (get real-this-command 'xstatus)
      (progn
        (set-background-color "white")
        (set-foreground-color "black")
        (put real-this-command 'xstatus nil))
    (progn
      (set-background-color "black")
      (set-foreground-color "gray")
      (put real-this-command 'xstatus t))))

;;; Default color and font settings

(custom-theme-set-faces
 'my-light
 `(default                      ((t (:background ,my-background-color         :foreground ,my-foreground-color))))
 ;; Las siguientes 'faces' afectan a los 'header' de 'org-mode' porque heredan los atributos de estas.
 ;;`(font-lock-constant-face      ((t (:foreground ,my-constant-face-color      :bold t))))
 ;;`(font-lock-variable-name-face ((t (:foreground ,my-variable-name-face-color :bold t))))
 )

;;; Cursor

;; Color del cursor
(set-cursor-color my-cursor-color)

;;Color al resaltar un texto con el cursor.
(set-face-attribute 'region nil :background my-cursor-select-text)

;;; Tabs

;; (set-face-attribute 'tab-bar nil
;;                     :background bg-color-active-tab-bar
;;                     :foreground fg-color-active-tab-bar
;;                     ;; :box        box-config
;;                     :overline   nil
;;                     :underline  nil
;;                     )
;; (set-face-attribute 'tab-bar-tab-inactive nil
;;                     :background bg-color-inactive-tab-bar
;;                     :foreground fg-color-inactive-tab-bar
;;                     ;; :box        box-config
;;                     :overline   nil
;;                     :underline  nil
;;                     )
;; (set-face-attribute 'tab-bar-tab nil
;;                     :background bg-color-tab-bar-tab
;;                     :foreground fg-color-tab-bar-tab
;;                     ;; :box        box-config
;;                     :overline   nil
;;                     :underline  nil
;;                     )

;;; Modeline

(set-face-attribute 'mode-line nil
                    :background bg-color-active-mode-line
                    :foreground fg-color-active-mode-line
                    ;; :box        box-config
                    :overline   nil
                    :underline  nil
                    )
(set-face-attribute 'mode-line-inactive nil
                    :background bg-color-inactive-mode-line
                    :foreground fg-color-inactive-mode-line
                    ;; :box        box-config
                    :overline   nil
                    :underline  nil
                    )

;;; Org blocks faces

;; Colors for org-block
;; https://stackoverflow.com/questions/44811679/orgmode-change-code-block-background-color
(require 'org)
(set-face-attribute 'org-block-begin-line nil :overline   "dark gray" :underline  "dark gray" :foreground "magenta" :background "white smoke" :extend t)
(set-face-attribute 'org-block nil                                                            :foreground "black"   :background "white smoke" :extend t)
(set-face-attribute 'org-block-end-line nil   :overline   "dark gray" :underline  "dark gray" :foreground "magenta" :background "white smoke" :extend t)

;;; End
(provide-theme 'my-light)
